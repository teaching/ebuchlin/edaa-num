;+
; NAME:
;  sa_shells_to_fields
;
; PURPOSE:
;  Transforms shell values array into fields and energies. Default is
;  to get energies per unit length, but energies per unit mass or per
;  unit volume can also be obtained. 
;
; CALLING SEQUENCE:
;  s = sa_shells_to_fields (shells, /uu, /bb, /zp, /zm, /tot, /ch, /energy, 
;                           rho=rho, aexp=aexp, k0=k0, /massic,
;                           /volumic, atm = atm)
;
; INPUTS:
;  shells: array of complex shell values or real energies or cross helicities,
;    3 dimensions:
;     - first dimensions of the array can be anything
;     - second dimension: must correspond to the position on
;         the loop if computing the energies
;     - last dimension: 0 is zp, 1 is zm
;
; KEYWORD PARAMETERS:
;  /uu, /bb, /zp, /zm, /tot, /ch (one of these is needed): field to compute
;  (velocity, magnetic, Elsasser +/-, total energy, cross-helicity)
;  /energy: compute lineic energy in field instead of returning the
;    amplitudes of the field. Does not apply to cross helicity or total energy.
;  rho: mass density of plasma
;  aexp: over-expansion factor (in area): the width of the loop at a
;    given position is the width deduced from k0, multiplied by square
;    root of aexp at this position
;  k0: k0 wavenumber of simulation
;  /massic:  get energy per unit mass instead of energy per unit length
;  /volumic: get energy per unit volume instead of energy per unit length
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUTS:
;  Field or energy of field
;
; SIDE EFFECTS:
;  None
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric.buchlin@ias.u-psud.fr
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  10 Oct 2005 EB/AV Created
;  07 Nov 2005 EB Adapted for shell-atm: rho...
;  27 Mar 2006 EB Added aexp, massic and volumic keywords
;  24 Jul 2014 EB Added ch keyword for cross helicity
;  05 Sep 2014 EB Added tot keyword for sum of fields
;-

function sa_shells_to_fields, shells, uu=uu, bb=bb, zp=zp, zm=zm, tot=tot, ch=ch, $
                              energy=energy, rho=rho, aexp=aexp, k0=k0, $
                              massic=massic, volumic=volumic, atm=atm
if keyword_set(uu) then uu = 1 else uu = 0
if keyword_set(bb) then bb = 1 else bb = 0
if keyword_set(zp) then zp = 1 else zp = 0
if keyword_set(zm) then zm = 1 else zm = 0
if keyword_set(tot) then tot = 1 else tot = 0
if keyword_set(ch) then ch = 1 else ch = 0
if uu + bb + zp + zm + tot + ch ne 1 then begin
    message, 'One and only one of /uu, /bb, /zp, /tot, /zm, /ch is needed'
endif

if uu then begin
   f = (shells[*, *, 0] + shells[*, *, 1]) * .5d
endif else if bb then begin
   f = (shells[*, *, 0] - shells[*, *, 1]) * .5d
endif else if zp then begin
   f = shells[*, *, 0]
endif else if zm then begin
   f = shells[*, *, 1]
endif else if tot then begin
   f = abs (shells[*, *, 0]^2) + abs (shells[*, *, 1]^2)
endif else if ch then begin
   ep = abs (shells[*, *, 0]^2)
   em = abs (shells[*, *, 1]^2)
   f = (ep - em) / (ep + em)
endif

if keyword_set(energy) and not ch then begin
    vol = 0
    mas = 0
    if keyword_set (volumic) then begin
      vol = 1           ; volumic energy
    endif else if keyword_set (massic) then begin
      mas = 1           ;  massic energy
    endif ; else: default (lineic energy)

    if not tot then f = abs (f) ^ 2  ; field is already squared for total energy

    ; nothing more to do for massic energy; the following is for lineic and volumic energies
    if not mas then begin  
      ; verify shape of rho
      if n_elements(rho) eq 0 then begin
          rho = 1.d
          message, 'Warning: no rho was provided, assuming it is 1.', /info
      endif
      if n_elements(rho) gt 1 and n_elements(rho) ne (size (f))[2] then begin
          message, 'Array rho should be the same size as 2nd dimension of shells'
      endif
      if n_elements(k0) ne 1 and not keyword_set(volumic) then begin
          message, 'Warning: energy requested and k0 not provided' + $
            ' (using k0=2pi)', /info
          k0 = 2.d * !pi
      endif
      ; verify shape of aexp
      if n_elements(aexp) eq 0 and not keyword_set(volumic) then begin
          aexp = 1.d
          message, 'Warning: no aexp was provided, assuming it is 1.', /info
      endif
      if n_elements(aexp) gt 1 and n_elements(aexp) ne (size (f))[2] then begin
          message, 'Array aexp should be the same size as 2nd dimension of shells'
      endif



      for ik=0, (size (f))[1] - 1 do begin
          ; multiply by rho to get energy per unit volume
          f[ik, *] *= rho
          ; multiply by area to get energy per unit length
          if not keyword_set(volumic) then $
            f[ik, *] *= !pi ^ 3 * aexp / k0 ^ 2
      endfor
    endif

    if uu or bb then f = f * .5d else f = f * .25d
endif

return, f

end
