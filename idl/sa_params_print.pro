;+
; NAME:
;  sa_params_print
;
; PURPOSE:
;  Print parameters used for a shell_loop simulation
;
; CALLING SEQUENCE:
;  sa_params_print, "simstate.dat", p=p, atm=atm
;
; OPTIONAL INPUT:
;  "simstate.dat" : simulation state file, from which parameters will be
;    extracted. If not present, data in keyword parameter p is used. If not
;    present, the file "simstate.dat" is used.
;
; KEYWORD PARAMETERS:
;  p: parameters data
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  05 Feb 2003 EB Creation
;  10 Oct 2005 EB/AV Added support of shell-atm
;-

pro sa_params_print, file, p=p, atm=atm

if n_elements(p) eq 0 then begin
    if keyword_set(atm) then atm = 1 else atm = 0
    p = sa_params_get (file, atm=atm)
endif

print, 'Nk: ', string_ns (p.nmin), ' - ', string_ns (p.nmax)
print, 'Nz: ', string_ns (p.nz)
print, 'k0: ', string_ns (p.k0), '    lambda: ', string_ns (p.lambda)
print, 'eta: ', string_ns (p.eta), '    nu: ', string_ns (p.nu)
print, 'eps: ', string_ns (p.eps), '    epsm: ', string_ns (p.epsm)
print, 'b0: ', string_ns (p.b0), '     dz: ', string_ns (p.dz)
print, 'nt: ', string_ns (p.nt), '     nrecen: ', string_ns (p.nrecen)
print, 'nrecsf: ', string_ns (p.nrecsf), '     nrecsp: ', string_ns (p.nrecsp)
if atm then begin
    print, 'rho0: ', string_ns (p.rho0), '     h0: ', string_ns (p.h0)
endif

end
