;+
; NAME:
;  sa_plot_enflux
;
; PURPOSE:
;  Read necessary information and plot energy flux in longitudinal
;  dimension as a function of time and position
;
; CALLING SEQUENCE:
;  sa_plot_enflux, sf, time=time, /log, 
;    /nointerpol, range=range, k=k, /ch,
;    xrange=xrange, yrange=yrange, gamma=gamma, atm=atm,
;    
;
; INPUTS:
;  
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default) to get paramaters (data will be
;  taken from other out_simstate_* files)
;
; KEYWORD PARAMETERS:
;  time: arrays of indices of times (from out_time2i) to plot
;  /log: shows intensities in logarithmic scale
;  /nointerpol: do not interpolate coordinates in plot_image (this may
;    cause axes values to be false)
;  range: the range of the color scale will be reduced (but not extended)
;    to fit this range
;  k: if specified, plot energy flux corresponding only to these
;    perpendicular wavenumber
;  xrange: range for x axis (not for spectrum cuts)
;  yrange: range for y axis (not for spectrum cuts)
;  gamma: gamma factor for displaying image (in the case of a cut).
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  11 Oct 2005 EB/AV Forked from sa_plot_cutk
;  07 Nov 2005 EB Adapted for shell-atm
;  19 Nov 2005 EB Adapted for case when out_simstate_nnn doesn't
;                 contain all planes
;  27 Mar 2006 EB aexp is now taken into account by sa_shells_to_field
;-

pro sa_plot_enflux, sf, time=time, $
                     log=log, nointerpol=nointerpol, range=range, k=k, ch=ch, $
                     xrange=xrange, yrange=yrange, gamma=gamma, $
                     atm=atm

if keyword_set(nointerpol) then nointerpol = 1 else nointerpol = 0
if n_elements(atm) eq 0 then atm = 1
if n_elements(sf) eq 0 then sf = "simstate.dat"

p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1
logk = alog10 (p.k0 * p.lambda ^ (lindgen (nk) + p.nmin))
z = dindgen (p.nz) * p.dz

; default: energy flux contained in all modes
if n_elements(k) eq 0 then k = lindgen (nk)

nt = n_elements(time) 
if nt eq 0 then begin
    time = lindgen (p.nrecsp)
endif
time = time > 0 < (p.nrecsp - 1)
time = time [uniq (time)]
nt = n_elements (time)
if nt eq 0 then begin
    message, 'No valid time was provided, should be between 0 and ' + $
      string_ns (p.nrecsp - 1), /info
    return
endif
realtime = dblarr (nt)

outt2i = 'out_time2i'
outsimstate = 'out_simstate_'

readarray_s, outt2i, p.nrecsp, t2i

; if out_simstate_nnn have only some planes, we need to have va, etc.
; for these planes only
tmpp = sa_params_get (outsimstate + t2i[time[0]], atm=atm)
nz = tmpp.nz
iz = (p.nz / nz) * lindgen (nz)
z = z[iz]
if atm then begin
    readarray, 'out_va', p.nz, va
    readarray, 'out_aexp', p.nz, aexp
    readarray, 'out_rho', p.nz, rho
    va = va[iz]
    aexp = aexp[iz]
    rho = rho[iz]
endif else begin
    va = 1
    aexp = 1
    rho = 1
endelse

s = dblarr (nt, nz)
for it=0, nt-1 do begin
    tmpp = sa_params_get (outsimstate + t2i[time[it]], shells=shells, atm=atm)
    realtime[it] = tmpp.time
    ep = total (sa_shells_to_fields (shells[k, *, *], /zp, /energy, $
                                     rho=rho, aexp=aexp, atm=atm, k0=p.k0), 1)
    em = total (sa_shells_to_fields (shells[k, *, *], /zm, /energy, $
                            rho=rho, aexp=aexp, atm=atm, k0=p.k0), 1)
    if keyword_set(ch) then begin
       s[it, *] = (ep - em) / (ep + em)
       title = 'Cross helicity'
    endif else begin
       s[it, *] = va * (ep - em)
       title = 'Energy flux'
    endelse
endfor

sa_plot_cuts_normalize, s, log=log, range=range, norm=norm, atm=atm
plot_image_eb, s, realtime, z, title= title, $
  gamma=gamma, norm=norm, xtitle='t', ytitle='z', $
  nointerpol=nointerpol
print, "Range: ", min (s, max=tmp), tmp

end
