;+
; NAME:
;  seq_count
;
; PURPOSE:
;  Gives beginning and length of sequences of successive integers in
;  an array
;
; CATEGORY:
;  gen
;
; CALLING SEQUENCE:
;  res = seq_count (array, /sort, /values, nbseq=nbseq)
;
; INPUTS:
;  array: array of integers
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;  sort: sort array at beginning
;  values: if set, output values instead of indices
;
; OUTPUTS:
;  2-line array: index (or value, if /values is set) of beginning of
;  sequence, and length of sequence
;
; OPTIONAL OUTPUTS:
;  nbseq: number of integer sequences found
;
; EXAMPLE:
;  seq_count ([0,1,6,7,8,9,12,13]) gives
;     [[0,2,6 ],[2,4,2]] if /values is unset
;  or [[0,6,12],[2,4,2]] if /values is set
;
; PROJECT:
;  PhD/gen
;
; AUTHORS:
;  EB Eric Buchlin eric.buchlin@ias.fr
;
; MODIFICATION HISTORY:
;  03 Jun 2003 EB Creation
;
;-

function seq_count, array, sort=sort, nbseq=nbseq, values=values


if keyword_set (sort) then begin
    array = array[sort (array)]
endif
na = n_elements (array) 
res = intarr (na, 2)
nbseq = 0
length = 1
for i = 0, na - 2 do begin
    if array[i + 1] eq array[i] + 1 then begin
        length = length + 1
    endif else begin
        res[nbseq, *] = [i - length + 1, length]
        length = 1
        nbseq = nbseq + 1
    endelse
endfor
if length ne 0 then begin
    res[nbseq, *] = [i - length + 1, length]
    length = 1
    nbseq = nbseq + 1
endif

res = res[0:nbseq-1, *]
if keyword_set (values) then begin
    res[*, 0] = array[res[*, 0]]
endif

return, res

end
