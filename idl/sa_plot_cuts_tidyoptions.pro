; tidy options for sa_plot_cut*

pro sa_plot_cuts_tidyoptions, sf, uu=uu, bb=bb, zp=zp, zm=zm, atm=atm, $
                              nointerpol=nointerpol, field=field

if keyword_set(uu)  then begin
    uu = 1
    field = 'u'
endif else uu = 0
if keyword_set(bb)  then begin
    bb = 1
    field = 'b'
endif else bb = 0
if keyword_set(zp)  then begin
    zp = 1
    field = 'z+'
endif else zp = 0
if keyword_set(zm)  then begin
    zm = 1
    field = 'z-'
endif else zm = 0

if uu + bb + zp + zm ne 1 then begin
    message, "You should ask for one of the fields spectra: " + $
      "please specify /uu, /bb, /zm or /zp"
    return
endif

if keyword_set(nointerpol) then nointerpol = 1 else nointerpol = 0
if n_elements(atm) eq 0 then atm = 1 else atm = 0

if n_elements(sf) eq 0 then sf = "simstate.dat"

end
