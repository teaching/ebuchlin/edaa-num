;+
; NAME:
;  plot_image_eb
;
; PURPOSE:
;  Plot 2D image with axes
;
; CALLING SEQUENCE:
;  plot_image_eb, image, x, y, xtitle=xtitle, ytitle=ytitle,
;    title=title, _extra=e, /norm, /nointerpol, gamma=gamma,
;    xticklen=xticklen, yticklen=yticklen
;
; INPUTS:
;  the x and y scales are defined by the first and last
;  elements in the arrays x and y so the procedure may be called by
;  plot_image,image,[xmin,xmax],[ymin,ymax]
;  the tickmarks are black and the background white
;  Keywords to plot are inherited
;
;
; KEYWORD PARAMETERS:
;  /nointerpol: does not interpolate the image. In this case and if
;    the coordinates are not evenly spaced, the image will be
;    stretched and the axes values won't be correct
;  xtitle, ytitle, title: titles of axes and of plot
;  /norm: stretch color table (use bytscl)
;  /nointerpol: do not interpolate data (even if x or y is irregularly
;     spaced)
;  gamma: gamma factor for displaying image. Implies /norm.
;  xticklen, yticklen: length of tick marks on axes (default -.02)
;
; OUTPUTS:
;
;
;
; SIDE EFFECTS:
;  Plot
;
; BUGS:
;  Interacts badly with !p.multi, but this can be overcome by using
;  !p.position
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  PhD IAS
;
; AUTHORS:
;  Eric Buchlin, IAS, eric.buchlin@medoc-ias.u-psud.fr
;
; MODIFICATION HISTORY:
;  10 Nov 2003 EB Creation, based from Rita Betta's plot_image
;  30 Mar 2004 EB Corrected bug of axes' second plotting
;  16 Jul 2004 EB Added gamma keyword
;  26 Jul 2005 EB Added (x|y)ticklen keywords
;  19 Nov 2005 EB Corrected bug: gamma correction with integer array
;                 lead to 2-level image
;-


pro plot_image_eb, image, x, y, xtitle=xtitle, ytitle=ytitle, title=title, $
                   norm=norm, _extra=e, nointerpol=nointerpol, gamma=gamma, $
                   xticklen=xticklen, yticklen=yticklen

if n_params() lt 1 then begin
  print,'plot_image, image, x, y'
  return
endif

if n_elements(xtitle) eq 0 then xtitle=''
if n_elements(ytitle) eq 0 then ytitle=''
if n_elements(title)  eq 0 then title=''
if keyword_set(nointerpol) then nointerpol = 1 else nointerpol = 0
if n_elements(xticklen) eq 0 then xticklen = -.02
if n_elements(yticklen) eq 0 then yticklen = -.02
si = size (image)
if si[0] ne 2 then message, "Image should be a 2D array"
nx = si[1]
ny = si[2]

; heuristics to determine black and white colors
black = 0
white = !d.n_colors - 1
if !d.name ne 'PS' and !d.n_colors gt 256 then white = 2L ^ 24 - 1L

oldpmulti = !p.multi

if n_elements(x) eq 0 then x = indgen (nx)
if n_elements(y) eq 0 then y = indgen (ny)

if n_elements(x) ne nx then $
  message, "x should be the same size as the first dimension of image"
if n_elements(y) ne ny then $
  message, "y should be the same size as the second dimension of image"

if nointerpol then begin
    xx = x
    yy = y
    im = image
endif else begin
; make x and y regular, interpolate image
    xx = x[0] + findgen (4 * nx) * (x[nx-1] - x[0]) / float (4 * nx)
    yy = y[0] + findgen (4 * ny) * (y[ny-1] - y[0]) / float (4 * ny)
    im = interpol2d (float (image), x, y, xx, yy)
endelse
xx1 = min (xx, max=xx2)
yy1 = min (yy, max=yy2)
plot, [xx1, xx2], [yy1, yy2], xstyle=1, ystyle=1, /nodata, $ 
  xtitle=xtitle, ytitle=ytitle, title=title, $
  color=black, back=white, xticklen=xticklen, yticklen=yticklen, _extra=e

if n_elements (gamma) ne 0 then begin
    minim = min (im, max=maxim)
    im = double (im - minim) / (maxim - minim)
    im = (im > 0.) ^ gamma
endif
if keyword_set(norm) or n_elements(gamma) ne 0 then im = bytscl (im)

if(!d.name eq 'PS') then begin
  tv, im, !x.window(0), !y.window(0), $    ; display image
    xsize=!x.window(1)-!x.window(0), $
    ysize=!y.window(1)-!y.window(0), $
    /norm
endif else begin
  px = !x.window * !d.x_vsize
  py = !y.window * !d.y_vsize
  im2 = congrid (im, px[1] - px[0], py[1] - py[0])
                                ; regrid image to fit plot window
  tv, im2, !x.window(0), !y.window(0),$    ; display image
    xsize=!x.window(1)-!x.window(0), $
    ysize=!y.window(1)-!y.window(0), $
    /norm
endelse

; plot axes again at same position
!p.multi = oldpmulti
plot,  [xx1, xx2], [yy1, yy2], xstyle=1, ystyle=1, /nodata, $
  color=black, back=white, xticklen=xticklen, yticklen=yticklen, $
  /noerase, _extra=e
!p.multi[0] = (oldpmulti[0] + 1) mod (!p.multi[1] * !p.multi[2])

end
