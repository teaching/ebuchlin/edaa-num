;+
; NAME:
;  sa_plot_dt
;
; PURPOSE:
;  Read necessary information and plot time step and time scales, as a
;  function of time, or as a function of perpendicular wavenumber k
;  for a given time, or as a function of both k and position z
;
; CALLING SEQUENCE:
;  sa_plot_dt, sf, xrange=xrange, yrange=yrange, /taunl, /k, /zk,
;    minmax=minmax, /xlog, hyper=hyper, atm=atm, bisk=bisk, title=title
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  xrange: range of x-axis
;  yrange: range of y-axis
;  /taunl: plot non-linear time scale (if out_taunl exists) instead of
;   time step. This is now the default: please use taunl=0 if you want
;   to plot the time step.
;  /k: plot time scales as a function of k (at the time corresponding
;   to sf) instead of time series of time scales
;  /zk: 2D plot of time scales as a function of k and z (at the time
;    corresponding to sf) instead of time series of time scales
;  xlog: plot time as a logarithmic axis
;  minmax: if set, the plot contains minmax couples of
;    points, with the min and max of the time series over some
;    interval around the point (saves memory or postscript file size)
;  /nocaption: don't plot caption for /k (which interferes with !p.multi)
;  hyper: hyperviscosity exponent (default for viscosity: 2)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;  bisk: value of the Biskamp 1994 "B_0" term alpha coefficient. Then
;    plot also the corresponding time scale (only possible with /k)
;  title: plot title
;  /dissk: compute and overplot dissipation wavenumber (for /k)
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; BUGS:
;  When p.nt < 0 (end of simulation determined by time instead of
;  number of timesteps), the number of timesteps is not available
;  and the timestep length cannot be computed: we assume that data is
;  output at every timestep... Fix: need to add this information in
;  what the simulation outputs to simstate.dat
;
;  With neither /k or /zk, the stratification of the atmosphere is not
;  taken into account
;
;  /zk is experimental
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; AUTHORS:
;  Eric Buchlin
;
; MODIFICATION HISTORY:
;  08 Nov 2003 EB Creation
;  15 Dec 2003 EB Added yrange keyword
;  09 Aug 2004 EB Added taunl keyword
;  11 Aug 2004 EB Added minmax keyword
;  16 Aug 2004 EB Added xlog keyword
;  07 Nov 2005 EB Added xrange keyword and adapted for shell-atm
;  15 Nov 2005 EB /taunl is now the default
;  16 Nov 2005 EB/AV Added /k keyword (from AV's sa_plot_taunl)
;  29 Mar 2006 EB Added /zk keyword
;  02 Nov 2006 EB Added /nocaption keyword
;  17 Dec 2012 EB Added bisk parameter
;  28 Apr 2014 EB Added hyper and title keywords
;-

pro sa_plot_dt, sf, xrange=xrange, yrange=yrange, taunl=taunl, k=k, zk=zk, $
                minmax=minmax, xlog=xlog, nocaption=nocaption, hyper=hyper, $
                atm=atm, bisk=bisk, title=title, dissk=dissk

if n_elements(taunl) eq 0 then taunl = 1
if keyword_set(xlog) then xlog = 1 else xlog = 0
if n_elements(atm) eq 0 then atm = 1
; k will have a different meaning later; use plotk for this option
if keyword_set(k) then plotk = 1 else plotk = 0
if keyword_set(zk) then plotzk = 1 else plotzk = 0
if n_elements(sf) eq 0 then sf = "simstate.dat"
if n_elements(title) eq 0 then title = ""
if n_elements(hyper) eq 0 then hyper = 2

if plotk or plotzk then begin
    ; use simstate.dat to get parameters (especially dz) of simulation,
    ; even if the plot is made from a out_simstate_nnn data file
    psim = sa_params_get (atm=atm)
    ; parameters of atmosphere
    if atm ne 0 then begin
        readarray, 'out_va', psim.nz, va,  err=err
        readarray, 'out_vad', psim.nz, vad
;        readarray, 'out_rho', psim.nz, rho
        readarray, 'out_aexp', psim.nz, aexp
    endif else begin
        va = replicate (1.d, psim.nz)
        vad = replicate (0.d, psim.nz)
;        rho = replicate (1.d, psim.nz)
        aexp = replicate (1.d, psim.nz)
    endelse
    ; the simulation state used for the plot
    p = sa_params_get (sf, shells=z, atm=atm)
    nk = p.nmax - p.nmin + 1L
    k = p.k0 * p.lambda ^ (p.nmin + lindgen (nk))
    logk = alog10 (k)
    ; select only planes that were output, not all planes
    zstep = psim.nz / p.nz
    iz = zstep * lindgen (p.nz)
    va = va[iz]
    vad = vad[iz]
;    rho = rho[iz]
    aexp = aexp[iz]
    ; convert zp and zm to u and b
    u = sa_shells_to_fields (z, /uu)
    b = sa_shells_to_fields (z, /bb)
    zp = sa_shells_to_fields (z, /zp)
    zm = sa_shells_to_fields (z, /zm)
endif

if plotk then begin
    ; compute time scales
    if keyword_set(bisk) then ntau = 6 else ntau = 5
    tau = dblarr (nk, ntau)
    tauname = ['t_al', 't_nl', 't_nu', 't_f', 't_cr', 't_bisk', 't_ca', 't_r']
    tauline = [2, 0, 3, 4, 1, 5, 6, 1]   ;; linestyles
;    tauline = [0, 0, 0, 0, 0, 0, 0, 0]  ;; if linestyles don't show up...
    if !d.name eq 'PS' then white = 0L else white = 2L^24 - 1L
    ; actually "white" is black for PS
    taucolor = replicate (white, ntau)
    ; Alfven time scale
    tau[*, 0]= replicate (alog10 (psim.dz / max (va)), nk)
    ; temporary values for Biskamp term
    bb0 = dblarr (p.nz)
    for ik=0, nk-1 do begin
        ; Non-linear time scale
        tau[ik, 1] = -alog10 (k[ik] * max (abs ([zp[ik, *] / sqrt (aexp), $
                                                 zm[ik, *] / sqrt (aexp)])))
        ; Dissipation time scale
        tau[ik, 2] = -alog10 (max ([psim.nu, psim.eta]) * $
                              k[ik] ^ hyper / max (aexp))
        ; Biskamp term (update temporary array and then compute time scale)
        if keyword_set (bisk) then begin
           if ik gt 2 then begin
              bb0 += double (zp[ik-1, *] - zm[ik-1, *])
              tau[ik, 5] = -alog10 (k[ik] * bisk * max (abs (bb0 / sqrt (aexp))))
           endif else begin
              tau[ik, 5] = !values.d_infinity
           endelse
        endif
    endfor

    ; Forcing time scale
    tau[*, 3] = replicate (alog10 (psim.tstar), nk)
    ; Crossing time scale
    tau[*, 4] = replicate (alog10 (p.dz * total (1.d / va)), nk)
    ; Cascade time scale: sum t_nl in inertial range
;     for ik=4, nk-1 do begin
;         if tau[ik, 1] ge tau[ik, 2] + 1. then break ; dissipation range
;         tau[0, 5] += 10.d ^ tau[ik, 1]
;     endfor
;     tau[*, 5] = replicate (alog10 (tau[0, 5]), nk)
    ; Reflection time scale
;    tau[*,6] = replicate (-alog10 (max (vad)), nk)
    ; ranges
    if n_elements(yrange) ne 2 then $
      yrange = [min (tau, max=tmp), tmp]
    if n_elements(xrange) ne 2 then $
      xrange = [min (logk, max=tmp), tmp]
    ; plots
    if not keyword_set(nocaption) then begin 
        pos1 = [0.1, 0.11, 0.97, 0.96]
        pos2 = [0.75, 0.74, 0.93, 0.94]
    endif
    plot, logk, logk, xrange=xrange, yrange=yrange, xtitle='log10 k_perp', $
      ytitle='log10 tau', title=title, /nodata, xstyle=1, ystyle=3, position=pos1
    for i=0, ntau-1 do begin
        oplot, logk, tau[*, i], line=tauline[i], color=taucolor[i]
    endfor
    
    ; dissipation wavenumber (position of max (spectrum * k^hyper)
    ; spectrum (u^2/k) deduced from tNL (1/ku)
    if keyword_set(dissk) then begin
       s = -2 * tau[*,1] + (hyper - 3) * logk
       sm = max (s, ism)
;       oplot, replicate (logk[ism], 2), !y.crange, linestyle=1
      oplot, [logk[ism]], [tau[ism, 1]], psym=4
    endif

    ; caption
    if not keyword_set(nocaption) then begin 
        x = [0, .6]
        xc = .7
        yinc = [.16, .16]
        for i=0, ntau-1 do begin
            plot, x, 1 - (i + 1) * yinc, line=tauline[i], $
              color=taucolor[i], /normal, $
              position=pos2, noerase=1, xrange=[0, 1], yrange=[0, 1], $
              xstyle=4, ystyle=4
            xyouts, xc, 1 - (i + 1) * yinc, tauname[i]
        endfor
    endif
endif else if plotzk then begin
    ; compute time scales
    tauname = ['t_al', 't_nl', 't_nu']
    ntau = n_elements(tauname)
    tau = dblarr (nk, p.nz, ntau)
    tauminind = intarr (nk, p.nz)
    taumin = dblarr (nk, p.nz)
    for ik=0, nk-1 do begin
        for iz=0, p.nz-1 do begin
            ; Alfven time scale of propagation between planes
            tau[ik, iz, 0] = alog10 (psim.dz / va[iz])
            ; Non-linear time scale
            tau[ik, iz, 1] = -alog10 (k[ik] / sqrt (aexp[iz]) * $
                                      max (abs ([zp[ik, iz], zm[ik, iz]])))
            ; Dissipation time scale
            tau[ik, iz, 2] = -alog10 (max ([psim.nu, psim.eta]) * $
                                      k[ik] ^ hyper / aexp[iz])
            taumin[ik, iz] = min ([tau[ik, iz, 0]+.7, tau[ik, iz, 1], $
                                   tau[ik, iz, 2]], tmp)
            tauminind[ik, iz] = tmp
        endfor
    endfor
    print, 'Time scales of Alfven wave propagation between planes, non-linear interactions, dissipation, minimum of these (in development)'
    tvscl, congrid([tau[*, *, 0], tau[*, *, 1], tau[*, *, 2], taumin], $
                   72 * 5, 100 * 5)
    pause
    tvscl, congrid([tau[*, *, 0], tau[*, *, 1], taumin], $
                   54 * 5, 100 * 5)

    stop
endif else begin
    p = sa_params_get (sf, atm=atm)
    readarray, 'out_time', p.nrecen, t
    if taunl then begin
        readarray, 'out_taunl', p.nrecen, dtnl, err=err
        if err ne 0 then begin
            taunl = 0
            message, 'Error opening file out_taunl' + $
              '. Reverting to display of time step instead ' + $
              'of non-linear time scale', /info
        endif else dtmax = max (dtnl, min=dtmin)
    endif
    if not taunl then begin
        dt = t - shift (t, 1)
        if p.nt gt 0 then begin
            dt = dt[1:*] / (p.nt / p.nrecen)
        endif else begin
            message, 'Warning: number of time steps is not available. ' + $
              'Assuming that it is p.nrecen', /info
            dt = dt[1:*]
        endelse
        t = t[0:p.nrecen-2]
        dtmax = max (dt, min=dtmin)
    endif
    dtnu = (p.k0 * p.lambda ^ p.nmax) ^ hyper
    dtnu = 1.d / (max ([p.nu, p.eta]) * dtnu)
    dtal = p.dz / p.b0
    if n_elements(yrange) ne 2 then begin
        yrange = [min ([dtmin, dtnu, dtal]), max ([dtmax, dtnu, dtal])]
    endif
    if n_elements(xrange) ne 2 then begin
        xrange = [min (t), max (t)]
    endif

    if taunl then begin
        t0 = t[0]
        t1 = t[p.nrecen - 1]
        if n_elements(minmax) ne 0 then begin
            if p.nrecen ge 2L * minmax  then begin
                t = congrid (t, 2L * minmax)
                step = double (p.nrecen - 1) / minmax
                dtnl2 = dblarr (2L * minmax)
                for i=0L, minmax - 1L do begin
                    dtnl2[2 * i] = max (dtnl[long ( i       * step) : $
                                             long ((i + 1L) * step)], min=tmp)
                    dtnl2[2 * i + 1] = tmp
                endfor
                dtnl = dtnl2
            endif
        endif
        plot, t, dtnl, /ylog, xrange=xrange, yrange=yrange, title=title, $
          xtitle='t', ytitle='dt', xstyle=1, ystyle=3, xlog=xlog
    endif else begin
        t0 = t[0]
        t1 = t[p.nrecen - 2]
        if n_elements(minmax) ne 0 then begin
            if p.nrecen ge 2L * minmax  then begin
                t = congrid (t, 2L * minmax)
                step = double (p.nrecen - 1) / minmax
                dt2 = dblarr (2L * minmax)
                for i=0L, minmax - 1L do begin
                    dt2[2 * i] = max (dt[long ( i       * step) : $
                                         long ((i + 1L) * step)], min=tmp)
                    dt2[2 * i + 1] = tmp
                endfor
                dt = dt2
            endif
        endif
        plot, t, dt, /ylog, yrange=yrange, xtitle='t', ytitle='dt', $
          xstyle=1, ystyle=3, xlog=xlog, title=title
    endelse
    oplot, [t0, t1], [dtnu, dtnu], linestyle=1
    oplot, [t0, t1], [dtal, dtal], linestyle=2
endelse

end
