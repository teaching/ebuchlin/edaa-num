;+
; NAME:
;  sa_data_merge
;
; PURPOSE:
;  Merge data output by successive simulations of the shell-atm model
;
; CALLING SEQUENCE:
;  sa_data_merge, dirs_in, dir_out, atm=atm
;
; INPUTS:
;  dirs_in: list of directories to merge
;  dir_out: directory where to put merged data
;
; OPTIONAL INPUTS:
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; SIDE EFFECTS:
;  Creates and fills dir_out
;
; RESTRICTIONS:
;  . dirs_in should contain successive parts of the same simulation.
;  . Unix-only (see spawn)
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; TODO:
;  Merge also out_simstate_nnn and time2i (need to remap time2i and nnn?)
;
; AUTHORS:
;  Eric Buchlin, http://eric.buchlin.org/
;
; MODIFICATION HISTORY:
;  21 Apr 2004 EB Creation
;  07 Nov 2005 EB Adapted to shell-atm
;-

pro sa_data_merge, dirs_in, dir_out, atm=atm

if n_elements(atm) eq 0 then atm = 1
ni = n_elements(dirs_in)
no = n_elements(dir_out) 

if ni lt 2 then begin
    message, "At least 2 input directories should be provided", /info
    return
endif
if no ne 1 then begin
    message, "An output directory should be provided", /info
    return
endif

; get data
pi = replicate ({sa_params}, ni)
for i=0, ni-1 do begin
    pushd, dirs_in[i]
    message, "Reading " + dirs_in[i] + "...", /info
    ; get and verify parameters
    pi[i] = sa_params_get ("simstate.dat", atm=atm)
    nk =  pi[i].nmax - pi[i].nmin + 1
    if i ne 0 then begin
        if nk - 1 ne pi[0].nmax - pi[0].nmin then begin
            message, "Number of shells has changed, " + dirs_in[i] + $
              " will not be taken into account", /info
            continue
        endif
        if pi[i].nz ne pi[0].nz then begin
            message, "Number of planes has changed, " + dirs_in[i] + $
              " will not be taken into account", /info
            continue
        endif
        if pi[i].lambda ne pi[0].lambda or $
           pi[i].k0 ne pi[0].k0 or $
           pi[i].dz ne pi[0].dz or $
           pi[i].eps ne pi[0].eps or $
           pi[i].epsm ne pi[0].epsm or $
           pi[i].nu ne pi[0].nu or $
           pi[i].eta ne pi[0].eta then begin
            message, "Warning: a parameter has changed in " + dirs_in[i] + $
              " but it still will be taken into account", /info
        endif
    endif
    readarray, "out_time",     pi[i].nrecen, tmp, err=err
    ; skip directories where there is no time information
    if err ne 0 then continue
    if n_elements(t) eq 0 then begin
        is1 = 1
        t = tmp
    endif else begin
        is1 = 0
        if min (tmp) lt max (t) then begin
            message, "Warning: non-increasing times", /info
        endif
        t = [t, tmp]
    endelse
    readarray, "out_taunl",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(taunl) eq 0 and is1 then taunl = tmp $
        else if n_elements(taunl) ne 0 and not is1 then taunl = [taunl, tmp]
    endif
    readarray, "out_ent02",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(en02) eq 0 and is1 then en02 = tmp $
        else if n_elements(en02) ne 0 and not is1 then en02 = [en02, tmp]
    endif
    readarray, "out_ent08",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(en08) eq 0 and is1 then en08 = tmp $
        else if n_elements(en08) ne 0 and not is1 then en08 = [en08, tmp]
    endif
    readarray, "out_ent14",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(en14) eq 0 and is1 then en14 = tmp $
        else if n_elements(en14) ne 0 and not is1 then en14 = [en14, tmp]
    endif
    readarray, "out_ent20",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(en20) eq 0 and is1 then en20 = tmp $
        else if n_elements(en20) ne 0 and not is1 then en20 = [en20, tmp]
    endif
    readarray, "out_enu",      pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(enu) eq 0 and is1 then enu = tmp $
        else if n_elements(enu) ne 0 and not is1 then enu = [enu, tmp]
    endif
    readarray, "out_enb",      pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(enb) eq 0 and is1 then enb = tmp $
        else if n_elements(enb) ne 0 and not is1 then enb = [enb, tmp]
    endif
    readarray, "out_dissu",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(du) eq 0 and is1 then du = tmp $
        else if n_elements(du) ne 0 and not is1 then du = [du, tmp]
    endif
    readarray, "out_dissb",    pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(db) eq 0 and is1 then db = tmp $
        else if n_elements(db) ne 0 and not is1 then db = [db, tmp]
    endif
    readarray, "out_crosshel", pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(ch) eq 0 and is1 then ch = tmp $
        else if n_elements(ch) ne 0 and not is1 then ch = [ch, tmp]
    endif
    readarray, "out_maghel",   pi[i].nrecen, tmp, err=err, /quiet
    if err eq 0 then begin
        if n_elements(mh) eq 0 and is1 then mh = tmp $
        else if n_elements(mh) ne 0 and not is1 then mh = [mh, tmp]
    endif
    if pi[i].nrecen lt 1000 then begin
        readarray, "out_movu",     pi[i].nrecen * pi[i].nz * nk * 2L, tmp, $
          err=err, /quiet
        if err eq 0 then begin
            if n_elements(movu) eq 0 and is1 then movu = tmp $
            else if n_elements(movu) ne 0 and not is1 then movu = [movu, tmp]
        endif
        readarray, "out_movb",     pi[i].nrecen * pi[i].nz * nk * 2L, tmp, $
          err=err, /quiet
        if err eq 0 then begin
            if n_elements(movb) eq 0 and is1 then movb = tmp $
            else if n_elements(movb) ne 0 and not is1 then movb = [movb, tmp]
        endif
    endif
    readarray, "out_specu",    pi[i].nrecsp * pi[i].nz * nk, tmp, $
      err=err, /quiet
    if err eq 0 then begin
        if n_elements(spu) eq 0 and is1 then spu = tmp $
        else if n_elements(spu) ne 0 and not is1 then spu = [spu, tmp]
    endif
    readarray, "out_specb",    pi[i].nrecsp * pi[i].nz * nk, tmp, $
      err=err, /quiet
    if err eq 0 then begin
        if n_elements(spb) eq 0 and is1 then spb = tmp $
        else if n_elements(spb) ne 0 and not is1 then spb = [spb, tmp]
    endif
    readarray, "out_specavu",    pi[i].nrecsp * nk, tmp, $
      err=err, /quiet
    if err eq 0 then begin
        if n_elements(spavu) eq 0 and is1 then spavu = tmp $
        else if n_elements(spavu) ne 0 and not is1 then spavu = [spavu, tmp]
    endif
    readarray, "out_specavb",    pi[i].nrecsp * nk, tmp, $
      err=err, /quiet
    if err eq 0 then begin
        if n_elements(spavb) eq 0 and is1 then spavb = tmp $
        else if n_elements(spavb) ne 0 and not is1 then spavb = [spavb, tmp]
    endif
    if i eq ni-1 then p = sa_params_get ("simstate.dat", shells=shells, $
                                         atm=atm)
    popd
endfor

; write merged data
spawn, ['mkdir', '-p', dir_out], /noshell
pushd, dir_out
message, "Writing " + dir_out + "...", /info
if n_elements(t) ne 0 then begin
    openw, u, "out_time", /get_lun, err=err
    printf, u, t
    free_lun, u

    if n_elements(taunl) ne 0 then begin
        openw, u, "out_taunl", /get_lun, err=err
        printf, u, taunl
        free_lun, u
    endif
    if n_elements(en02) ne 0 then begin
        openw, u, "out_ent02", /get_lun, err=err
        printf, u, en02
        free_lun, u
    endif
    if n_elements(en08) ne 0 then begin
        openw, u, "out_ent08", /get_lun, err=err
        printf, u, en08
        free_lun, u
    endif
    if n_elements(en14) ne 0 then begin
        openw, u, "out_ent14", /get_lun, err=err
        printf, u, en14
        free_lun, u
    endif
    if n_elements(en20) ne 0 then begin
        openw, u, "out_ent20", /get_lun, err=err
        printf, u, en20
        free_lun, u
    endif
    if n_elements(enu) ne 0 then begin
        openw, u, "out_enu", /get_lun, err=err
        printf, u, enu
        free_lun, u
    endif
    if n_elements(enb) ne 0 then begin
        openw, u, "out_enb", /get_lun, err=err
        printf, u, enb
        free_lun, u
    endif
    if n_elements(du) ne 0 then begin
        openw, u, "out_dissu", /get_lun, err=err
        printf, u, du
        free_lun, u
    endif
    if n_elements(db) ne 0 then begin
        openw, u, "out_dissb", /get_lun, err=err
        printf, u, db
        free_lun, u
    endif
    if n_elements(ch) ne 0 then begin
        openw, u, "out_crosshel", /get_lun, err=err
        printf, u, ch
        free_lun, u
    endif
    if n_elements(mh) ne 0 then begin
        openw, u, "out_maghel", /get_lun, err=err
        printf, u, mh
        free_lun, u
    endif
    if n_elements(movu) ne 0 then begin
        openw, u, "out_movu", /get_lun, err=err
        printf, u, movu
        free_lun, u
    endif
    if n_elements(movb) ne 0 then begin
        openw, u, "out_movb", /get_lun, err=err
        printf, u, movb
        free_lun, u
    endif
    if n_elements(spu) ne 0 then begin
        openw, u, "out_specu", /get_lun, err=err
        printf, u, spu
        free_lun, u
    endif
    if n_elements(spb) ne 0 then begin
        openw, u, "out_specb", /get_lun, err=err
        printf, u, spb
        free_lun, u
    endif
    if n_elements(spavu) ne 0 then begin
        openw, u, "out_spavu", /get_lun, err=err
        printf, u, spavu
        free_lun, u
    endif
    if n_elements(spavb) ne 0 then begin
        openw, u, "out_spavb", /get_lun, err=err
        printf, u, spavb
        free_lun, u
    endif
    ; generate and write simstate.dat
    p.nrecen = total (pi.nrecen)
    p.nrecsp = total (pi.nrecsp)
    p.nrecsf = total (pi.nrecsf)
    sa_params_write, p, "simstate.dat", shells=shells, atm=atm
endif else begin
    message, "  No data to write", /info
endelse
popd
end
