;+
; Parameters for shell-atm
;
;
;  07 Jun 2005 EB Added time
;  07 Nov 2005 EB Changed from sl_ (shell-loop) to sa_ (shell-atm)
;-

pro sa_params__define
tmp = {sa_params, nmin:0L, nmax:0L, nz:0L, k0:0.d, lambda:0.d, $
       nu:0.d, eta:0.d, $
       eps:0.d, epsm:0.d, b0:0.d, dz:0.d, nt:0L, $
       nrecen:0L, nrecsp:0L, nrecsf:0L, $
       time:0.d, tstar:0.d, rho0:0.d, h0:0.d }
end
