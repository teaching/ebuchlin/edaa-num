;+
; NAME:
;  interpol2d
;
; PURPOSE:
;  Interpolates a 2D array whose coordinates can be (separately) irregular .
;
; CALLING SEQUENCE:
;  r = interpol2d (v, x, y, ux, uy, /lsquadratic, /quadratic, /spline)
;
; INPUTS:
;  v: 2D array
;  x: vector of abscissas for v (same dimension as the first dimension
;    of v)
;  y: vector of ordinates for v (same dimension as the second
;    dimension of v) 
;  ux: output abscissas
;  uy: output ordinates
;
; KEYWORD PARAMETERS:
;  /lsquadratic: least-squares quadratic fit in each direction
;  /quadratic: quadratic interpolation in each direction
;  /spline: spline interpolation in each direction
;
; OUTPUTS:
;  r: resulting 2D array
; SIDE EFFECTS:
;  Plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  PhD IAS
;
; AUTHORS:
;  Eric Buchlin, IAS, eric.buchlin@medoc-ias.u-psud.fr
;
; MODIFICATION HISTORY:
;  10 Nov 2003 EB Creation, based from Rita Betta's plot_image
;
;-


function interpol2d, v, x, y, ux, uy, $
                     lsquadratic=lsquadratic, quadratic=quadratic, $
                     spline=spline

s = size (v)
sx = size (x)
sy = size (y)
if s[0] ne 2 then begin
    message, "v should be a 2D array", /info
    return, 0
endif
if sx[0] ne 1 or s[1] ne sx[1] then begin
    message, "x should the same number of elements than the first" + $
      " dimension of v", /info
    return, 0
endif
if sy[0] ne 1 or s[2] ne sy[1] then begin
    message, "y should the same number of elements than the second" + $
      " dimension of v", /info
    return, 0
endif
nx = sx[1]
ny = sy[1]

if keyword_set(lsquadratic) then lsquadratic = 1 else lsquadratic = 0
if keyword_set(  quadratic) then   quadratic = 1 else   quadratic = 0
if keyword_set(     spline) then      spline = 1 else      spline = 0

na = n_elements (ux)
nb = n_elements (uy)

r1 = dblarr (nx, nb)
for ix=0, nx-1 do begin
    r1[ix, *] = interpol (v[ix, *], y, uy)
;    plot_image_eb, r1
;    c = get_kbrd(1)
endfor

r = dblarr (na, nb)
for iy=0L, nb-1L do begin
    r[*, iy] = interpol (r1[*, iy], x, ux)
;    plot_image_eb, r 
;    c = get_kbrd(1)
endfor

return, r
end

