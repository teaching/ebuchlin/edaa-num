;+
; NAME:
;  sa_plot_heatfunc
;
; PURPOSE:
;  Read necessary information and plot heating function (dissipation
;    power per unit length)
;
; CALLING SEQUENCE:
;  sa_plot_heatfunc, sf, /log, /nointerpol, range=range, time=time
;    xrange=xrange, yrange=yrange, cutt=cutt, cutz=cutz, /ddd,
;    xtitle=xtitle, ytitle=ytitle, title=title,
;    gamma=gamma, profile=profile, atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  /log: if a 2D cut is plotted, shows intensities in logarithmic
;    scale
;  /nointerpol: do not interpolate coordinates in plot_image (this may
;    cause axes values to be false)
;  time: plot only for these times (expressed as an index of the
;    time dimension of the spectra data cube; is between 0 and
;    p.nrecsp - 1). time can be an array.
;  cutt: plot only cut(s) of the 2D heating function for the given time(s)
;  cutz: plot only cut(s) of the 2D heating function for the given z
;  ddd: plot surface plot instead of 2D image
;  range: for the cuts:
;            if range is a 2-element array, the range of the color
;              scale will be reduced (but not extended) to fit this
;              range
;            if it is a scalar different from 0, the range of the
;              color scale will match the range of values of the whole
;              data cube
;            if it is 0 or not set, the contrast will be maximized for
;              each plot
;  gamma: gamma for displaying image
;  xrange, yrange: ranges for x and y axes (only for profile)
;  xtitle, ytitle, title: titles for axes and for plot
;  profile: plot profile of heating along the model
;     =1 : read out_profheat
;     =2 : compute heating profile from out_simstate_nnnnnnnnnn
;  /volumic: plot dissipation power per unit volume instead of unit
;    length (no effect if atm is 0)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  22 Apr 2004 EB Creation
;  16 Jul 2004 EB Added gamma keyword
;  07 Nov 2005 EB Adapted for shell-atm
;  10 Nov 2005 EB Added ddd, cutt and cutz keywords
;  19 Nov 2005 EB Adapted for case when out_simstate_nnn doesn't
;                 contain all planes
;  12 Dec 2005 EB Added xtitle, ytitle, and title keywords
;  27 Mar 2006 EB Added profile and volumic keywords
;  15 May 2006 EB Added profile=1 or 2 option
;-

pro sa_plot_heatfunc, sf, log=log, nointerpol=nointerpol, range=range, $
                      xrange=xrange, yrange=yrange, time=time, $
                      xtitle=xtitle, ytitle=ytitle, title=title, $
                      cutt=cutt, cutz=cutz, ddd=ddd, gamma=gamma, $
                      profile=profile, massic=massic, volumic=volumic, $
                      atm=atm

if keyword_set(nointerpol) then nointerpol = 1 else nointerpol = 0
if n_elements(gamma) eq 0 then gamma = 1.
if n_elements(atm) eq 0 then atm = 1
if keyword_set(massic) then begin
    massic = 1
    volumic = 0
    title2 = ' (per unit mass)'
endif else if keyword_set(volumic) then begin
    massic = 0
    volumic = 1
    title2 = ' (per unit volume)'
endif else begin
    volumic = 0
    massic = 0
    title2 = ' (per unit length)'
endelse
if n_elements(profile) eq 0 then profile = 0

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1
k = p.k0 * p.lambda ^ (indgen (nk) + p.nmin)
logk = alog10 (k)
z = indgen (p.nz) * p.dz

; use out_profheat to plot average profile of heating
if profile eq 1 then begin
    readarray, 'out_profheat', p.nz, ph
    if atm then begin
        readarray, 'out_rho', p.nz, rho
        readarray, 'out_aexp', p.nz, aexp
    endif else begin
        rho = 1.
        aexp = 1.
    endelse
    if (volumic or massic) then ph *= p.k0 ^ 2 / (!pi ^ 3 * aexp)
    if (           massic) then ph /= rho
    if n_elements(xrange) ne 2 then xrange = [min (z, max=tmp), tmp]
    if n_elements(yrange) ne 2 then yrange = [min (ph, max=tmp), tmp]
    plot, z, ph, xtitle='Position z', ytitle='Average H(z)'+title2, $
      xrange=xrange, yrange=yrange
    return
endif

nt = n_elements(time) 
if nt eq 0 then begin
    time = indgen (p.nrecsp)
endif else if nt eq 1 then begin
    time = [time]
endif
time = time (uniq (time > 0 < (p.nrecsp - 1)))
nt = n_elements(time)
if nt eq 0 then begin
    message, 'No valid time was provided, should be between 0 and ' + $
      string_ns (p.nrecsp - 1), /info
    return
endif

if atm eq 0 then begin
    outspecu = 'out_specu'
    outspecb = 'out_specb'
    outtime  = 'out_time'
    readarray, outtime,  p.nrecen, t
    realtime = t[(time+1) * (p.nrecen / p.nrecsp) - 1] 

    readarray, outspecu, nk * p.nz * p.nrecsp, tmp
    tmp = reform (tmp, nk, p.nz, p.nrecsp, /overwrite)
    s = dblarr (p.nz, p.nrecsp)
    for i=0, p.nz - 1 do begin
        for j=0, p.nrecsp - 1 do begin
            s[i, j] = total (tmp[*, i, j] * k * k * p.nu)
        endfor
    endfor
    readarray, outspecb, nk * p.nz * p.nrecsp, tmp
    tmp = reform (tmp, nk, p.nz, p.nrecsp, /overwrite)
    for i=0, p.nz - 1 do begin
        for j=0, p.nrecsp - 1 do begin
            s[i, j] = s[i, j] + total (tmp[*, i, j] * k * k * p.eta)
        endfor
    endfor
    s = s[*, time]
endif else begin
    realtime = dblarr (nt)
    outt2i = 'out_time2i'
    outsimstate = 'out_simstate_'
    readarray_s, outt2i, p.nrecsp, t2i
    readarray, 'out_rho', p.nz, rho
    readarray, 'out_aexp', p.nz, aexp
    ; if out_simstate_nnn have only some planes, we need have rho, etc.
    ; for these planes only
    tmpp = sa_params_get (outsimstate + t2i[time[0]], atm=atm)
    nz = tmpp.nz
    iz = (p.nz / nz) * lindgen (nz)
    z = z[iz]
    rho = rho[iz]
    aexp = aexp[iz]
    s = dblarr (nt, nz)
    for it=0, nt-1 do begin
        tmpp = sa_params_get (outsimstate + t2i[time[it]], shells=shells, $
                              atm=atm)
        realtime[it] = tmpp.time
        ; get energy for u
        tmp = sa_shells_to_fields (shells, rho=rho, aexp=aexp, atm=atm, /uu, $
                                   /energy, massic=massic, volumic=volumic, $
                                   k0=p.k0)
        ; compute power of dissipation
        ; (aexp is needed to take into account
        ; the variation of k^2 with altitude)
        s[it, *] = reform ((k ^ 2) # tmp) * p.nu / aexp
        ; same for b
        tmp = sa_shells_to_fields (shells, rho=rho, aexp=aexp, atm=atm, /bb, $
                                   /energy, massic=massic, volumic=volumic, $
                                   k0=p.k0)
        s[it, *] += reform ((k ^ 2) # tmp) * p.eta / aexp
    endfor
    s *= 2.d  ; power, not energy (as computed by sa_shells_to_fields)
endelse

if profile eq 2 then begin
    sp = total (s, 1) / (size (s))[1]
    if keyword_set(log) then begin   
        w = where (sp gt 0,  count)
        if count ne 0 then begin
            ms = min (sp[w]) > 1d-50
            w = where (s lt ms,  count)
            if count ne 0 then sp[w] = ms / 2.d
        endif
        sp = alog10 (sp) > (-50)
    endif
    plot, z, sp, xtitle='Position z', ytitle='Average H(z)'+title2

stop

    return
endif
if keyword_set(log) then begin   
    w = where (s gt 0,  count)
    if count ne 0 then begin
        ms = min (s[w]) > 1d-50
        w = where (s lt ms,  count)
        if count ne 0 then s[w] = ms / 2.d
    endif
    s = alog10 (s) > (-50)
endif
print, "Range: ", min (s, max=tmp), tmp
if n_elements(cutt) ne 0 then begin
    nc = n_elements(cutt)
    for i=0, nc-1 do begin
        if cutt[i] ge nt then begin
            message, /info, "Time index chosen for cut is to high " + $
              "(maximum allowed index is " + string_ns (nt) + ")", /info
            continue
        endif
        if n_elements(xtitle) eq 0 then xtitle = 'Position z'
        if n_elements(ytitle) eq 0 then $
          ytitle = 'Heating H(t=t0,z)' + title2
        if n_elements(title) eq 0 then $
          title = "t=" + string_ns (realtime[cutt[i]])
        plot, z, s[*, cutt[i]], title=title, xtitle=xtitle, ytitle=ytitle
    endfor
endif else if n_elements(cutz) ne 0 then begin
    nc = n_elements(cutz)
    for i=0, nc-1 do begin
        if cutz[i] ge nt then begin
            message, /info, "z index chosen for cut is to high " + $
              "(maximum allowed index is " + string_ns (p.nz) + ")", /info
            continue
        endif
        if n_elements(xtitle) eq 0 then xtitle = 'Time t'
        if n_elements(ytitle) eq 0 then $
          ytitle = 'Heating H(t,z=z0)' + title2
        if n_elements(title) eq 0 then title = "z0=" + string_ns (z[cutz[i]])
        plot, realtime, s[*, cutz[i]], title=title, $
          xtitle=xtitle, ytitle=ytitle
    endfor
endif else begin
    if n_elements(xtitle) eq 0 then xtitle = 'Time t'
    if n_elements(ytitle) eq 0 then ytitle = 'Position z'
    if keyword_set(ddd) then begin
        if n_elements(range) eq 2 then begin
            ; normalize gray scale
            b1 = min (range, max=b2)
            shades = bytscl (s < b2 > b1)
        endif else shades = bytscl (s)
        if n_elements(title) eq 0 then $
          title = 'Heating function H(t,z)' + title2
        shade_surf, s, realtime, z, shades=shades, az=20, ax=60, $
          xtitle=xtitle, ytitle=ytitle, ztitle=title, /xstyle, pixels=1000
    endif else begin 
        if n_elements(range) eq 2 then begin
        ; normalize to the given boundary values
        ; (reduce the range of displayed values)
            b1 = min (range, max=b2)
            s = bytscl (s < b2 > b1)
        endif else s = bytscl (s)
        ; 2D plot (t, z) of heating function
        if n_elements(title) eq 0 then $
          title = 'Heating function H(t,z)' + title2
        plot_image_eb, title=title, s, realtime, z, $
          norm=norm, xtitle=xtitle, ytitle=ytitle, $
          nointerpol=nointerpol, gamma=gamma
    endelse
endelse

end
