;+
; NAME:
;  string_ns
;
; PURPOSE:
;  Converts number to a string with no leading or trailing spaces
;
; CALLING SEQUENCE:
;  s = string_ns (n,format=format)
;
; INPUTS:
;  n: number
;  format: format for string conversion
;
; OUTPUTS:
;  s: string
;
; PROJECT:
;  PhD/gen
;
; AUTHORS:
;  EB Eric Buchlin eric.buchlin@ias.fr
;
; MODIFICATION HISTORY:
;  25 Jul 2003 EB Creation
;-

function string_ns, n, format=format
return, strcompress (string (n, format=format), /remove_all)
end
