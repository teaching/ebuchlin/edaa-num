;+
; NAME:
;  sa_params_get
;
; PURPOSE:
;  Get parameters used for a shell_loop simulation
;
; CALLING SEQUENCE:
;  p = sa_params_get ("simstate.dat", shells=shells, time=time, forc=forc,
;    atm=atm, /unformatted)
;
; INPUTS:
;  "simstate.dat" : simulation state file, from which parameters will be
;    extracted 
;
; KEYWORD PARAMETERS:
;  /unformatted: file is "F77" unformatted (binary) 
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;  p : sa_params structure containing the parameters
;
; OPTIONAL OUTPUTS:
;  shells: last shell values (in all planes). Dimensions: (kperp, z,
;    z+/zm)
;  forc: forcing coefficients
;  time: the time of the simulation. Deprecated; use p.time instead
;
; LANGUAGE:
;  GDL: bypass bug in GDL<=0.9.3 when reading arrays
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  13 Dec 2002 EB Creation
;  08 Nov 2003 EB Added shells keyword
;  17 Jun 2005 EB Added support for p.time
;  10 Oct 2005 AV/EB Added support for shell_atm
;  18 Nov 2005 EB/AV Added /unformatted keyword
;  17 Feb 2006 EB Added output of forcing coefficients
;-

function sa_params_get, file, shells=shells, time=time, forc=forc, atm=atm, $
                        unformatted=unformatted

if n_elements(atm) eq 0 then atm = 1
p = {sa_params}
tmpd = 0.d
tmpt = ""
tmpl = 0L

if n_elements(file) eq 0 then file = "simstate.dat"

if keyword_set(unformatted) then begin
    openr, u, file, err=err, /get_lun, /f77_unformatted
    if err ne 0 then begin
        message, "Couldn't open file " + file, /info
        return, p
    endif
    readu, u, tmpl
    p.nmin   = tmpl
    readu, u, tmpl
    p.nmax   = tmpl 
    readu, u, tmpl
    p.nz     = tmpl
    readu, u, tmpd
    p.k0     = tmpd
    readu, u, tmpd
    p.lambda = tmpd
    readu, u, tmpd
    p.nu     = tmpd
    readu, u, tmpd
    p.eta    = tmpd
    readu, u, tmpd
    p.eps    = tmpd
    readu, u, tmpd
    p.epsm   = tmpd
    readu, u, tmpd
    p.b0     = tmpd
    readu, u, tmpd
    p.dz     = tmpd
    readu, u, tmpl
    p.nt     = tmpl
    readu, u, tmpd              ; delta_t
    readu, u, tmpl
    p.nrecen = tmpl
    readu, u, tmpl
    p.nrecsf = tmpl
    readu, u, tmpl
    p.nrecsp = tmpl

    n = 2L * (p.nmax - p.nmin + 1) * p.nz * 2L
    shells = dblarr (n)
    for i=0L, n-1L do begin
        readu, u, tmpd
        shells[i] = tmpd
    endfor
    shells = reform (shells, 2L, p.nmax - p.nmin + 1, p.nz, 2L, /overwrite)

    readu, u, tmpd
    time = tmpd
    p.time = tmpd

    if atm then begin
        readu, u, tmpd
        p.tstar = tmpd
        readu, u, tmpl
        nkforce = tmpl
        for i=0, 8L * nkforce - 1 do readu, u, tmpd
        readu, u, tmpd
        p.rho0   = tmpd
        readu, u, tmpd
        p.h0     = tmpd
    endif
endif else begin 
    openr, u, file, err=err, /get_lun
    if err ne 0 then begin
        message, "Couldn't open file " + file, /info
        return, p
    endif
    readf, u, tmpl
    p.nmin   = tmpl
    readf, u, tmpl
    p.nmax   = tmpl 
    readf, u, tmpl
    p.nz     = tmpl
    readf, u, tmpd
    p.k0     = tmpd
    readf, u, tmpd
    p.lambda = tmpd
    readf, u, tmpd
    p.nu     = tmpd
    readf, u, tmpd
    p.eta    = tmpd
    readf, u, tmpd
    p.eps    = tmpd
    readf, u, tmpd
    p.epsm   = tmpd
    readf, u, tmpd
    p.b0     = tmpd
    readf, u, tmpd
    p.dz     = tmpd
    readf, u, tmpl
    p.nt     = tmpl
    readf, u, tmpd              ; delta_t
    readf, u, tmpl
    p.nrecen = tmpl
    readf, u, tmpl
    p.nrecsf = tmpl
    readf, u, tmpl
    p.nrecsp = tmpl

    shells = dblarr (2, p.nmax - p.nmin + 1, p.nz, 2)
    readf, u, shells
    ; seek (compensate for GDL bug)
    for ii=0L, 4L * (p.nmax - p.nmin + 1L) * p.nz - 2L do readf, u, tmpd
    shells = complex (shells[0, *, *, *], shells[1, *, *, *], /double)
    shells = reform (shells, p.nmax - p.nmin + 1, p.nz, 2)

    readf, u, tmpd
    time = tmpd
    p.time = tmpd

    if atm then begin
        readf, u, tmpd
        p.tstar = tmpd

        readf, u, tmpl
        nkforce = tmpl
        forc = dblarr (2, 2, nkforce, 2)
        readf, u, forc
        ; seek (compensate for GDL bug)
        for ii=0L, 8L * nkforce - 2L do readf, u, tmpd
        forc = complex (forc[0, *, *, *], forc[1, *, *, *], /double)
        forc = reform (forc, 2, nkforce, 2)
        readf, u, tmpd
        p.rho0   = tmpd
        readf, u, tmpd
        p.h0     = tmpd
    endif
endelse

free_lun, u

return, p

end
