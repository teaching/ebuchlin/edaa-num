;+
; NAME:
;  sa_plot_energy
;
; PURPOSE:
;  Read necessary information and plot energy in box
;
; CALLING SEQUENCE:
;  sa_plot_energy, sf, /uu, /bb, /ub, /tot, /xlog, /ylog, 
;       xrange=xrange, yrange=yrange, minmax=minmax, atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb and /ub is needed:
;  /uu: plot only velocity spectra
;  /bb: plot only magnetic field spectra
;  /ub: plot both, on same plot
;  /tot: plot total energy (sum of u and b)
;  /xlog: logarithmic x-axis
;  /ylog: logarithmic y-axis
;  xrange: range on x-axis
;  yrange: range on y-axis
;  minmax: if set, the plot contains minmax couples of
;    points, with the min and max of the time series over some
;    interval around the point (saves memory or postscript file size)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; AUTHORS:
;  EB: Eric Buchlin
;
; MODIFICATION HISTORY:
;  11 Nov 2003 EB Creation
;  17 Dec 2003 EB Added /tot and /xlog keywords
;  21 Jan 2004 EB Added xrange and yrange keywords
;  05 Aug 2004 EB Added minmax keyword
;  07 Nov 2005 EB Adapted for shell-atm
;-

pro sa_plot_energy, sf, uu=uu, bb=bb, ub=ub, tot=tot, xlog=xlog, $
                    ylog=ylog, xrange=xrange,  yrange=yrange, $
                    minmax=minmax, atm=atm

if keyword_set(tot) or keyword_set (ub)  then begin
    uu = 1
    bb = 1
endif else if keyword_set(uu)  then begin
    uu = 1
    bb = 0
endif else if keyword_set(bb) then begin
    bb = 1
    uu = 0
endif else begin
    uu = 1
    bb = 1
endelse
if n_elements(atm) eq 0 then atm = 1
if keyword_set(xlog) then xlog = 1 else xlog = 0

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)

outtime = 'out_time'
outenu  = 'out_enu'
outenb  = 'out_enb'

readarray, outtime, p.nrecen, t
nt = p.nrecen
if n_elements(minmax) eq 0 then minmax = 100000000L ; do not use minmax

if nt ge minmax then t = congrid (t, 2 * minmax)
if uu and not keyword_set(tot) then begin
    readarray, outenu, p.nrecen, su
    if nt ge minmax then begin
        step = double (nt - 1) / minmax
        suu = dblarr (2 * minmax)
        for i = 0, minmax - 1 do begin
            suu[2 * i] = max (su[long(i * step) : long((i + 1L) * step)], $
                              min=tmp)
            suu[2 * i + 1] = tmp
        endfor
        su = suu
    endif
endif
if bb and not keyword_set(tot) then begin
    readarray, outenb, p.nrecen, sb
    if nt ge minmax then begin
        step = double (nt - 1) / minmax
        sbb = dblarr (2 * minmax)
        for i = 0, minmax - 1 do begin
            sbb[2 * i] = max (sb[long(i * step) : long((i + 1L) * step)], $
                              min=tmp)
            sbb[2 * i + 1] = tmp
        endfor
        sb = sbb
    endif
endif
if keyword_set(tot) then begin
    readarray, outenu, p.nrecen, su
    readarray, outenb, p.nrecen, sb
    sb = sb + su
    if nt ge minmax then begin
        step = double (nt - 1) / minmax
        sbb = dblarr (2 * minmax)
        for i = 0, minmax - 1 do begin
            sbb[2 * i] = max (sb[long(i * step) : long((i + 1L) * step)], $
                              min=tmp)
            sbb[2 * i + 1] = tmp
        endfor
        sb = sbb
    endif
endif

if n_elements(xrange) ne 2 then begin
    xrange = [min(t, max=tmp), tmp]
    ix1 = 0
    ix2 = n_elements(t) - 1 
;     if nt lt minmax then begin
;         if uu then suu = su
;         if bb then sbb = sb
;     endif
endif else begin
    ix1 = min (where (t ge xrange[0], count))
    if count eq 0 then ix1 = 0
    ix2 = max (where (t le xrange[1], count))
    if count eq 0 then ix2 = n_elements(t) - 1 
;     if uu then suu = su[ix1:ix2]
;     if bb then sbb = sb[ix1:ix2]
endelse
if uu and not bb then begin
    if n_elements(yrange) ne 2 then yrange = [min(su, max=tmp), tmp]
    plot, t[ix1:ix2], su[ix1:ix2], xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle='Energy (u)', xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else if bb and not uu then begin
    if n_elements(yrange) ne 2 then yrange = [min(sb, max=tmp), tmp]
    plot, t[ix1:ix2], sb[ix1:ix2], xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle='Energy (b)', xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else if keyword_set(tot)  then begin
    if n_elements(yrange) ne 2 then yrange = [min(sb, max=tmp), tmp]
    plot, t[ix1:ix2], sb[ix1:ix2], xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle='Energy', xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else begin ; /ub
    if n_elements(yrange) ne 2 then begin 
        yr1  = min (su, max=yr2 )
        yr1b = min (sb, max=yr2b)
        yr1 = min ([yr1, yr1b])
        yr2 = max ([yr2, yr2b])
        yrange = [yr1, yr2]
    endif
    plot, t[ix1:ix2], su[ix1:ix2], xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle='Energy (u and b)', xstyle=1, ystyle=3, $
      xrange=xrange, yrange=yrange
    oplot, t[ix1:ix2], sb[ix1:ix2]
endelse
end

