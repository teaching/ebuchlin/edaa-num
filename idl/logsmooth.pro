;+
; NAME:
;  logsmooth
;
; PURPOSE:
;  Smooth an large array with logarithmic bins
;
; CALLING SEQUENCE:
;  b = logsmooth (a, logbin, bincenter=bincenter)
;
; INPUTS:
;  a: array
;
; OPTIONAL INPUTS:
;  logbin: logarithmic binsize in decades. Default is 0.1
;
; OUTPUTS:
;  b: smoothed array. b has same size as a, but its values are
;    constant on each bin
;
; OPTIONAL OUTPUTS:
;  bincenter: array of indices of centers of bins
;
; RESTRICTIONS:
;  1D arrays
;
; LANGUAGE:
;   IDL
;
; PROJECT:
;   PhD/IAS
;
; AUTHORS:
;   Eric Buchlin, IAS, eric.buchlin@ias.u-psud.fr
;
; MODIFICATION HISTORY:
;   05 Nov 2003 EB Creation
;-

function logsmooth, a, logbin, bincenter=bincenter
if n_elements(logbin) eq 0 then logbin = 0.1
n = n_elements(a)
bincenter = dblarr (alog10 (n-1) / logbin + 1)
b = a
i = 0L
j2 = 0L
while j2 lt n - 1L do begin
    j1 = floor (10. ^ (i * logbin))
    j2 = floor (10. ^ ((i+1) * logbin) - 1) < (n-1L)
    if j2 gt j1 then begin
        nj = j2 - j1 + 1L
        b[j1:j2] = total (b[j1:j2]) / nj
    endif
    bincenter[i] = (j1 + j2) / 2L
    i = i + 1L
endwhile
bincenter = bincenter[uniq (bincenter)]
return, b
end

