;+
; NAME:
;  sa_plot_cutk
;
; PURPOSE:
;  Read necessary information and plot cut of (z,k,t) data cube of
;  field (square amplitude --- massic energy) for a given k
;
; CALLING SEQUENCE:
;  sa_plot_cutk, k, sf, /uu, /bb, /zp, /zm, /ch, time=time, /log, 
;    /nointerpol, range=range,
;    gamma=gamma, atm=atm, /energy
;
; INPUTS:
;  k: index of kperp used to plot the cut 
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default) to get paramaters (data will be
;  taken from other out_simstate_* files)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb, /zm and /zp is needed:
;  /uu: plot only velocity spectra
;  /bb: plot only magnetic field spectra
;  /zm: plot only zm spectra
;  /zp: plot only zp spectra
;  /ch: plot only cross helicity
;  time: arrays of indices of times (from out_time2i) to plot
;  /log: shows intensities in logarithmic scale
;  /nointerpol: do not interpolate coordinates in plot_image (this may
;    cause axes values to be false)
;  range: the range of the color scale will be reduced (but not extended)
;    to fit this range
;  gamma: gamma factor for displaying image (in the case of a cut).
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;  /energy: if set, read density in file and plot volumic energy
;    instead of massic energy
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric.buchlin@ias.u-psud.fr
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  10 Oct 2005 EB/AV Forked from sa_plot_spectra
;  07 Nov 2005 EB Adapted for shell-atm
;  27 Mar 2006 EB Take aexp into account
;  05 Sep 2014 EB Added /ch
;-

pro sa_plot_cutk, k, sf, uu=uu, bb=bb, zm=zm, zp=zp, ch=ch, time=time, $
                     log=log, nointerpol=nointerpol, range=range, $
                     gamma=gamma, atm=atm, $
                     energy=energy

sa_plot_cuts_tidyoptions, sf, uu=uu, bb=bb, zp=zp, zm=zm, ch=ch, $
  nointerpol=nointerpol, atm=atm, field=field

p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1
logk = alog10 (p.k0 * p.lambda ^ (lindgen (nk) + p.nmin))

if k lt p.nmin or k gt p.nmax then begin
    message, 'k out of bounds', /info
    k = k < p.nmax > p.nmin
endif

nt = n_elements(time) 
if nt eq 0 then begin
    time = lindgen (p.nrecsp)
endif
time = time > 0 < (p.nrecsp - 1)
time = time [uniq (time)]
nt = n_elements (time)
if nt eq 0 then begin
    message, 'No valid time was provided, should be between 0 and ' + $
      string_ns (p.nrecsp - 1), /info
    return
endif
realtime = dblarr (nt)

outt2i = 'out_time2i'
outsimstate = 'out_simstate_'

readarray_s, outt2i, p.nrecsp, t2i


; use nz and dz from out_simstate_nnn, not from simstate.dat
tmpp = sa_params_get (outsimstate + t2i[time[0]], atm=atm)
z = lindgen (tmpp.nz) * tmpp.dz
if keyword_set(energy) then begin
    readarray, 'out_rho', p.nz, rho
    readarray, 'out_aexp', p.nz, aexp
    step = p.nz / tmpp.nz
    selectz = step * lindgen (tmpp.nz)
    rho  =  rho[selectz]
    aexp = aexp[selectz]
endif else begin
    rho = 1.d
    aexp = 1.d
endelse

s = dblarr (nt, tmpp.nz)
for it=0, nt-1 do begin
    tmpp = sa_params_get (outsimstate + t2i[time[it]], shells=shells, atm=atm)
    realtime[it] = tmpp.time
    s[it, *] = sa_shells_to_fields (shells[k, *, *], rho=rho, aexp=aexp, $
                         uu=uu, bb=bb, zp=zp, zm=zm, ch=ch, /energy, k0=p.k0, atm=atm)
endfor

sa_plot_cuts_normalize, s, log=log, range=range, norm=norm, atm=atm
if norm eq 0 then begin
  print, "Color range: ", min (range, max=tmp), tmp, "  Data range: ", min (s, max=tmp), tmp
endif else begin
  print, "Data and color range: ", min (s, max=tmp), tmp
endelse

plot_image_eb, s, realtime, z, $
  gamma=gamma, norm=norm, xtitle='t', ytitle='z', $
  title=field+', log10 k='+string_ns(logk[k])+' (ik='+string_ns(k)+')', $
  nointerpol=nointerpol

end
