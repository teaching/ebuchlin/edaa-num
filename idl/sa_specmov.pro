;+
; NAME:
;  sa_specmov
;
; PURPOSE:
;  Create movie of average spectra, read in out_specavu and
;  out_specavb
;  Outputs PNG files in current directory, that can be converted to a
;  movie: with mplayer/mencoder, use:
;  mencoder "mf://*.png" -fps 25 -ovc lavc -lavcopts 
;    vcodec=mpeg4:vbitrate=600000 -ffourcc DX50 -o specmov.avi
;
; CALLING SEQUENCE:
;  sa_specmov, sf, xrange=xrange, yrange=yrange, time=time, /ts, /usemencoder, 
;     atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  xrange: range for x axis
;  yrange: range for y axis
;  time: indices of time output to use (default: all (0 to p.nrecsp-1))
;  /ts: plot time series of energy and dissipation in separate panels
;  /usemencoder: try to use mencoder to encode AVI movie
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUTS:
;
; SIDE EFFECTS:
;  Show plots of spectra
;  Create PNG files (need to be deleted manually in the end)
;  Create AVI movie (if /mencoder is used and mencoder available)
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  14 Nov 2005 EB Creation
;-

pro sa_specmov, sf, xrange=xrange, yrange=yrange, time=time, ts=ts, $
                usemencoder=usemencoder, atm=atm
if n_elements(sf) eq 0 then sf = "simstate.dat"
if n_elements(atm) eq 0 then atm = 1
if keyword_set(ts) then ts = 1 else ts = 0
if keyword_set(usemencoder) then usemencoder = 1 else usemencoder = 0
p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1L
k = p.k0 * p.lambda ^ (p.nmin + lindgen (nk))
logk = alog10 (k)
readarray, 'out_dissu', p.nrecen, d
readarray, 'out_dissb', p.nrecen, db
readarray, 'out_enu', p.nrecen, e
readarray, 'out_enb', p.nrecen, eb
readarray, 'out_time',  p.nrecen, t
d = d + db
e = e + eb
; times for spectra (try to guess them, if out_time2 isn't available)
readarray_s, 'out_time2i', p.nrecsp, itspec, err=err
if err eq 0 then itspec =  long (itspec) else $
  itspec = lindgen (p.nrecsp) * (p.nrecen / p.nrecsp)
readarray, 'out_time2', p.nrecsp, tspec, err=err
if err ne 0 then tspec = t[itspec]
; indices of times to use
nt = n_elements(time) 
if nt eq 0 then begin
    time = indgen (p.nrecsp)
endif else if nt eq 1 then begin
    time = [time]
endif
time = time > 0L < (p.nrecsp - 1L)
time = time [uniq (time)]
nt = n_elements (time)
if nt eq 0 then begin
    message, 'No valid time was provided, should be between 0 and ' + $
      string_ns (p.nrecsp - 1), /info
    return
endif

; open average spectra file (and read them progressively later)
su = dblarr (nk)
sb = dblarr (nk)
fileu = 'out_specavu'
fileb = 'out_specavb'
openr, fu, fileu, /get_lun, err=err
if err ne 0 then message, "File " + fileu + " not readable"
openr, fb, fileb, /get_lun, err=err
if err ne 0 then message, "File " + fileb + " not readable"

if ts then begin
;window, xsize=1000, ysize=700
    pos1 = [.07, .07, .70, .95]
    pos2 = [.75, .56, .95, .95]
    pos3 = [.75, .07, .95, .51]
    ; factors for time series (to have order of magnitude of 1 on axis)
    dlogfactor = -floor (alog10 (max (d))) + 1L
    elogfactor = -floor (alog10 (max (e))) + 1L
endif else begin
    pos1 = [.1, .08, .97, .97]
endelse

oldp = !p
oldx = !x
oldy = !y
if n_elements(xrange) ne 2 then xrange = [min (logk, max=tmp), tmp]
if n_elements(yrange) ne 2 then yrange = [-15, 5]
!x.style = 3
!y.style = 1

eyranged = dblarr (100)
eyrangee = dblarr (100)
for is=0L, p.nrecsp-1 do begin

    ; read average spectra
    readf, fu, su
    readf, fb, sb
    sutot = alog10 (su)
    sbtot = alog10 (sb)

    ; throw away if is is not in time array
    w = where (time eq is, count)
    if count eq 0 then continue

    print, is, w[0], tspec[is]

    ; plot time series panels
    if ts then begin
        ; x range
        exrange = tspec[is] + [-1, 1]
        ; indices of local window
        iloc = where (t ge exrange[0] and t le exrange[1])
        tloc = t[iloc]
        dloc = d[iloc] * 10.d ^ dlogfactor
        eloc = e[iloc] * 10.d ^ elogfactor
    
        ; y range (by power of 10) for energy time series, with memory
        ; (to avoid frequent changes of scales)
        eyrangee = shift (eyrangee, 1)
        eyrangee[0] = 10.d ^ ceil (alog10 (max (eloc)))
        !p.noerase = 0
        ; plot energy time series
        plot, tloc, eloc, xrange=exrange, yrange=[0, eyrangee[0]], $
          position=pos2, $
          ytitle='Energy x 10^'+string_ns (elogfactor, format='(I)')
        oplot, replicate (tspec[is], 2), [0, max (eyrangee)], linestyle=1
        !p.noerase = 1

        ; y range (by power of 10) for dissipation time series, with memory
        eyranged = shift (eyranged, 1)
        eyranged[0] = 10.d ^ ceil (alog10 (max (dloc)))
        plot, tloc, dloc, xrange=exrange, yrange=[0, max (eyranged)], $
          position=pos3, xtitle='Time', $
          ytitle='Dissipation power x 10^'+string_ns (dlogfactor, format='(I)')
        oplot, replicate (tspec[is], 2), [0, eyranged[0]], linestyle=1
    endif

;     c = get_kbrd(1)
;     if c eq 's' then stop
    ; plot spectra
    plot, logk, sutot, xrange=xrange, yrange=yrange, position=pos1, $
      xtitle='log10 k_perp', ytitle='log10 Eu, Eb'
    oplot, logk, sbtot, linestyle=2

    im = tvrd ()
    ims = size (im)
    ; make RGB image (so that PNG file is DirectClass and not PseudoClass,
    ; which is not recognized by mencoder
    imrgb = reform ([[im], [im], [im]], ims[1], ims[2], 3)
    imrgb = transpose (imrgb, [2, 0, 1])
    pngfile = "specmov_" + string (is, format='(I6.6)') + ".png"
    write_png, pngfile, imrgb
    
endfor

free_lun, fu
free_lun, fb
!p = oldp
!x = oldx
!y = oldy

if usemencoder then begin
    spawn, 'mencoder "mf://*.png" -fps 25 -ovc lavc -lavcopts ' + $
      'vcodec=mpeg4:vbitrate=600000 -ffourcc DX50 -o specmov.avi', $
      exit_status=err
    if err ne 0 then begin
        message, 'mencoder failed or not found', /info
        message, 'Please run it manually', /info
        print, 'mencoder "mf://*.png" -fps 25 -ovc lavc -lavcopts ' + $
          'vcodec=mpeg4:vbitrate=600000 -ffourcc DX50 -o specmov.avi'
    endif
endif


end
