; normalize value ranges for sa_plot_cut*

pro sa_plot_cuts_normalize, s, log=log, range=range, norm=norm, atm=atm

if n_elements(atm) eq 0 then atm = 1
if keyword_set(log) then begin   
    w = where (s gt 0,  count)
    if count ne 0 then begin
        ms = min (s[w]) > 1d-50
        w = where (s lt ms,  count)
        if count ne 0 then s[w] = ms / 2.d
    endif
    s = alog10 (s) > (-50)
endif
if n_elements(range) ge 2 then begin
    b1 = min (range, max=b2)
    s = bytscl (s, min=b1, max=b2)
    norm = 0  ; no further normalization in plot_image_eb
endif else norm = 1

end

