;+
; NAME:
;  readarray_s
;
; PURPOSE:
;  Reads a string array in a file
;
; CALLING SEQUENCE:
;  Procedure syntax:
;   readarray_s, file, n, s, err=err, /quiet
;  Function syntax:
;   s = readarray_s (file, n, err=err, /quiet)
;
; INPUTS:
;  file: file containing array
;  n:    number of records
;
; OUTPUTS:
;  s:    array
;
; OPTIONAL OUTPUT
;  err: error opening the file
;
; BUGS:
;  This file must be explicitely compiled before using the function
;  syntax
;
; PROJECT:
;  PhD/gen (originally shell)
;
; AUTHORS:
;  EB Eric Buchlin
;
; MODIFICATION HISTORY:
;  04 Nov 2005 EB Forked from readarray
;-

pro readarray_s, file, n, s, err=err, quiet=quiet
if n le 0 then begin
    err = 1
    return
endif
s = strarr (n)
openr, u, file, /get_lun, err=err
if err ne 0 then begin
    if not keyword_set(quiet) then $
      message, "File " + file + " not readable", /info
    return
endif
readf, u, s
free_lun, u
return
end

function readarray_s, file, n, err=err
if n le 0 then begin
    err = 1
    return, 0
endif
s = strarr (n)
openr, u, file, /get_lun, err=err
if err ne 0 then begin
    if not keyword_set(quiet) then $
      message, "File " + file + " not readable", /info
    return, 0
endif
readf, u, s
free_lun, u
return, s
end
