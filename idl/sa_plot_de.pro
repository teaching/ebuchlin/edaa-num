;+
; NAME:
;  sa_plot_de
;
; PURPOSE:
;  Read necessary information and plot energy dissipation time series
;
; CALLING SEQUENCE:
;  sa_plot_de, sf, /uu, /bb, /ub, /tot, /xlog, /ylog,
;     xrange=xrange, yrange=yrange, minmax=minmax, ytitle=ytitle, atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb and /ub is needed:
;  /uu: plot only velocity spectra
;  /bb: plot only magnetic field spectra
;  /ub: plot both, on same plot
;  /tot: plot total energy (sum of u and b)
;  /xlog: logarithmic x-axis
;  /ylog: logarithmic y-axis
;  xrange: range of x-axis
;  yrange: range of y-axis
;  minmax: if set, the plot contains minmax couples of
;    points, with the min and max of the time series over some
;    interval around the point (saves memory or postscript file size)
;  ytitle: title for y-axis
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  20 Jan 2004 EB Forked from sa_plot_energy, added xrange and yrange
;  12 May 2004 EB Added minmax keyword
;  01 Sep 2004 EB Added ytitle keyword
;  07 Nov 2005 EB Adapted for shell-atm
;-

pro sa_plot_de, sf, uu=uu, bb=bb, ub=ub, tot=tot, xlog=xlog, ylog=ylog, $
                xrange=xrange, yrange=yrange, minmax=minmax, ytitle=ytitle, $
                atm=atm

if keyword_set(tot) or keyword_set (ub)  then begin
    uu = 1
    bb = 1
endif else if keyword_set(uu)  then begin
    uu = 1
    bb = 0
endif else if keyword_set(bb) then begin
    bb = 1
    uu = 0
endif else begin
    uu = 1
    bb = 1
endelse

if keyword_set(xlog) then xlog = 1 else xlog = 0
if keyword_set(ylog) then ylog = 1 else ylog = 0
if n_elements(atm) eq 0 then atm = 1

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)

outtime = 'out_time'
outdu  = 'out_dissu'
outdb  = 'out_dissb'

readarray, outtime, p.nrecen, t
nt =  n_elements(t) 

if n_elements(minmax) ne 0 then begin
    if xlog ne 0 then begin
        message, "Warning: use of minmax together with /xlog is" + $
          " not advised", /info
    endif
endif else begin
    minmax = 100000000L   ; do not use minmax
endelse

if nt ge minmax then t = congrid (t, 2 * minmax)
if uu then begin
    readarray, outdu, p.nrecen, su
    if nt ge minmax then begin
        step = double (nt - 1) / minmax
        suu = dblarr (2 * minmax)
        for i = 0L, minmax - 1 do begin
            suu[2 * i] = max (su[long(i * step) : long((i + 1L) * step)], $
                              min=tmp)
            suu[2 * i + 1] = tmp
        endfor
        su = suu
    endif
endif
if bb then begin
    readarray, outdb, p.nrecen, sb
    if nt ge minmax then begin
        step = double (nt - 1) / minmax
        sbb = dblarr (2 * minmax)
        for i = 0L, minmax - 1 do begin
            sbb[2 * i] = max (sb[long(i * step) : long((i + 1L) * step)], $
                              min=tmp)
            sbb[2 * i + 1] = tmp
        endfor
        sb = sbb
    endif
endif

if not keyword_set(xrange) then begin
    xrange = [min (t, max=tmp), tmp]
    ix1 = 0
    ix2 = n_elements(t) - 1 
    if nt lt minmax then begin
        if uu then suu = su
        if bb then sbb = sb
    endif
endif else begin
    ix1 = min (where (t ge xrange[0], count))
    if count eq 0 then ix1 = 0
    ix2 = max (where (t le xrange[1], count))
    if count eq 0 then ix2 = n_elements(t) - 1 
    if uu then suu = su[ix1:ix2]
    if bb then sbb = sb[ix1:ix2]
endelse

if uu and not bb then begin
    if not keyword_set(yrange) then yrange = [min (suu,  max=tmp), tmp]
    if n_elements(ytitle) eq 0 then ytitle = '!7e!X (u)'
    plot, t[ix1:ix2], suu, xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle=ytitle, xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else if bb and not uu then begin
    if not keyword_set(yrange) then yrange = [min (sbb,  max=tmp), tmp]
    if n_elements(ytitle) eq 0 then ytitle = '!7e!X (b)'
    plot, t[ix1:ix2], sbb, xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle=ytitle, xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else if keyword_set(tot)  then begin
    if not keyword_set(yrange) then yrange = [min (suu + sbb,  max=tmp), tmp]
    if n_elements(ytitle) eq 0 then ytitle = '!7e!X'
    plot, t[ix1:ix2], suu + sbb, xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle=ytitle, xstyle=1, ystyle=3, xrange=xrange, yrange=yrange
endif else begin ; /ub
    if not keyword_set(yrange) then begin
        yr1  = min (suu, max=yr2 )
        yr1b = min (sbb, max=yr2b)
        yrange = [min ([yr1, yr1b]), max ([yr2, yr2b])]
    endif
    plot, t[ix1:ix2], suu, xlog=xlog, ylog=ylog, xtitle='t', $
      ytitle='!7e!X (u and b)', xstyle=1, ystyle=3, $
      xrange=xrange, yrange=yrange
    oplot, t[ix1:ix2], sbb
endelse
end

