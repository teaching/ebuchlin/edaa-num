;+
; NAME:
;  sa_plot_cutt
;
; PURPOSE:
;  Read necessary information and plot cut of (z,k,t) data cube of
;  field (square amplitude --- massic energy) for a given t
;
; CALLING SEQUENCE:
;  sa_plot_cutt, it2, sf, /uu, /bb, /zp, /zm, /ch, /log, 
;    /nointerpol, range=range,
;    gamma=gamma, atm=atm, /energy, compexp=compexp
;
; INPUTS:
;  it2: index of t (in out_time2i) used to plot the cut 
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default) to get paramaters (data will be
;  taken from other out_simstate_* files)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb, /zm and /zp is needed:
;  /uu: plot only velocity spectra
;  /bb: plot only magnetic field spectra
;  /zm: plot only zm spectra
;  /zp: plot only zp spectra
;  /ch: plot only cross helicity
;  /log: shows intensities in logarithmic scale
;  /nointerpol: do not interpolate coordinates in plot_image (this may
;    cause axes values to be false)
;  range: the range of the color scale will be reduced (but not extended)
;  gamma: gamma factor for displaying image (in the case of a cut).
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;  /energy: if set, read density in file and plot volumic energy
;    instead of massic energy
;  compexp: compensate values with k^compexp
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric.buchlin@ias.u-psud.fr
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  10 Oct 2005 EB/AV Forked from sa_plot_spectra
;  07 Nov 2005 EB Adapted for shell-atm
;  27 Mar 2006 EB Take aexp into account
;  24 Jul 2014 EB Added /ch
;-

pro sa_plot_cutt, it2, sf, uu=uu, bb=bb, zm=zm, zp=zp, ch=ch, $
                     log=log, nointerpol=nointerpol, range=range, $
                     gamma=gamma, atm=atm, $
                     energy=energy, compexp=compexp

sa_plot_cuts_tidyoptions, sf, uu=uu, bb=bb, zp=zp, zm=zm, ch=ch, $
  nointerpol=nointerpol, atm=atm, field=field

p = sa_params_get (sf, atm=atm)
nz0 = p.nz

if it2 lt 0 or it2 ge p.nrecsp then begin
    message, 'it2 out of bounds', /info
    it2 = it2 < (p.nrecsp-1) > 0
endif

outt2i = 'out_time2i'
outsimstate = 'out_simstate_'

readarray_s, outt2i, p.nrecsp, t2i

p = sa_params_get (outsimstate + t2i[it2], shells=shells, atm=atm)
nk = p.nmax - p.nmin + 1
logk = alog10 (p.k0 * p.lambda ^ (lindgen (nk) + p.nmin))
; use nz and dz from out_simstate_nnn, not from simstate.dat
z = lindgen (p.nz) * p.dz
if keyword_set(energy) then begin
    readarray, 'out_rho', p.nz, rho
    readarray, 'out_aexp', p.nz, aexp
    step = nz0 / p.nz
    selectz = step * lindgen (p.nz)
    rho  =  rho[selectz]
    aexp = aexp[selectz]
endif else begin
    rho = 1.d
    aexp = 1.d
endelse

; square amplitude of the field we want, or cross helicity
s = sa_shells_to_fields (shells[*, *, *], rho=rho, aexp=aexp, $
                         uu=uu, bb=bb, zp=zp, zm=zm, ch=ch, /energy, k0=p.k0, atm=atm)
if keyword_set (compexp) then begin
  for iz=0L, p.nz - 1L do begin
    s[*, iz] *= 10. ^ (compexp * logk)
  endfor
endif

sa_plot_cuts_normalize, s, log=log, range=range, norm=norm, atm=atm
if norm eq 0 then begin
  print, "Color range: ", min (range, max=tmp), tmp, "  Data range: ", min (s, max=tmp), tmp
endif else begin
  print, "Data and color range: ", min (s, max=tmp), tmp
endelse

plot_image_eb, s, logk, z, $
  gamma=gamma, norm=norm, xtitle='log10 kperp', ytitle='z', $
  title=field+', t='+string_ns(p.time)+' (it2='+string_ns(it2)+')', $
  nointerpol=nointerpol

end
