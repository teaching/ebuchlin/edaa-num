;+
; NAME:
;  sa_gen_html
;
; PURPOSE:
;  Generate HTML page (with images) for a summary of results of a run
;
; CALLING SEQUENCE:
;  sa_gen_html, title=title, /not2
;
; INPUTS:
;
; KEYWORD PARAMETERS:
;  /not2: do not attempt to read out_time2 and all the files output at
;    these times
;
; SIDE EFFECTS:
;  Create a www/ subdirectory containing the HTML file and PNG images
;
; RESTRICTIONS:
;  Unix system only
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  22 Nov 2005 EB Creation
;  27 Mar 2006 EB Added profiles of physical parameters and of average
;    heating; output is now valid XHTML 1.0
;  17 Feb 2007 EB Added /not2
;-


; write color PNG file of current plot
pro grab_plot, file
write_png, file, tvrd (true=1)
end


pro sa_gen_html, title=title, not2=not2

window, 0, xsize=480, ysize=350
if n_elements(title) eq 0 then title = 'Shell-atm run'
subdir = 'www/'

spawn, ['mkdir', '-p', subdir], /noshell    
p = sa_params_get ()

message, 'Generating HTML file', /info

openw, u, subdir + 'index.html', /get_lun
printf, u, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"'
printf, u, '   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
printf, u, '<html>'
printf, u, '<head>'
printf, u, '  <meta http-equiv="Content-Type"'
printf, u, '      content="text/html; charset=iso-8859-1" />'
printf, u, '  <meta name="Generator"'
printf, u, '      content="sa_gen_html (E. Buchlin)" />'
printf, u, '  <title>' + title + '</title>'
printf, u, '</head>'
printf, u, '<body>'
printf, u, '<h1>' + title + '</h1>'
printf, u, '<p>Parameters of the run:</p>'
printf, u, '<pre>'
; include output.log until '*** Running main loop ***'
openr, v, 'output.log', /get_lun
s = ""
while not eof (v) do begin
    readf, v, s
    if stregex (s, 'Running main loop', /boolean) then break
    if not stregex (s, 'I am number', /boolean) then printf, u, s
endwhile
free_lun, v
printf, u, '</pre>'
printf, u, '<h2>Profiles of physical parameters</h2>'
printf, u, '<h3>Alfv�n speed</h3>'
printf, u, '<img src="plot_profile_va.png" />'
printf, u, '<h3>Mass density</h3>'
printf, u, '<img src="plot_profile_rho.png" />'
printf, u, '<h3>Magnetic field B0</h3>'
printf, u, '<img src="plot_profile_bfi.png" />'
printf, u, '<h3>Flux tube width</h3>'
printf, u, '<img src="plot_profile_l.png" />'
printf, u, '<h3>Magnetic flux (should be uniform)</h3>'
printf, u, '<img src="plot_profile_bfl.png" />'
printf, u, '<h2>Energy</h2>'
printf, u, '<img src="plot_energy.png" />'
printf, u, '<h2>Dissipation power</h2>'
printf, u, '<img src="plot_de.png" />'
printf, u, '<h2>Time scales</h2>'
printf, u, '<img src="plot_dt.png" />'
printf, u, '<h2>Time scales on shells (last time)</h2>'
printf, u, '<img src="plot_dt_k.png" />'
printf, u, '<h2>Energy balance: energies, powers, integrated powers</h2>'
printf, u, '<img src="plot_enbal.png" />'
if not keyword_set(not2) then begin 
    printf, u, '<h2>Energy flux across planes</h2>'
    printf, u, '<img src="plot_enflux.png" />'
    printf, u, '<h2>Energy flux across shells</h2>'
    printf, u, '<p>Over the whole simulation, and the second half of the simulation.</p>'
    printf, u, '<img src="plot_kflux.png" />'
    printf, u, '<img src="plot_kflux2.png" />'
endif
printf, u, '<h2>Perpendicular spectra</h2>'
printf, u, '<p>At some times, summed over z.</p>'
printf, u, '<img src="plot_specav.png" />'
printf, u, '<h2>Heating as a function of k</h2>'
printf, u, '<p>At some times, summed over z.</p>'
printf, u, '<img src="plot_specav_heating.png" />'
if not keyword_set(not2)  then begin
    printf, u, '<h2>Heating function</h2>'
    printf, u, '<img src="plot_heatfunc.png" />'
    printf, u, '<p>Average profiles of heating function, per unit mass, volume, and length.</p>'
    printf, u, '<img src="plot_heatfuncprofm.png" />'
    printf, u, '<img src="plot_heatfuncprofv.png" />'
    printf, u, '<img src="plot_heatfuncprofl.png" />'
endif
printf, u, '<h2>2D spectra</h2>'
printf, u, '<p>z+ and z-.</p>'
printf, u, '<img src="plot_spectrum2d_p.png" />'
printf, u, '<img src="plot_spectrum2d_m.png" />'
printf, u, '<h2>Perpendicular and parallel average spectra</h2>'
printf, u, '<p>z+ and z-.</p>'
printf, u, '<img src="plot_spectrumpp_p.png" />'
printf, u, '<img src="plot_spectrumpp_m.png" />'
if not keyword_set(not2)  then begin
    printf, u, '<h2>Cuts of the fields at some k</h2>'
    printf, u, '<p>ikperp=5, z+ and z-.</p>'
    printf, u, '<img src="plot_cutk_p5.png" />'
    printf, u, '<img src="plot_cutk_m5.png" />'
    printf, u, '<p>ikperp=12, z+ and z-.</p>'
    printf, u, '<img src="plot_cutk_p12.png" />'
    printf, u, '<img src="plot_cutk_m12.png" />'
    printf, u, '<h2>Cuts of the fields at some t</h2>'
    printf, u, '<p>Beginning of simulation, z+ and z-.</p>'
    printf, u, '<img src="plot_cutt_p0.png" />'
    printf, u, '<img src="plot_cutt_m0.png" />'
    printf, u, '<p>Middle of simulation, z+ and z-.</p>'
    printf, u, '<img src="plot_cutt_p1.png" />'
    printf, u, '<img src="plot_cutt_m1.png" />'
    printf, u, '<p>End of simulation, z+ and z-.</p>'
    printf, u, '<img src="plot_cutt_p2.png" />'
    printf, u, '<img src="plot_cutt_m2.png" />'
    printf, u, '<h2>Cuts of the fields at some z</h2>'
    printf, u, '<p>First boundary, z+ and z-.</p>'
    printf, u, '<img src="plot_cutz_p0.png" />'
    printf, u, '<img src="plot_cutz_m0.png" />'
    printf, u, '<p>Middle of box, z+ and z-.</p>'
    printf, u, '<img src="plot_cutz_p1.png" />'
    printf, u, '<img src="plot_cutz_m1.png" />'
    printf, u, '<p>Close to second boundary, z+ and z-.</p>'
    printf, u, '<img src="plot_cutz_p2.png" />'
    printf, u, '<img src="plot_cutz_m2.png" />'
endif

free_lun, u

; end   ; only html

message, 'Generating plots', /info

if not keyword_set(not2)  then begin
    readarray_s, 'out_time2i', 3, t2i
    t2i0 = t2i[0]
    psim = sa_params_get ('out_simstate_' + t2i0)
endif

; try to evaluate time step
if not keyword_set(not2) then begin
    readarray, 'out_time2', 3, t2
    ; use 2-1 instead of 1-0, because first output may be done twice
    dt = (t2[2] - t2[1]) / (float (t2i[2]) - float (t2i[1]))
endif else begin
    ; guess (wrong if simulation restarted, but only order of
    ; magnitude needed)
    dt = p.time / p.nt
endelse

sa_plot_profile, /vaa
grab_plot, subdir + 'plot_profile_va.png'
sa_plot_profile, /rho
grab_plot, subdir + 'plot_profile_rho.png'
sa_plot_profile, /bfield
grab_plot, subdir + 'plot_profile_bfi.png'
sa_plot_profile, /l
grab_plot, subdir + 'plot_profile_l.png'
sa_plot_profile, /bflux
grab_plot, subdir + 'plot_profile_bfl.png'

sa_plot_energy, /tot, minmax=2000
grab_plot, subdir + 'plot_energy.png'

sa_plot_de, /tot, minmax=2000
grab_plot, subdir + 'plot_de.png'

sa_plot_dt, minmax=2000
grab_plot, subdir + 'plot_dt.png'
sa_plot_dt, /k
grab_plot, subdir + 'plot_dt_k.png'

powscale = 10. ^ floor (alog10 (dt * p.nt))
if powscale ge 1 then powscale = long (powscale)
sa_plot_enbal, powscale=powscale, minmax=2000
grab_plot, subdir + 'plot_enbal.png'

if not keyword_set(not2)  then begin
    sa_plot_enflux
    grab_plot, subdir + 'plot_enflux.png'
    sa_plot_kflux
    grab_plot, subdir + 'plot_kflux.png'
    sa_plot_kflux, time=p.nrecsp/2+lindgen(p.nrecsp/2)
    grab_plot, subdir + 'plot_kflux2.png'
endif

spyr = [-5, 10]
heyr = [-5, 10]

if p.nrecsp ge 50 then time = lindgen (50) * p.nrecsp / 50L $
else time = lindgen (p.nrecsp)
sa_plot_specav, /bu, time=time, yrange=spyr
grab_plot, subdir + 'plot_specav.png'
sa_plot_specav, /bu, /heating, time=time, yrange=heyr
grab_plot, subdir + 'plot_specav_heating.png'

sa_plot_spectrum2d, /zp
grab_plot, subdir + 'plot_spectrum2d_p.png'
sa_plot_spectrum2d, /zm
grab_plot, subdir + 'plot_spectrum2d_m.png'

sa_plot_spectrumpp, /zp
grab_plot, subdir + 'plot_spectrumpp_p.png'
sa_plot_spectrumpp, /zm
grab_plot, subdir + 'plot_spectrumpp_m.png'

if not keyword_set(not2)  then begin
    sa_plot_cutk, 5, /zp, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutk_p5.png'
    sa_plot_cutk, 5, /zm, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutk_m5.png'

    sa_plot_cutk, 12, /zp, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutk_p12.png'
    sa_plot_cutk, 12, /zm, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutk_m12.png'

    sa_plot_cutt, 0, /zp, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_p0.png'
    sa_plot_cutt, 0, /zm, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_m0.png'

    sa_plot_cutt, p.nrecsp / 2, /zp, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_p1.png'
    sa_plot_cutt, p.nrecsp / 2, /zm, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_m1.png'

    sa_plot_cutt, p.nrecsp - 1, /zp, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_p2.png'
    sa_plot_cutt, p.nrecsp - 1, /zm, /noi, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutt_m2.png'

    sa_plot_cutz, 0, /zp, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_p0.png'
    sa_plot_cutz, 0, /zm, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_m0.png'

    sa_plot_cutz, psim.nz / 2, /zp, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_p1.png'
    sa_plot_cutz, psim.nz / 2, /zm, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_m1.png'

    sa_plot_cutz, psim.nz - 1, /zp, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_p2.png'
    sa_plot_cutz, psim.nz - 1, /zm, /energy, gamma=.5
    grab_plot, subdir + 'plot_cutz_m2.png'

    sa_plot_heatfunc, gamma=.5
    grab_plot, subdir + 'plot_heatfunc.png'
    sa_plot_heatfunc, /profile, /massic
    grab_plot, subdir + 'plot_heatfuncprofm.png'
    sa_plot_heatfunc, /profile, /volumic
    grab_plot, subdir + 'plot_heatfuncprofv.png'
    sa_plot_heatfunc, /profile
    grab_plot, subdir + 'plot_heatfuncprofl.png'
endif


end

