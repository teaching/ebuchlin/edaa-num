;+
; NAME:
;  readarray
;
; PURPOSE:
;  Reads an array in a file
;
; CALLING SEQUENCE:
;  Procedure syntax:
;   readarray, file, n, s, err=err, /quiet, /unformatted
;  Function syntax:
;   s = readarray (file, n, err=err, /quiet, /unformatted)
;
; INPUTS:
;  file: file containing array in foratted form (text)
;  n:    number of records
;
; OUTPUTS:
;  s:    array
;
; KEYWORD PARAMETERS:
;  err: error code when opening the file
;  /quiet: don't say anything, even in case of an error
;  /unformatted: file is "F77" unformatted (binary) 
;
; BUGS:
;  This file must be explicitely compiled before using the function
;  syntax
;  Returns only arrays of double
;
; PROJECT:
;  PhD/gen (originally shell)
;
; AUTHORS:
;  EB Eric Buchlin eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  07 Feb 2002 EB Creation
;  30 Jun 2003 EB Added function syntax 
;  21 Apr 2004 EB Added err and /quiet keywords
;  18 Nov 2005 EB Added /unformatted keyword
;-

pro readarray, file, n, s, err=err, quiet=quiet, unformatted=unformatted
if n le 0 then begin
    err = 1
    return
endif
s = dblarr (n)
if keyword_set(unformatted) then begin
    openr, u, file, /get_lun, err=err, /f77_unformatted
    if err ne 0 then begin
        if not keyword_set(quiet) then $
          message, "File " + file + " not readable", /info
        return
    endif
    tmp = 0.d
    for i=0, n-1 do begin
        readu, u, tmp
        s[i] = tmp
    endfor
;    readu, u, s
endif else begin
    openr, u, file, /get_lun, err=err
    if err ne 0 then begin
        if not keyword_set(quiet) then $
          message, "File " + file + " not readable", /info
        return
    endif
    readf, u, s
endelse
free_lun, u
return
end

function readarray, file, n, err=err, quiet=quiet, unformatted=unformatted
readarray, file, n, s, err=err, quiet=quiet, unformatted=unformatted
return, s
end
