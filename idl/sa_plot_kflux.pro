;+
; NAME:
;  sa_plot_kflux
;
; PURPOSE:
;  Read necessary information and plot energy flux across shells of
;  the shell model (flux to z+ for k>kn, to z- for k>kn, and total)
;
; CALLING SEQUENCE:
;  sa_plot_kflux, sf, z=z, time=time, xrange=xrange, yrange=yrange,
;  /pipm, /atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  z: indices (in out_simstate_nnn) of planes on which an average is
;    done. Default: all available planes.
;  time: indices (in time2) of times on which an average is
;    done. Default: all available times.
;  xrange: range for x axis
;  yrange: range for y axis
;  /pipm: plot also the energy flux for both z+ and z-
;  /atm: if set (default), concerns shell-atm instead of shell-loop
;
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  EB Eric Buchlin,   eric@arcetri.astro.it
;  AV Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  16 Nov 2005 AV Creation
;  07 Apr 2007 EB Added /pipm
;-

pro sa_plot_kflux, sf, z=z ,time=time, xrange=xrange, yrange=yrange, $
                   pipm=pipm, atm=atm

if n_elements(sf) eq 0 then sf = "simstate.dat" ; not used!
if n_elements(atm) eq 0 then atm = 1

outt2i = 'out_time2i'
outsimstate = 'out_simstate_'

; get number of output from simstate.dat
psim = sa_params_get (sf, atm=atm)

readarray_s, outt2i, psim.nrecsp, t2i

; get other parameters from first available out_simstate_nnn
p = sa_params_get (outsimstate + t2i[0], atm=atm)
; wavenumbers
nk = long (p.nmax - p.nmin + 1L)
k = p.k0 * p.lambda ^ (indgen (nk) + p.nmin)
logk = alog10 (k)
; positions for average
if n_elements(z) eq 0 then begin
    z = lindgen (p.nz)
endif else if n_elements(z) eq 1 then begin 
    z = [z]
endif
z = z[uniq (z > 0 < p.nz)]
nz = n_elements(z) 
; times for average
if n_elements(time) eq 0  then begin
    time = lindgen (psim.nrecsp)
endif else if n_elements(time) eq 1 then begin
    time = [time]
endif
time = time[uniq (time > 0 < psim.nrecsp)]
nt = n_elements(time) 
t2i = t2i[time]

; coefficients of shell-model
a1 = 0.5d * (p.eps + p.epsm) 
a2 = 0.5d * (2.d - p.eps - p.epsm) 
a3 = 0.5d * (p.epsm - p.eps) 

realtime = dblarr (nt)
; flux across k_n , for +/-
kfl = dblarr(nk, 2)
for it2i=0, nt-1 do begin
    tmpp = sa_params_get (outsimstate + t2i[it2i], shells=shells, atm=atm)
    realtime[it2i] = tmpp.time
    zp = sa_shells_to_fields (shells, /zp)
    zm = sa_shells_to_fields (shells, /zm)
    for iz=0, nz-1 do begin
        pzp = zp[*, z[iz]]
        pzm = zm[*, z[iz]]
        l = p.lambda
        ik = 1
        kfl[ik, 0] += -imaginary (k[ik] * $
                      (a1 / l   * pzp[ik-1] * pzp[ik  ] * pzm[ik+1] + $
                       a2 / l   * pzp[ik-1] * pzm[ik  ] * pzp[ik+1]))
        kfl[ik, 1] += -imaginary (k[ik] * $
                      (a1 / l   * pzm[ik-1] * pzm[ik  ] * pzp[ik+1] + $
                       a2 / l   * pzm[ik-1] * pzp[ik  ] * pzm[ik+1]))
        for ik=2, nk-2 do begin
            kfl[ik, 0] += -imaginary (k[ik] * $
                          (a3 / l^2 * pzm[ik-2] * pzp[ik-1] * pzp[ik  ] + $
                           a2 / l^2 * pzp[ik-2] * pzm[ik-1] * pzp[ik  ] + $
                           a1 / l   * pzp[ik-1] * pzp[ik  ] * pzm[ik+1] + $
                           a2 / l   * pzp[ik-1] * pzm[ik  ] * pzp[ik+1]))
            kfl[ik, 1] += -imaginary (k[ik] * $
                          (a3 / l^2 * pzp[ik-2] * pzm[ik-1] * pzm[ik  ] + $
                           a2 / l^2 * pzm[ik-2] * pzp[ik-1] * pzm[ik  ] + $
                           a1 / l   * pzm[ik-1] * pzm[ik  ] * pzp[ik+1] + $
                           a2 / l   * pzm[ik-1] * pzp[ik  ] * pzm[ik+1]))
        endfor
        ik = nk-1
        kfl[ik, 0] += -imaginary (k[ik] * $
                      (a3 / l^2 * pzm[ik-2] * pzp[ik-1] * pzp[ik  ] + $
                       a2 / l^2 * pzp[ik-2] * pzm[ik-1] * pzp[ik  ]))
        kfl[ik, 1] += -imaginary (k[ik] * $
                      (a3 / l^2 * pzp[ik-2] * pzm[ik-1] * pzm[ik  ] + $
                       a2 / l^2 * pzm[ik-2] * pzp[ik-1] * pzm[ik  ]))
    endfor
endfor
kfl = kfl / nt / nz

; ranges of plots
if n_elements(xrange) ne 2 then xrange = [min (logk, max=tmp), tmp]
if n_elements(yrange) ne 2 then $
  yrange = [min ([kfl[*, 0], kfl[*, 1], total (kfl, 2)], max=tmp), tmp]

plot, logk, /nodata, xrange=xrange, yrange=yrange, xstyle=1, ystyle=3, $
  xtitle='log10 k', ytitle='!7P!3!N'
oplot, logk, total (kfl, 2), line=0
if keyword_set(pipm) then begin 
    oplot, logk, kfl[*, 0], line=3
    oplot, logk, kfl[*, 1], line=2
endif
oplot, logk, intarr (nk), line=1
end
