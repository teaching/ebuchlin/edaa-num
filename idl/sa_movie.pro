;+
; NAME:
;  sa_movie
;
; PURPOSE:
;  Show a movie or save an AVI file of data output by shell-atm
;
; CALLING SEQUENCE:
;  sa_movie, sf, file="file.avi", time=time, atm=atm
;
; OPTIONAL INPUT:
;  sf: file simstate.dat (default)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;  /uu, /bb, /zp, /zm: plot u, b, zp or zm
;  range: value range for color scale
;
; KEYWORD PARAMETERS:
;  file: AVI file to write
;  time: time indices to use
;
; OUTPUT:
;  
;
; KEYWORD OUTPUT:
;  
;
; SIDE EFFECTS:
;  Show a movie or save a AVI file
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  13 Dec 2002 EB Creation
;  07 Nov 2005 EB Adapted to shell-atm
;-

pro sa_movie, sf, zp=zp, zm=zm, uu=uu, bb=bb, range=range, file=file, time=time, atm=atm

compexp = 2. / 3


if n_elements(sf) eq 0 then sf = "simstate.dat"
if n_elements(atm) eq 0 then atm = 1
p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1
logk = alog10 (p.k0 * p.lambda ^ (indgen (nk) + p.nmin))
z = indgen (p.nz) * p.dz

nt = n_elements(time) 
if nt eq 0 then begin
    time = indgen (p.nrecsp)
endif else if nt eq 1 then begin
    time = [time]
endif
time = time (uniq (time > 0 < (p.nrecsp - 1)))
nt = n_elements(time)

if atm eq 0 then begin
    filetype = 's'              ; s is spectra, m is movie
    datafile = 'out_specu'
    outtime = 'out_time'
    readarray, outtime, p.nrecen, t
    if filetype eq 'm' then begin
        realtime = t[time]
        readarray, datafile, nk * p.nz * p.nrecen, data
        data = reform (data, nk, p.nz, p.nrecen, /overwrite)
    endif else if filetype eq 's' then begin
        realtime = t[(time+1) * (p.nrecen / p.nrecsp) - 1] 
        readarray, datafile, nk * p.nz * p.nrecsp, data
        data = reform (data, nk, p.nz, p.nrecsp, /overwrite)
    endif
    data = data[*, *, time]

    window, 0, xsize=400, ysize=400, retain=2

; spectrum correction (makes spectrum flat)
    for it=0, nt - 1 do begin
        for iz=0, p.nz - 1 do begin
            data[*, iz, it] = data[*, iz, it] * 10. ^ (logk * compexp)
        endfor
    endfor


    for i=0, nt-1 do begin
        plot_image_eb, reform (data[*, *, i], nk, p.nz), $
          logk, z, /norm, xtitle='log10 k', ytitle='z', $
          title=datafile, /nointerpol
        
        frame = tvrd ()
        framefile = "movie/frame" + string_ns (i, format='(I5)')+ ".png"
        write_png, framefile, frame
        
    endfor
endif else begin
; for shell-atm, from out_simstate_nnn at nnn found
; in out_time2i by sa_plot_cutt
    p = sa_params_get ()
    for i=0, p.nrecsp - 1 do begin
        sa_plot_cutt, i, sf, zp=zp, zm=zm, uu=uu, bb=bb, /noi, atm=atm, /energy, compexp=compexp, range=range
        frame = tvrd ()
        framefile = "movie/frame" + string_ns (i, format='(I05)')+ ".png"
        write_png, framefile, frame
    endfor
endelse

if n_elements(file) eq 0 then file = "out.avi"

pushd, 'movie/'
spawn, "mencoder ""mf://*.png""  -fps 25 -ovc lavc " + $
  " -lavcopts vcodec=mpeg4:vbitrate=600000 -ffourcc DX50 -o " + file
popd

end
