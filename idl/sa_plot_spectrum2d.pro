;+
; NAME:
;  sa_plot_spectrum2d
;
; PURPOSE:
;  Read necessary information and plot 2D spectra
;
; CALLING SEQUENCE:
;  sa_plot_spectrum2d, sf, /uu, /bb, /zm, /zp, /tot, /nointerpol, /isotropic,
;    range=range, xrange=xrange, yrange=yrange, ks=ks, slope=slope,
;    /contour, /thcont, time=time, /noplot, spec=spec, kperp=kperp,
;    kpar=kpar, title=title, atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb, /zm, /zp or /tot needed:
;  /uu: plot velocity field spectrum
;  /bb: plot magnetic field spectrum
;  /zp: plot z+ Els�sser field spectrum
;  /zm: plot z- Els�sser field spectrum
;  /tot: plot total field spectrum
;
;  /nointerpol: do not interpolate coordinates in plot_image (this may
;    cause axes values to be false)
;  range: maximum data range for color scale
;  xrange: range for x axis
;  yrange: range for y axis
;  /isotropic: isotropic plot
;  /contour: overplot contours
;  /thcont: overplot thererical contour. kpar=kpar * bperpRMS/b0,
;    green with exponent 2/3, blue  with exponent 1.
;  /han: hanning window
;  /ktheta: theta-logk plot (not implemented)
;  /polar: polar plot (not implemented)
;  /noplot: don't do plot (useful when called for spec output)
;  time: array of times (of indices of non-integrated output times) on
;    which to average (otherwise use sf)
;  title: part of plot title (with follow name of field)
;
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OPTIONAL OUTPUTS:
;  spec: 2D spectrum
;  kperp: array of perpendicular wavenumbers
;  kpar : array of parallel wavenumbers
;
; SIDE EFFECTS:
;  Show plot
;
; RESTRICTIONS:
;  Assume that all out_simstate_nnn have the same nmin, nmax and nz.
;  With PS device, need device, decomposed=1
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric.buchlin@ias.u-psud.fr
;  Andrea Verdini, verdini@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  13 Feb 2004 EB Forked from sa_plot_spectra
;  07 Nov 2005 EB Adapted for shell-atm
;  09 Dec 2005 EB Take into account precise surface of shells (but
;    still missing other constant factors)
;  06 Feb 2005 EB time, /thcont, /noplot, spec, kperp and kpar keywords
;  05 Jun 2014 EB Added title keyword
;-

pro sa_plot_spectrum2d, sf, uu=uu, bb=bb, zp=zp, zm=zm, tot=tot, $
                        nointerpol=nointerpol, isotropic=isotropic, $
                        range=range, xrange=xrange, yrange=yrange, $
                        contour=contour, thcont=thcont, han=han, $
                        time=time, ktheta=ktheta, polar=polar, $
                        noplot=noplot, spec=spec, slope=slope, $
                        kperp=kperp, kpar=kpar, title=title, atm=atm

if keyword_set(nointerpol) then nointerpol = 1 else nointerpol = 0

if n_elements(atm) eq 0 then atm = 1
if n_elements(sf) eq 0 then sf = "simstate.dat"
if n_elements(title) eq 0 then title = "" else title += ", "
p = sa_params_get (sf, shells=shells, atm=atm)
nk = long (p.nmax - p.nmin + 1L)
logk = alog10 (p.k0 * p.lambda ^ (indgen (nk) + p.nmin))
z = lindgen (p.nz) * p.dz

if n_elements(time) ne 0 then begin
    time = time[uniq (time < (p.nrecsp - 1L))]
    nt = n_elements (time) 
    readarray_s, 'out_time2i', p.nrecsp, t2i
endif else nt = 1L

for it=0L, nt-1L do begin
    if n_elements(time) ne 0 then begin
        ; overwrite p and shells with current value from out_simstate_nnn
        file = 'out_simstate_' + t2i[time[it]]
        message, 'Get spectrum for ' + file, /info
        p = sa_params_get (file, shells=shells)
        if it eq 0 then begin
            nk = long (p.nmax - p.nmin + 1L)
            logk = alog10 (p.k0 * p.lambda ^ (indgen (nk) + p.nmin))
            z = lindgen (p.nz) * p.dz
        endif else begin ; checks that all have same nk and nz
            if nk ne long (p.nmax - p.nmin + 1L) or $
               p.nz ne n_elements(z) then $
                  message, 'All files should have same nk and nz'
        endelse
    endif  ; else just use shells already read in simstate.dat
    
    if keyword_set (uu)  then begin
        title += 'Velocity'
         f = shells[*, *, 0] + shells[*, *, 1]
    endif else if keyword_set(bb)  then begin
        title += 'Magnetic'
        f = shells[*, *, 0] - shells[*, *, 1]
    endif else if keyword_set(zp)  then begin
        title += 'z+'
        f = shells[*, *, 0]
    endif else if keyword_set(zm)  then begin
        title += 'z-'
        f = shells[*, *, 1]
    endif else if keyword_set(tot) then begin
        title += 'total'
        f = shells
    endif else begin
        message, 'Choose field: /uu, /bb, /zp, /zm or /tot', /info
        return
    endelse
    if not keyword_set(tot) then f = reform (f, nk, p.nz, /overwrite)
    
    ; Hanning window
    if keyword_set(han)  then begin
       mask = hanning (p.nz, /double)
       if keyword_set(tot) then begin
          for ik=0L, nk-1L do for ii=0, 1 do f[ik, *, ii] *= mask
       endif else begin
          for ik = 0L, nk-1L do f[ik,*] *= mask
       endelse
    endif
; Fourier spectrum
    tt = fft (f, dimension=2, /double)
; power spectrum
    tt = abs (tt * tt)
; sum spectra for z+ and z- if total spectra requested
    if keyword_set(tot) then tt = total (tt, 3)
; sum positive and negative frequencies (excluding 0 and Nyquist frequencies)
    if p.nz mod 2 ne 0 then message, 'Code not suitable for odd nz'
    swap = reverse (tt[*, p.nz / 2L + 1L:p.nz - 1L], 2)
    tt = tt[*, 1L:(p.nz/2L)-1L]
    tt += swap
; frequencies corresponding to these values in the spectrum
    zf = (findgen (p.nz/2 - 1) + 1) / (p.dz * p.nz)
; smooth logarithmically along kz and keep only relevant information
    for i=0, p.nmax-p.nmin do begin
        tt[i, *] = logsmooth (tt[i, *], .1, bincenter=bincenter, bincenval=bincenval)
    endfor
    tt = tt[*, bincenter]
    zf = zf[0] * 10. ^ bincenval
; axes of tt (logarithm)
    kperp = alog10 (p.k0 * p.lambda ^ (p.nmin + indgen (p.nmax - p.nmin + 1)))
    kpar = alog10 (zf * 2. * !pi)
; spectrum along kperp is still "average energy in a shell". Need to
; divide by kperp(lambda^2-1) to get an equivalent 1D perpendicular
; spectrum
    for i=0, n_elements(zf)-1 do tt[*, i] /= 10. ^ kperp
    tt /= (p.lambda ^ 2 - 1.d)
; divide also by 2 if Z� field and not u or b
    if keyword_set(zp) or keyword_set(zm) then tt /= 2.d

; update sum of spectra (will give average spectrum)
    if it eq 0 then ttav = tt else ttav += tt

endfor
tt = ttav / nt

if not keyword_set(noplot) then begin
    if n_elements(range) eq 2 then begin
        tt = tt > range[0] < range[1]
    endif
    plot_image_eb, bytscl(alog10(tt)), kperp, kpar, nointerpol=nointerpol, $
      xtitle="log10(kperp)", ytitle="log10(kpar)", isotropic=isotropic,  $
      title=title

    if !d.name eq 'PS' then begin
       ccolor = [255L, 255L * 256L, 255L * 65536L]
    endif else ccolor = [255L, 255L * 256L, 255L * 65536L]
    if keyword_set(contour) then begin
                                ; spectrum contour
        contour, bytscl(alog10(tt)), kperp, kpar, /overplot, $
          levels=25*(indgen(10)+1), c_color=ccolor[0]
    endif
    if keyword_set(thcont) then begin 
       ; compute bperp/b0
       bperp = shells[*, *, 0] - shells[*, *, 1]
       bperpb0 = mean (sqrt (total (abs (bperp ^ 2), 1))) / p.b0
       print, 'Bperp/B0:', bperpb0
; prediction of theories:
; ellipses, axes kperp and kpar = (bperpb0 kperp)^kparexp, with
; kparexp=1 and 2/3 (critical balance for -2 and K41 spectra)
       kparexp = [2./3., 1.]
       for ik=0, 1 do begin
          theta = findgen (91) / 90. * !pi / 2.
          theta[0] = 1e-2
          theta[90] = !pi / 2. - 1e-2
          theta = [1e-6, 1e-4, theta, !pi / 2. - 1e-4, !pi / 2. - 1e-6]
          for i=2, 7 do begin
             oplot, i + alog10 (cos (theta)),  $
                    kparexp[ik] * (i + alog10 (bperpb0)) + alog10 (sin (theta)), color=ccolor[1+ik], linestyle=2
          endfor
       endfor
    endif
endif
spec = tt

end
