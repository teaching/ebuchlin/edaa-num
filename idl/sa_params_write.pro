;+
; NAME:
;  sa_params_write
;
; PURPOSE:
;  Write parameters for a shell_loop simulation in a fake simstate.dat
;  file that can be used for data analysis or to start a simulation
;
; CALLING SEQUENCE:
;  sa_params_write, p, "simstate.dat", shells=shells, time=time,
;    forc=forc, atm=atm
;
; INPUTS:
;  p (of type sa_params): parameters
;  "simstate.dat" : simulation state file 
;
; OPTIONAL INPUT:
;  shells: values of shells (real array, with real and imaginary parts
;    of the complex values of the shells). Default is an array of zeroes.
;  time: the time of the simulation. Deprecated; use p.time instead
;    (but time overrides p.time)
;  forc: coefficients of forcing
;    (complex, dimensions (2[coefficients], nkforce, 2[boundaries]))
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;  p : sa_params structure containing the parameters
;
; OPTIONAL OUTPUTS:
;  shells: last shell values (in all planes)
;
; RESTRICTIONS:
;  Assumes that nkforce=3 if forc not provided
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-loop, shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  22 Apr 2004 EB Creation
;  07 Jun 2005 EB Support for p.time. Format for double precision output
;  07 Nov 2005 EB Adapted for shell-atm
;  17 Feb 2006 EB Added input of forcing coefficients
;-

pro sa_params_write, p, file, shells=shells, time=time, forc=forc, atm=atm

if n_elements(file) eq 0 then file = "simstate.dat"
if n_elements(time) ne 0 then p.time = time
if n_elements(atm) eq 0 then atm = 1

openw, u, file, err=err, /get_lun
if err ne 0 then begin
    message, "Couldn't open file " + file, /info
    return
endif

printf, u, p.nmin
printf, u, p.nmax
printf, u, p.nz
printf, u, p.k0,     format='(E23.15E3)'
printf, u, p.lambda, format='(E23.15E3)'
printf, u, p.nu,     format='(E23.15E3)'
printf, u, p.eta,    format='(E23.15E3)'
printf, u, p.eps,    format='(E23.15E3)'
printf, u, p.epsm,   format='(E23.15E3)'
printf, u, p.b0,     format='(E23.15E3)'
printf, u, p.dz,     format='(E23.15E3)'
printf, u, p.nt
printf, u, 0.         ; delta_t
printf, u, p.nrecen
printf, u, p.nrecsf
printf, u, p.nrecsp

if n_elements(shells) eq 0 then $
  shellri = dblarr (2, p.nmax - p.nmin + 1, p.nz, 2) $
else begin
    s = size (shells)
    if s[0] eq 3 then begin     ; complex array
        shellri = dblarr (2, p.nmax - p.nmin + 1, p.nz, 2)
        shellri[0, *, *, *] = real_part (shells)
        shellri[1, *, *, *] = imaginary (shells)
    endif  ; else: supposed to be array of real and imaginary values
endelse

printf, u, reform (shellri, 1, n_elements(shellri)), format='(E23.15E3)'

printf, u, p.time, format='(E23.15E3)'

if atm then begin
    printf, u, p.tstar, format='(E23.15E3)'
    if n_elements(forc) eq 0 then begin
        printf, u, 3            ; assuming nkforce=3
        printf, u, dblarr (3 * 8), format='(E23.15E3)' ; assuming nkforce=3
    endif else begin
        s = size (forc)
        if (s[0] ne 3 or (s[4] ne 6 and s[4] ne 9) or $
            s[1] ne 2 or s[3] ne 2) then begin
            message, 'forc should be a (2,n,2) complex array'
        endif
        printf, u, s[2]
        forcri = dblarr (2, 2, s[2], 2)
        forcri[0, *, *, *] = real_part (forc)
        forcri[1, *, *, *] = imaginary (forc)
        printf, u, reform (forcri, 1, 2L * s[5]), format='(E23.15E3)'
    endelse
    printf, u, p.rho0, format='(E23.15E3)'
    printf, u, p.h0, format='(E23.15E3)'
endif

free_lun, u

end
