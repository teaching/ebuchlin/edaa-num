;+
; NAME:
;  pause
;
; PURPOSE:
;  Print message and wait for a key
;
; CALLING SEQUENCE:
;  pause, "message"
;
; OPTIONAL INPUTS:
;  message: message to print. Default is "Press any key to continue..."
;
; LANGUAGE:
;  IDL
;
; AUTHORS:
;  Eric Buchlin, IAS, eric.buchlin@ias.fr
;
; MODIFICATION HISTORY:
;  09 May 2004 EB Creation
;-


pro pause, m
if n_elements(m) eq 0 then m = 'Press any key to continue...'
print, m
c = get_kbrd (1)
end
