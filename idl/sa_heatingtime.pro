;+
; NAME:
;  sa_heatingtime
;
; PURPOSE:
;  Read necessary information and get characteristic time of heating
;
; CALLING SEQUENCE:
;  tauh = sa_heatingtime (sf, /uu, /bb, /ub, /tot, trange=trange)
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb and /ub is needed:
;  /uu: only velocity spectra
;  /bb: only magnetic field spectra
;  /tot: total (sum of u and b)
;  trange: range of times to use
;
; OUTPUT:
;  Characteristic time(s) of heating
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  None
;
; RESTRICTIONS:
;  Evenly-spaced data only
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  Eric Buchlin, http://eric.buchlin.org/
;
; MODIFICATION HISTORY:
;  29 May 2007 EB Forked from sa_plot_de
;-

function sa_heatingtime, sf, uu=uu, bb=bb, tot=tot, $
                         trange=trange

if keyword_set(tot) then begin
    uu = 1
    bb = 1
endif else if keyword_set(uu)  then begin
    uu = 1
    bb = 0
endif else if keyword_set(bb) then begin
    bb = 1
    uu = 0
endif else begin
    uu = 1
    bb = 1
endelse

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf)

outtime = 'out_time'
outdu  = 'out_dissu'
outdb  = 'out_dissb'

readarray, outtime, p.nrecen, t
nt =  n_elements(t) 


if keyword_set(tot) then begin
    readarray, outdu, du
    readarray, outdb, d
    d += du
endif else if uu then begin
    readarray, outdu, p.nrecen, d
endif else begin                ; /bb
    readarray, outdb, p.nrecen, d
endelse

if n_elements(trange) eq 2 then begin
    w = where (t ge trange[0], count)
    if count eq 0 then itmin = 0 else itmin = min (w)
    w = where (t le trange[1], count)
    if count eq 0 then itmax = nt - 1 else itmax = max (w)
    t = t[itmin:itmax]
    d = d[itmin:itmax]
    nt = itmax - itmin + 1
endif


; Fourier transform and get frequency (assuming evenly-spaced data)
dd = fft (d)
dd = abs (dd) ^ 2
dd = shift (dd, nt / 2L - 1L)
f0 = nt / (t[nt-1] - t[0])
f = (findgen (nt) - nt / 2L + 1L) / (t[nt-1] - t[0])
; select >0 frequencies
f = f[nt/2L:*]
dd = dd[nt/2L:*]

; plot, f, dd

; get average frequency, weighted by spectral energy
fav = total (f * dd) / total (dd)

; return characteristic heating time

stop
return, 1.d / fav

end

