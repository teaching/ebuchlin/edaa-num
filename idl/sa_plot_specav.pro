;+
; NAME:
;  sa_plot_specav
;
; PURPOSE:
;  Read necessary information and plot averaged energy in each shell
;  (or spectra) (summed over z)
;
; CALLING SEQUENCE:
;  sa_plot_specav, sf, /uu, /bb, /ub, /bu, /tot, time=time, , shift=shift
;    /heating, /image, /nointerpol, xrange=xrange, yrange=yrange,
;    atm=atm, taverage=taverage, /realspec, colors=colors,
;    /diffcons, /diffav, compexp=compexp, /fit, frange=frange
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb, /ub and /tot is needed:
;  /uu: plot only velocity spectra
;  /bb: plot only magnetic field spectra
;  /ub: plot both, on separate plots (!p.multi must be set before
;    calling this procedure if necessary)
;  /bu: plot both, on same plot (u in black or white, b in blue
;  time: plot spectra only for these times (expressed as an index of the
;    time dimension of the spectra data cube; is between 0 and
;    p.nrecsp - 1). time can be an array. The spectra are taken _at the
;    middle of the loop_.
;  shift: shift (in orders of magnitude) on the y axis to "pile up"
;    the spectra
;  /heating: multiply by nu or eta k^2, so as to have heating instead
;    of spectrum
;  /image: produce image with axes (k,t). In this case, the color range
;    is yrange
;  /nointerpol: no interpolation for image plot
;  xrange: range for x axis
;  yrange: range for y axis
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;  taverage: plot a number taverage of partial averages over given
;    times instead of all spectra. 1: plot global average, 2: plot
;    average of first half of provided times, plus average of second half...
;  /realspec: plot real 1D spectrum (instead of energy as a function
;    of shell)
;  /slope: plot slope of spectrum instead of the spectrum itself
;  colors: colors of curves at different times (24-bit true color values)
;  /diffcons: plot differences between consecutive spectra (in log10
;    space)
;  /diffav: plot differences with average spectrum (in log10 space)
;  compexp: compensate power-law by multiplying spectrum by k^compexp
;  compshift: compensate constant by multiplying spectrum by compshift
;  /fit: fit spectrum
;  frange: range for fitting spectrum (implies /fit)
; 
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  EB Eric Buchlin
;
; MODIFICATION HISTORY:
;  07 Nov 2005 EB Forked from sl_plot_spectra
;  15 Nov 2005 EB Added shift keyword
;  19 Nov 2005 EB Added /image and /heating keywords
;  23 Jan 2006 EB Added /tot keyword
;  24 Jan 2006 EB Added /taverage and /realspec keywords
;  29 Jan 2006 EB Added /slope keyword
;  24 May 2006 EB Added colors keyword, extended meaning of taverage
;  29 May 2006 EB Added /diffcons and /diffav keywords
;  02 Apr 2007 EB Added compexp and compshift keywords
;  28 Jan 2014 EB Added /fit and frange keywords
;-

pro sa_plot_specav, sf, uu=uu, bb=bb, ub=ub, bu=bu, tot=tot, time=time, $
                    heating=heating, image=image, nointerpol=nointerpol, $
                    xrange=xrange, yrange=yrange, $
                    atm=atm, shift=shift, taverage=taverage, $
                    realspec=realspec, slope=slope, colors=colors, $
                    diffcons=diffcons, diffav=diffav, $
                    compexp=compexp,  compshift=compshift, $
                    fit=fit, frange=frange

if keyword_set(uu)  then begin
    uu = 1
    bb = 0
endif else if keyword_set(bb) then begin
    bb = 1
    uu = 0
endif else if keyword_set(ub) or keyword_set(bu) or keyword_set(tot) then begin
    uu = 1
    bb = 1
endif else begin
    message, "You should ask for at least one of fields spectra: " + $
      "please specify /uu, /bb, /ub, /bu or /tot", /info
    return
endelse
ub = [uu, bb]
if n_elements(shift) eq 0 then shift = 0.d
if n_elements(compexp) eq 0 then compexp = 0
if n_elements(compshift) eq 0 then compshift = 1

if n_elements(atm) eq 0 then atm = 1

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)
nk = p.nmax - p.nmin + 1
logk = alog10 (p.k0 * p.lambda ^ (indgen (nk) + p.nmin))

nt = n_elements(time) 
if nt eq 0 then begin
    time = indgen (p.nrecsp)
endif else if nt eq 1 then begin
    time = [time]
endif
time = time > 0L < (p.nrecsp - 1L)
time = time [uniq (time)]
nt = n_elements (time)
if nt eq 0 then begin
    message, 'No valid time was provided, should be between 0 and ' + $
      string_ns (p.nrecsp - 1), /info
    return
endif

if keyword_set(image) then begin 
; get real times: tries to get them from out_time if out_time2 cannot
; be read
    readarray, 'out_time2', p.nrecsp, tspec, err=err
    if err ne 0 then begin
        readarray, 'out_time', p.nrecen, t
        itspec = lindgen (p.nrecsp) * (p.nrecen / p.nrecsp)
        tspec = t[itspec]
    endif
    tspec = tspec[time]
endif

; don't read more data than needed
p.nrecsp = max (time) + 1L

for i=0, 1 do begin
    ; i is current field: 0 is for u and 1 is for b
    ; Pass if no need to plot current field
    if ub[i] eq 0 then continue
    if i eq 0 then begin ; u
        if keyword_set(bu) then field = "u,b" else field = "u"
        readarray, 'out_specavu', long(nk) * p.nrecsp, s
    endif else begin ; b
        field = "b"
        if keyword_set(bu) then field = "u,b"
        if keyword_set(tot) then field = "tot"
        readarray, 'out_specavb', long(nk) * p.nrecsp, s
    endelse
    s = reform (s, nk, p.nrecsp, /overwrite)

    if keyword_set(tot) then begin
        if i eq 0 then begin
            if keyword_set(heating) then s *= p.nu
            s_sav = s
            continue
        endif
        if i eq 1 then begin
            if keyword_set(heating) then s *= p.eta
            s += s_sav
        endif
    endif

    s = s[*, time]

    if n_elements(taverage) eq 1 and nt gt 1 then begin
        ; reinitialize nt (needed if using /bu or /ub)
        nt = n_elements(time) 
        dtav = double (nt - 1) / double (taverage)
        for j=0, taverage - 1 do begin
            tmin = floor (dtav * j)
            tmax = floor (dtav * (j + 1))
;            print, tmin, tmax
            s[*, j] = total (s[*, tmin:tmax], 2) / (tmax - tmin + 1)
        endfor
        nt = taverage
        s = s[*, 0:taverage-1]
    endif    
 
    s = alog10 (s > 1.d-300)
    
 
    if keyword_set(heating) then begin
        if not keyword_set(tot)  then begin
            if i eq 0 then lognu = alog10 (p.nu) else lognu = alog10 (p.eta)
        endif else lognu = 0  ; if /tot, nu or eta already taken into account
        ; multiply spectrum by nu or eta k^2, so as to have heating
        for it=0, nt-1 do s[*, it] += 2.d * logk + lognu
    endif
    if keyword_set(realspec) then begin
        ; divide by k(lambda^2-1)/2 so as to have 1D spectrum
        for it=0, nt-1 do $
          s[*, it] -= logk + alog10 ((p.lambda ^ 2 - 1.d) / 2.d)
    endif
    if keyword_set(slope) then begin
        ; get slope (centered differences)
        dk2 = alog10 (p.lambda) * 2.
        if nt eq 1 then s = reform (s, nk, 1)
        s = (shift (s, -1, 0) - shift (s, 1, 0)) / dk2
        s[[0, nk-1], *] = 0  ; will be set to !values.f_nan before plot
    endif

    if keyword_set(diffcons) then begin
        ; compute differences between consecutive spectra (in the current,
        ; i.e. log10 space
        for j=0, nt-2 do s[*, j] = s[*, j + 1L] - s[*, j]
        s = s[*, 0:nt-2L]
        nt = nt - 1L
    endif else if keyword_set(diffav) then begin
        ; compute differences with average spectrum (in the current,
        ; i.e. log10 space
        avs = total (s, 2) / nt
        for j=0, nt-1 do s[*, j] -= avs 
    endif 

    ; compensate spectrum by power-law
    for j=0, nt-1 do s[*, j] +=  alog10 (compshift) + compexp * logk
    
    ; set ranges
    if n_elements(yrange) ne 2 then begin
        yrange = [min (s, max=tmp), tmp + (nt - 1) * shift]
        if yrange[0] le -50 then begin
            w = where (s gt -50, count)
            if count ne 0 then yrange[0] = min (s[w]) > (-50)
        endif
    endif else if keyword_set(image) then begin
        s = s > yrange[0] < yrange[1]
    endif
    if n_elements(xrange) ne 2 then xrange = [min (logk, max=tmp), tmp]

    if keyword_set(slope) then s[[0, nk-1], *] = !values.f_nan
 
    if keyword_set(image) then begin
        if keyword_set(heating) then begin
            title = 'Heating, ' + field
        endif else if keyword_set(realspec)  then begin
            title = 'Summed perpendicular 1D spectra, ' + field
        endif else begin
            title = 'Summed perpendicular spectra, ' + field
        endelse
        if compexp ne 0 then title += ', x k!U' + string_ns (compexp) + '!N'
        ; plot an image of axes (k,t)
        plot_image_eb, s, logk, tspec,  $
          title=title, xtitle='log10 k_perp', ytitle='t', /norm, $
          nointerpol=nointerpol
        print, min (s, max=tmp), tmp
    endif else begin
        ; plot as a function of k for some t's
        if not keyword_set(bu) or i eq 0 then begin
            if keyword_set(heating) then begin
                ytitle = 'Heating, ' + field
            endif else if keyword_set(realspec)  then begin
                ytitle = 'Summed perpendicular 1D spectra: log10 E' + field
            endif else begin
                ytitle = 'Summed perpendicular spectra: log10 E' + field
            endelse
            if compexp ne 0 then ytitle += ', x k!U' + string_ns (compexp) + '!N'
            plot, logk, logk, xrange=xrange, yrange=yrange, $
              xstyle=1, ystyle=3, /nodata, $
              xtitle='log10 k_perp', ytitle=ytitle
        endif
        ; colors for /bu options (both u and b, but with different colors)
        if keyword_set(bu) and i eq 1 then begin
            if !d.name eq 'PS' then begin
                loadct, 0
                tvlct, ctr, ctg, ctb, /get
                ctr[254] = 0
                ctg[254] = 0
                ctb[254] = 255
                tvlct, ctr, ctg, ctb
                !p.color = 254
            endif else begin
                !p.color = 255L * 65536L
            endelse
        endif
        ; colors with colors keyword (overrides /bu colors)
        nc = n_elements(colors) 
        if nc ge 1 and !d.name eq 'PS' then begin
            ; fill color table with colors, from 1 to 254
            loadct, 0
            tvlct, ctr, ctg, ctb, /get
            if nc gt 254 then begin
                message, 'Asked too many colors, maximum is 254', /info
                nc = 254
            endif
            for j=0, nc-1 do begin
                ctr[1+j] = colors[j] mod 256L
                ctg[1+j] = (colors[j] mod 65536L) / 256L
                ctb[1+j] = colors[j] / 65536L
            endfor
            tvlct, ctr, ctg, ctb
        endif
        for j=0L, nt-1L do begin
            if nc ge 1 then begin
                if !d.name eq 'PS' then !p.color = (j mod nc) + 1L $
                  else !p.color = colors[j mod nc]
            endif
            oplot, logk, s[*, j] + shift * j
            ; fit
            if keyword_set(fit) or n_elements(frange) eq 2 then begin
                  ifrange = lonarr (2)
               if n_elements(frange) eq 2 then begin
                  w = where (logk ge frange[0], c)
                  if c eq 0 then ifrange[0] = 0 else ifrange[0] = min (w)
                  w = where (logk le frange[1], c)
                  if c eq 0 then ifrange[1] = nk - 1L else ifrange[1] = max (w) 
                  endif else begin ; automatic fitting range selection: to fraction of dissipative wavenumber
                  ifrange[0] = 2
                  ; go back to real spectrum, assume k^2 viscosity to find dissipative wavenumber
                  rs = s[*, j] - logk * compexp
                  if not keyword_set(realspec) then rs -= logk
                  sm = max (s[*, j] +  2 * logk, ism)
                  ifrange[1] = (ism - 2) > (ifrange[0] + 1)
               endelse
               ; restricted x and y for fit
               logkr = logk[ifrange[0]:ifrange[1]]
               sr = s[ifrange[0]:ifrange[1], j]
               linpar = linfit (logkr, sr)
               oplot, logkr, linpar[0] + linpar[1] * logkr, thick=2
               sl = linpar[1] - compexp
               if not keyword_set(realspec) then sl -= 1.
               lkr2 = logkr[n_elements(logkr) * (1. + 2. * i) / 4.]
               xyouts, lkr2, linpar[0] + linpar[1] * lkr2, string_ns (sl, format='(f9.5)')
            endif
        endfor
    endelse
endfor
if !d.name eq 'PS' then !p.color = 0 else !p.color = !d.n_colors - 1

end
