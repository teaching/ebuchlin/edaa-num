;+
; NAME:
;  sa_plot_enbal
;
; PURPOSE:
;  Read necessary information and plot energy, and energy deduced from
;  integration of powers of energy gains and losses
;
; CALLING SEQUENCE:
;  sa_plot_enbal, sf, powscale=powscale, /bw, /xlog, /ylog, 
;       xrange=xrange, yrange=yrange, minmax=minmax, atm=atm
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  powscale: vertical scale to apply to powers; don't plot powers if
;  0; plot powers only if -1
;  /bw: black and white plot
;  /xlog: logarithmic x-axis
;  xrange: range on x-axis
;  yrange: range on y-axis
;  minmax: if set, the plot contains minmax couples of
;    points, with the min and max of the time series over some
;    interval around the point (saves memory or postscript file size)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot. A negative power contribution to the energy is shown as negative.
;  Energies:
;    thick yellow: total energy
;    thick magenta: expected energy
;    red: integrated dissipation power
;    green: integrated forcing power
;  Powers:
;    magenta: numerical dissipation
;    red: dissipation (explicit)
;    green: forcing
;  
;  Expected energy is estimated from initial energy, and integrated
;  difference between forcing and explicit dissipation powers.
;  Numerical dissipation is estimated from the derivative of the
;  difference between expected energy and energy.
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  12 Nov 2005 EB Creation (forked from sa_plot_energy)
;  04 May 2006 EB Added powscale=-1 option
;-

pro sa_plot_enbal, sf, powscale=powscale, bw=bw, xlog=xlog, $
                   xrange=xrange,  yrange=yrange, minmax=minmax, atm=atm

if n_elements(powscale) eq 0 then powscale = 1
if n_elements(atm) eq 0 then atm = 1
if keyword_set(xlog) then xlog = 1 else xlog = 0
if n_elements(minmax) eq 0 then minmax = 100000000L ; do not use minmax

if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)
nt = p.nrecen

readarray, 'out_time',  nt, t
readarray, 'out_dissu', nt, d
readarray, 'out_dissb', nt, db
readarray, 'out_enu',   nt, e
readarray, 'out_enb',   nt, eb
readarray, 'out_forcp', nt, f
d = d + db
e = e + eb

; restrict to requested xrange
if n_elements(xrange) ne 2 then begin
    xrange = [min(t, max=tmp), tmp]
    ix1 = 0
    ix2 = n_elements(t) - 1 
endif else begin
    ix1 = min (where (t ge xrange[0], count))
    if count eq 0 then ix1 = 0
    ix2 = max (where (t le xrange[1], count))
    if count eq 0 then ix2 = n_elements(t) - 1 
endelse
t =  t[ix1:ix2]
e =  e[ix1:ix2]
d =  d[ix1:ix2]
f =  f[ix1:ix2]
nt = n_elements(t) 

; integrate powers (d and f) (linear interpolation - trapezium)
sd = dblarr (nt)
sf = dblarr (nt)
for i=1L, nt - 1L do begin
    dt = t[i] - t[i-1]
    sd[i] = sd[i-1] + .5d * (d[i] + d[i-1]) * dt
    sf[i] = sf[i-1] + .5d * (f[i] + f[i-1]) * dt
endfor

; expected energy, deduced from integrated dissipation and forcing
ee = e[0] + sf - sd

; numerical dissipation: derivative of difference between expected
; energy and energy
nd = ((ee - e) - shift (ee - e, 1)) / (t - shift (t, 1))
nd[0] = 0

; reduce number of points
if nt ge minmax then begin
    t = congrid (t, 2L * minmax)
    step = double (nt - 1) / minmax

    tmpe = dblarr (2 * minmax)
    tmpd = dblarr (2 * minmax)
    tmpf = dblarr (2 * minmax)
    tmpee = dblarr (2 * minmax)
    tmpsd = dblarr (2 * minmax)
    tmpsf = dblarr (2 * minmax)
    tmpnd = dblarr (2 * minmax)
    for i=0, minmax - 1 do begin
        tmpe[2 * i] = max (e[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpe[2 * i + 1] = tmpmin
        tmpd[2 * i] = max (d[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpd[2 * i + 1] = tmpmin
        tmpf[2 * i] = max (f[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpf[2 * i + 1] = tmpmin
        tmpee[2 * i] = max (ee[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpee[2 * i + 1] = tmpmin
        tmpsd[2 * i] = max (sd[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpsd[2 * i + 1] = tmpmin
        tmpsf[2 * i] = max (sf[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpsf[2 * i + 1] = tmpmin
        tmpnd[2 * i] = max (nd[long(i * step) : long((i + 1L) * step)], $
                          min=tmpmin)
        tmpnd[2 * i + 1] = tmpmin
    endfor
    e = tmpe
    d = tmpd
    f = tmpf
    ee = tmpee
    sd = tmpsd
    sf = tmpsf
    nd = tmpnd
    nt = n_elements(t) 
endif


if powscale gt 0 then begin
    if powscale eq 1 then begin
        ytitle = 'Energy, integrated powers, powers'
    endif else begin
        ytitle = 'Energy, integrated powers, powers x ' + string_ns (powscale)
    endelse
endif else if powscale eq 0 then begin
    ytitle = 'Energy, integrated powers'
endif else if powscale eq -1 then begin
    ytitle = 'Powers'
endif

if n_elements(yrange) ne 2 then begin
    if powscale gt 0 then begin
        yrange = [min ([-sd, -d * powscale, sf, f * powscale, $
                        ee, e, -nd * powscale], $
                       max=tmp), tmp]
    endif else if powscale eq 0 then begin
        yrange = [min ([-sd, sf, ee, e], max=tmp), tmp]
    endif else if powscale eq -1 then begin
        yrange = [min ([-d, f, -nd], max=tmp), tmp]
    endif
endif

plot, [t[0], t[nt-1]], [0, 0], xrange=xrange, yrange=yrange, xlog=xlog, $
  xtitle='t', ytitle=ytitle, xstyle=1, ystyle=3, linestyle=1

if !d.name eq 'PS' then tvlct, r0, g0, b0, /get
if keyword_set(bw)  then begin
    if !d.name eq 'PS' then begin
        loadct, 0
        red = 0
        green = 0
        magenta = 0
        yellow = 128
    endif else begin
        white = 2L ^ 24 - 1
        red = white
        green = white
        magenta = white
        yellow = 128L + 128L * 256L + 128L * 65536L
    endelse
endif else begin
    if !d.name eq 'PS' then begin
        r = [r0[0], 255,   0, 255, 128, r0[5:*]]
        g = [g0[0],   0, 255,   0, 128, g0[5:*]]
        b = [b0[0],   0,   0, 255,   0, b0[5:*]]
        tvlct, r, g, b
        red = 1
        green = 2
        magenta = 3
        yellow = 4
    endif else begin
        red = 255L
        green = 255L * 256L
        blue = 255L * 65536L
        magenta = red + blue
        yellow = red + green
    endelse
endelse
if !d.name eq 'PS' then thick = 8 else thick = 4
if powscale ne -1 then begin
    oplot, t, e, color=yellow, thick=thick
    oplot, t, ee, color=magenta, thick=2
    oplot, t, -sd, color=red
    oplot, t, sf, color=green
endif
if powscale ne 0 then begin
    if powscale eq -1 then powscale = 1  ; powers alone, but not inverted!
    oplot, t, -d * powscale, color=red
    oplot, t, f * powscale, color=green
    oplot, t, -nd * powscale, color=magenta
endif
print, (ee[nt-1] - e[nt-1]) / ee[nt-1]

end

