;+
; NAME:
;  sa_plot_profile
;
; PURPOSE:
;  Read necessary information and plot profiles of Alfv�n speed,
;  Alfv�n speed derivative, magnetic field, mass density, or flux tube width
;
; CALLING SEQUENCE:
;  sa_plot_profile, sf, /vaa, /vad, /bfield, /bflux, /rho, /l,
;   /xlog, /ylog, xrange=xrange, yrange=yrange
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  At least one of these is needed:
;     /vaa:    plot Alfv�n speed
;     /vad:    plot Alfv�n speed derivative
;     /bfield: plot magnetic field
;     /bflux:  plot magnetic flux
;     /rho:    plot mass density
;     /l:      plot flux tube width
;
;  /xlog: logarithmic x-axis
;  /ylog: logarithmic y-axis
;  xrange: range of x-axis
;  yrange: range of y-axis
;  ytitle: title for y-axis (otherwise chosen according to plotted variable)
;
; OUTPUT:
;
; KEYWORD OUTPUT:
;  
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell-atm
;
; AUTHORS:
;  Eric Buchlin, e.buchlin@imperial.ac.uk
;
; MODIFICATION HISTORY:
;  23 Mar 2006 Creation
;-

pro sa_plot_profile, sf, vaa=vaa, vad=vad, rho=rho, bfield=bfield, l=l, $
                     aexp=aexp,  bflux=bflux, $
                     xlog=xlog, ylog=ylog, $
                     xrange=xrange, yrange=yrange, ytitle=ytitle


if keyword_set(xlog) then xlog = 1 else xlog = 0
if keyword_set(ylog) then ylog = 1 else ylog = 0
if n_elements(sf) eq 0 then sf = "simstate.dat"
p = sa_params_get (sf, atm=atm)

readarray, 'out_rad', p.nz, z
if n_elements(xrange) ne 2 then xrange = [min (z, max=tmp), tmp]

; data that can be read directly in files
if keyword_set(vaa) or keyword_set(bfield) or keyword_set(bflux) then begin
    readarray, 'out_va', p.nz, va
    if keyword_set(vaa) then begin
        d = va
        dtitle = 'Alfv�n speed Va'
    endif
endif
if keyword_set(rho) or keyword_set(bfield) or keyword_set(bflux) then begin
    readarray, 'out_rho', p.nz, rho
    if keyword_set(rho) then begin
        d = rho
        dtitle = 'Mass density rho'
    endif
endif
if keyword_set(aexp) or keyword_set(l) or keyword_set(bflux) then begin
    readarray, 'out_aexp', p.nz, aexp
    if keyword_set(aexp) then begin
        d = aexp
        dtitle = 'Over-expansion factor a_exp'
    endif
endif
if keyword_set(vad) then begin
    readarray, 'out_vad', p.nz, d
    dtitle = 'Alfv�n speed derivative dVa/dz'
endif
; data that need to be computed from the data read in files
if keyword_set(bfield) then begin
; the same conversion factor between the values of b and Tesla applies than
; between the values of u and m/s
    d = va * sqrt (4.d-7 * !pi * rho)
    dtitle = 'Magnetic field B_0'
endif
if keyword_set(l) then begin
    d = 2.d * !pi / p.k0 * sqrt (aexp)
    dtitle = 'Flux tube width l'
endif
if keyword_set(bflux) then begin
    ; area multiplied by magnetic field
    d = (!pi ^ 3 / p.k0 ^ 2 * aexp) * (va * sqrt (4.d-7 * !pi * rho))
    dtitle = 'Magnetic flux'
endif

if n_elements(d) eq 0 then begin
    message, 'A profile should be specified', /info
    return
endif

if n_elements(ytitle) eq 0 then ytitle = dtitle

if n_elements(yrange) ne 2 then yrange = [min (d, max=tmp), tmp]
plot, z, d, xlog=xlog, ylog=ylog, xtitle='Position z', ytitle=ytitle, $
  xrange=xrange, yrange=yrange

end

