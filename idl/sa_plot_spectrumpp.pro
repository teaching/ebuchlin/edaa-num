;+
; NAME:
;  sa_plot_spectrumpp
;
; PURPOSE:
;  Read necessary information and plot spectra in perpendicular and
;  parallel direction
;
; CALLING SEQUENCE:
;  sa_plot_spectrumpp, sf, /uu, /bb, /zm, /zp, /tot
;    xrange=xrange, yrange=yrange, atm=atm, cutk=cutk
;
; OPTIONAL INPUTS:
;  sf: file simstate.dat (default)
;
; KEYWORD PARAMETERS:
;  One of /uu, /bb, /zm, /zp or /tot needed:
;  /uu: plot velocity field spectrum
;  /bb: plot magnetic field spectrum
;  /zp: plot z+ Els�sser field spectrum
;  /zm: plot z- Els�sser field spectrum
;  /tot: plot total field spectrum
;
;  cutk: plot cuts in 2D spectrum for parallel and perpendicular
;    wavenumbers cutk
;
;  xrange: range for x axis
;  yrange: range for y axis
;  time: array of times (of indices of non-integrated output times) on
;    which to average (otherwise use sf)
;  atm: if 1 (default), is for shell-atm, if 0, is for shell-loop
;
; SIDE EFFECTS:
;  Show plot
;
; LANGUAGE:
;  IDL
;
; PROJECT:
;  shell IAS
;
; AUTHORS:
;  Eric Buchlin, eric@arcetri.astro.it
;
; MODIFICATION HISTORY:
;  01 Nov 2004 EB Forked from sa_plot_spectrum2d
;  07 Nov 2005 EB Adapted for shell-atm
;  09 Dec 2005 EB Take into account precise surface of shells (but
;    still missing other constant factors)
;  09 Dec 2005 EB Added cutk keyword
;  06 Feb 2006 EB Added time keyword. Replaced computation of 2D
;    spectrum by call to sa_plot_spectrum2d
;-

pro sa_plot_spectrumpp, sf, uu=uu, bb=bb, zp=zp, zm=zm, tot=tot, $
                        xrange=xrange, yrange=yrange, atm=atm, cutk=cutk, $
                        time=time

if keyword_set (uu)  then begin
    title = 'Velocity'
endif else if keyword_set(bb)  then begin
    title = 'Magnetic'
endif else if keyword_set(zp)  then begin
    title = 'z+'
endif else if keyword_set(zm)  then begin
    title = 'z-'
endif else if keyword_set(tot) then begin
    title = 'Total'
endif else begin
    message, 'Choose field: /uu, /bb, /zp, /zm or /tot', /info
    return
endelse

sa_plot_spectrum2d, sf, uu=uu, bb=bb, zp=zp, zm=zm, tot=tot, atm=atm, $
  time=time, spec=tt, kperp=kperp, kpar=kpar, /noplot

; cut at some log10 k (perp and par); linear interpolation if necessary
if n_elements(cutk) ne 0 then begin
    for ic=0, n_elements(cutk) - 1 do begin
        icperp1 = max (where (kperp le cutk[ic], count1))
        icperp2 = min (where (kperp ge cutk[ic], count2))
        if count1 eq 0 or count2 eq 0 then message, $
          'No corresponding kperp found'
        alpha = (cutk[ic] - kperp[icperp1]) / (kperp[icperp2] - kperp[icperp1])
        spar = tt[icperp1, *] * (1.d - alpha) + tt[icperp2, *] * alpha
        spar = alog10 (reform (spar))
        
        icpar1 = max (where (kpar le cutk[ic], count1))
        icpar2 = min (where (kpar ge cutk[ic], count2))
        if count1 eq 0 or count2 eq 0 then message, $
          'No corresponding kpar found'
        alpha = (cutk[ic] - kpar[icpar1]) / (kpar[icpar2] - kpar[icpar1])
        sperp = tt[*, icpar1] * (1.d - alpha) + tt[*, icpar2] * alpha
        sperp = alog10 (reform (sperp))

        if ic eq 0 then begin
            sparall  = spar
            sperpall = sperp
        endif else begin
            sparall  = [[sparall ], [spar ]]
            sperpall = [[sperpall], [sperp]]
            stop

        endelse
    endfor

    title = title + ' energy spectra for log10 k = ' + string_ns (cutk[0])
    for ic=1, n_elements(cutk) do title = title + ', ' + string_ns (cutk[ic])
endif else begin
    stt = size (tt)
    sperp = alog10 (total (tt, 2) / stt[2])
    spar = alog10 (total (tt, 1) / stt[1])
    title = title + ' average energy spectra'
endelse
if n_elements(xrange) ne 2 then xr = [min ([kperp, kpar], max=tmp), tmp] $
  else xr = xrange
if n_elements(yrange) ne 2 then yr = [min ([sperp, spar], max=tmp), tmp] $
  else yr = yrange
plot, kperp, sperp, xrange=xr, yrange=yr, xstyle=3, ystyle=3, $
  xtitle='log10 k', ytitle='log10 E', title=title
oplot, kpar, spar, linestyle=2

end
