!+
!***********************************************************
! Module types
!***********************************************************
! Definition of user-defined types
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  15 Jul 05: AV/EB: created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!-
module types
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_types = "default"

  ! "Double precision" numbers precision
  integer, parameter :: dfloat = selected_real_kind(15,300)
  real(kind=dfloat), parameter :: pi = 3.1415926535897932_dfloat

  integer, parameter :: nkforce = 3

  ! Parallel routine calls error code
  integer :: ierr
  ! Allocation error code
  integer :: aerr

!+
  ! Random coefficients of forcing
  type forcecoeffs
!-
     ! Both components on first boundary
      complex(kind=dfloat), dimension(0:nkforce-1) :: a1, b1
      !! allocatable members of types are not standard Fortran
!      complex(kind=dfloat), dimension(:), allocatable :: a1, b1
     ! Both components on second boundary
      complex(kind=dfloat), dimension(0:nkforce-1) :: a2, b2
      !! allocatable members of types are not standard Fortran
!      complex(kind=dfloat), dimension(:), allocatable :: a2, b2
   end type forcecoeffs

!+
  ! Parameters for the parallel run
  type mpiparams
!-
     ! processor rank
     integer :: me
     ! size (number of processors)
     integer :: np
  end type mpiparams



!+
  ! Parameters of simulation (including parameters of the parallel run)
  ! and for the expansion function
  type params
!-
     !    logarithmic discretization of k_n: k_n = k0 * lambda ^ n,
     !      n = nmin..nmax
     integer           :: nmin, nmax
     !    Total number of planes
     integer           :: tnz
     !    Number of planes (for this processor)
     integer           :: nz
     !    Index of plane 0 of this processor in the whole simulation box
     !    (goes from 0 to tnz-1)
     integer           :: nz0
     ! Indices of extremes planes (for this processor), taking into
     ! account the planes from the neighboring processors that are needed for
     ! the numerical schemes
     !integer           :: iz1, iz2
     real(kind=dfloat) :: k0
     real(kind=dfloat) :: lambda
     !    Model viscosity and magnetic diffusivity
     real(kind=dfloat) :: nu, eta
     !    Model "eps" and "epsm" parameter
     real(kind=dfloat) :: eps, epsm
     !    Magnetic field at the base (Gauss)
     real(kind=dfloat) :: b0
     !    Density at the base        (cm-3)
     real(kind=dfloat) :: rho0
     !    Beta plasma at base
     !real(kind=dfloat) :: beta0
     !    Distance between planes
     real(kind=dfloat) :: dz
     !    Number of time steps
     integer           :: nt
     !    Maximum value of time
     real(kind=dfloat) :: tmax
     !    Initial time step (then current time step)
     real(kind=dfloat) :: delta_t
     !    Continue from saved state ?
     logical           :: continue
     ! Both next ones are not relevant if we continue an existing simulation
     !    Initial field amplitude
     real(kind=dfloat) :: initamp
     !    Initial field spectrum power-law index
     real(kind=dfloat) :: initslope
     !    Forcing amplitude
     real(kind=dfloat) :: forcamp
     !    Correlation time of forcing
     real(kind=dfloat) :: tstar
     ! h scale height
     real(kind=dfloat) :: h

     !!! some additional variables that are not really parameters
     ! non linear timescale (now defined in type "timescales"!!)
     !real(kind=dfloat) :: taunl
     ! Power of forcing at boundaries
     real(kind=dfloat) :: forcp
     ! Parameters for the parallel run
     type(mpiparams) :: mpi
     ! Random coefficients of forcing
     type(forcecoeffs) :: fc
  end type params

!+
  ! What do we output, and at which frequency
  type saveflags
!-
     ! Time between each output of integrated quantities
     real(kind=dfloat) :: periodt
     ! Time between each output of non-integrated quantities
     real(kind=dfloat) :: specperiodt
     ! Number of planes to output in out_simstate_nnn
     integer :: outnz
  end type saveflags

!+
  ! Time scales as computed by the last call of check_time_scales  
  type timescales
!-
     ! Minimum non-linear time scale
     real(kind=dfloat) :: taunl
     ! Minimum Alfvén time scale
     real(kind=dfloat) :: taual
     ! Minimum dissipation (perpendicular) time scale
     real(kind=dfloat) :: taunu
     ! Crossing time scale
     real(kind=dfloat) :: taucr
  end type timescales

!+
  ! Number of records saved for each type of output
  type saverecords
!-
     integer :: energy
     integer :: spec
  end type saverecords

end module types
