!+
!***********************************************************
! Module iodata
!***********************************************************
! Output of data in files during the simulation,
! opening and closing the files
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module iodata
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_iodata = "formatted"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Open files useful for output
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_open_files (p, outflags)
!-
    implicit none
    type(params),    intent(in) :: p
    type(saveflags), intent(in) :: outflags

    open (10, file='out_time',     position='append', form='formatted')
    open (101, file='out_taunl',   position='append', form='formatted')
    open (102, file='out_time2i',  position='append', form='formatted')
    open (103, file='out_time2',   position='append', form='formatted')
    open (20, file='out_enu',      position='append', form='formatted')
    open (21, file='out_enb',      position='append', form='formatted')
    open (22, file='out_dissu',    position='append', form='formatted')
    open (23, file='out_dissb',    position='append', form='formatted')
    open (24, file='out_forcp',    position='append', form='formatted')
    open (30, file='out_specavu',  position='append', form='formatted')
    open (31, file='out_specavb',  position='append', form='formatted')
  end subroutine output_open_files

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files for integrated quantities
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_a (zp, zm, k, p, tscales, outflags, outrecords, &
       t, ti, lasttime)
    use diagnostics
    use iosimstate
!-
    implicit none
    type(params),         intent(in)                           :: p
    type(timescales),     intent(in)                           :: tscales
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                    :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    type(saveflags),      intent(in)                           :: outflags
    type(saverecords),    intent(inout)                        :: outrecords
    real(kind=dfloat),    intent(in)                           :: t
    integer,              intent(in)                           :: ti
    logical, optional,    intent(in)                           :: lasttime
    
    ! u and b ('save' is for better performance only)
    real   (kind=dfloat)                                       :: tmp
    complex(kind=dfloat), save, allocatable, dimension(:,:)    :: u  , b

    ! are the saved arrays already allocated?
    logical, save :: isallocated = .FALSE.

153 format (ES23.15E3)

    if (.NOT. isallocated) then
       allocate (   u(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (   b(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
       outrecords%energy = 0
    end if

    if (mod (t,  outflags%periodt) < p%delta_t) then
       ! time corresponding to output of integrated quantities
       write (10,153)  t
       ! last computed non-linear time scale
       write (101,153) tscales%taunl
       
       call zpm_to_ub (zp, zm, u, b, p)

       ! Kinetic and magnetic energies in whole box
       ! factor 2., because we compute the total kinetic and magnetic 
       ! energies from u and b, with a function designed for z...
       tmp = energy_tot (u, p)
       write (20,153) 2._dfloat * tmp
       tmp = energy_tot (b, p)
       write (21,153) 2._dfloat * tmp

       ! instantaneous dissipations in whole box
       tmp = diss_tot (u, k, p, p%nu)
       write (22,153) tmp
       tmp = diss_tot (b, k, p, p%eta)
       write (23,153) tmp

       ! power of forcing at boundaries, as computed by last call of
       ! propagate_and_force
       write (24,153) p%forcp

       outrecords%energy = outrecords%energy + 1
    end if

  end subroutine output_data_a


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files for non-integrated quantities
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_b (zp, zm, k, p, tscales, outflags, outrecords, &
       t, ti, lasttime)
    use diagnostics
    use iosimstate
!-
    implicit none
    type(params),         intent(inout)                        :: p
    type(timescales),     intent(in)                           :: tscales
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                  :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    type(saveflags),      intent(in)                           :: outflags
    type(saverecords),    intent(inout)                        :: outrecords
    real(kind=dfloat),    intent(in)                           :: t
    integer,              intent(in)                           :: ti
    logical, optional,    intent(in)                           :: lasttime

    integer                                                   :: i, j, q
    ! "local" time steps for remembering how many spectra we summed to
    ! compute average
    integer, save                                             :: tloc = 0

    ! u and b ('save' is for better performance only)
    real   (kind=dfloat)                                      :: tmp
    complex(kind=dfloat), save, allocatable, dimension(:,:)   :: u  , b
    ! averages of spectra
    real   (kind=dfloat), save, allocatable, dimension(:)     :: spavu, spavb

    ! are these saved arrays already allocated?
    logical, save :: isallocated = .FALSE.
    ! is the conversion to u and b already done?
    logical, save :: isubdone

153 format (ES23.15E3)

    if (.NOT. isallocated) then
       allocate (   u(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (   b(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate ( spavu(p%nmin:p%nmax), stat=aerr)
       spavu = 0
       if (aerr .ne. 0) stop 'Allocation error'
       allocate ( spavb(p%nmin:p%nmax), stat=aerr)
       spavb = 0
       if (aerr .ne. 0) stop 'Allocation error'

       isallocated = .TRUE.
       outrecords%spec = 0
    end if
    isubdone = .FALSE.
    
    !! Prepare output of spectra (averaged over time)
    ! Try to use 10 different times per specperiod to compute average
    if (mod (t,  outflags%specperiodt / 10._dfloat) < p%delta_t) then

       call zpm_to_ub (zp, zm, u, b, p)
       isubdone = .TRUE.
       
       ! Spectra
       ! In the end, the coefficients should be consistent with those
       ! of energy_tot1dk
       ! The sum of all values of the spectra should give the total energy
       do j = 0, p%nz - 1
          spavu(:) = spavu(:) + real (u(:, j) * conjg (u(:, j)), &
               kind=dfloat) * p%rho0
       end do
       do j = 0, p%nz - 1
          spavb(:) = spavb(:) + real (b(:, j) * conjg (b(:, j)), &
               kind=dfloat) * p%rho0 
       end do

       tloc = tloc + 1
    end if

    !! Output data (spectra, raw fields (simstate))
    if (mod (t, outflags%specperiodt) < p%delta_t) then

       ! perpendicular spectra, summed on all planes
       spavu = spavu / real (tloc, kind=dfloat)
       ! sum spectra from all processors, and write them
       do i = p%nmin, p%nmax
           write (30,153) spavu(i) * .5_dfloat * p%dz * pi ** 3 / p%k0 ** 2
       end do
       spavb = spavb / real (tloc, kind=dfloat)
       ! sum spectra from all processors, and write them
       do i = p%nmin, p%nmax
           write (31,153) spavb(i) * .5_dfloat * p%dz * pi ** 3 / p%k0 ** 2
       end do

       ! Raw fields (analyze later in IDL; this makes output procedures
       ! less clumsy and time-consuming, while allowing broader analyzis
       ! possibilities), split in one file per output.
       call write_simstate (zp, zm, p, outflags, outrecords, t, ti)

       ! Time of output of simulation state or spectra
       write (103,153) t

       outrecords%spec = outrecords%spec + 1
       tloc = 0
       spavu = 0
       spavb = 0
    end if

  end subroutine output_data_b



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Close files used for output
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_close_files (p, outflags)
!-
    implicit none
    type(params),    intent(in) :: p
    type(saveflags), intent(in) :: outflags

    close (10)
    close (101)
    close (102)
    close (103)
    close (20)
    close (21)
    close (22)
    close (23)
    close (24)
    close (30)
    close (31)

  end subroutine output_close_files

end module iodata
