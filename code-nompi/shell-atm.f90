!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!***********************************************************************
! MHD GOY shell-model-based coronal stratified atmosphere model
!***********************************************************************
!
! Constrained geometry, (static stratification, no wind)
! Each plane cross-section is a (pseudo-2D) shell-model
!   (see Giuliani & Carbone 1998)
!
! Parameters:
!  read in params.txt
!
! Output and output files:
!  see read_output_options
!
! Forcing: on modes 2-4 (see propagate_and_force) of photospheric planes
! 
! Structure:
!  Initialize:
!     read_parameters
!     read_output_options
!     get_wavenumbers
!     init_shells or read_simstate
!     output_open_files
!  Main loop:
!  |  Check if need to adapt time step for output
!  |  integrate_step
!  |  |  integrate_step_nonlin
!  |  |     derivs2d
!  |  |  dissipate_nuperp
!  |  |  propagate_wave
!  |  |  forcing
!  |  [check for numerical explosion and quit]
!  |  output_data_a
!  |  output_data_b
!  |  Every 10 steps: check_time_scales
!  |  Every 10^7/np steps: write_simstate
!  (repeat)
!  Quit:
!     output_close_files
!     write_simstate
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  28 Nov 02: EB forked from shell_mhd
!  10 Dec 02: EB Alfven wave propagation
!  06 Nov 03: EB 3rd order space Alfven wave propagation and integration
!                in Runge-Kutta time integration
!  11 Dec 03: EB Added Lax-Wendroff Alfven wave propagation
!                no 2D k-array anymore
!  17 Jan 04: EB Corrected bug in RK3 scheme implementation
!                Photospheric forcing as boundary conditions
!  20 May 04: EB Alfven wave propagation included in forcing instead of 
!                shell-models
!  08 Jul 04: EB Added Beam-Warming numerical scheme
!  10 Aug 04: EB Output of TauNL. Stop criterium on time (tmax).
!                Implicit dissipation in planes
!  14 Mar 05: EB/AV Parallel code (forked from shell-loop.f90)
!  14 Jul 05: AV/EB: merged shell-loop and stat-atm_sfer
!  07 Aug 05: EB Save random coefficients of forcing in simstate.dat
!                tstar is now a parameter read in params.txt
!  19 Aug 05: AV Completed module atmosphere (unif, cart, spher)
!                Added module for flux tube expansion, fexpand (none, MJ)
!                Added type p% and parameters file paramexp.txt
!  19 Aug 05: AV Added left and right for the call to sendrecv in 
!                propagate_and_force
!  22 Aug 05: AV Added normalization for Va, Va_m, Vad 
!                  - use CsN in the atmosphere module 
!                  - initial and forcing amplitude input as normalized 
!  22 Aug 05: AV To check numerical scheme added some TRUE/FALSE option
!                    to be changed before compiling
!                     - added simple starting amplitude at the base
!                        (in accordance with the forcing)
!  22 Aug 05: AV Corrected bug in isend irecv in 'propagate_and_force'
!                     - missing wait fot ozm
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Program shell_atm
  use types
  use nonlin
  use initialize
  use diagnostics
  use iodata
  use ioparams
  use iosimstate
!-
  implicit none

  ! Parameters of simulation (including parameters of the parallel run)
  type(params) :: p
  ! Time scales as computed by the last call of check_time_scales  
  type(timescales) :: tscales
  ! What do we output, and at which frequency
  type(saveflags) :: outflags
  ! Number of records saved for each type of output
  type(saverecords) :: outrecords

  ! Shell values
  complex(kind=dfloat), dimension(:,:), allocatable :: zp, zm
  ! Wavenumber values
  complex(kind=dfloat), dimension(:), allocatable :: k

  ! time step number
  integer           :: ti
  ! time (in model units)
  real(kind=dfloat) :: t
  ! total energy (for check of numerical 'explosion')
  real(kind=dfloat) :: en

  ! did we need to adapt the time step for the next output at fixed time?
  logical :: adaptdt
  ! saved delta_t, and new ones
  real(kind=dfloat) :: olddt, newdt1, newdt2

  ! external C routine for unlimiting stacksize
  call unlimitstack

  ! Initialize MPI variables (TO DO)

  ! Handle parameters and output options
  call read_parameters (p)
  call read_output_options (p, outflags)

  allocate (zp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'
  allocate (zm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'
  allocate (k (p%nmin:p%nmax), stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'

  call get_wavenumbers (k, p)

  call print_parameters (p)
  call print_output_options (p, outflags)

  if (p%continue) then
     ! continue from the final state of a previous simulation
     print*,"Continue from previous simulation"
     call read_simstate (zp, zm, p, t)
  else
     ! initialize fields
     ! Could be deprecated soon if we use external (IDL?) routines to
     ! initialize fields read by read_simstate
     print*,"New simulation"
     call init_shells (zp, zm, k, p)
     t = 0._dfloat
  end if
  
  ! open files for output (and leave them open until the end of the simulation)
  call output_open_files (p, outflags)
  ! output profiles of atmosphere
  call output_data_atm (p, outflags)

  call check_time_scales (zp, zm, k, p, 0, t, tscales)
  call output_data_a (zp, zm, k, p, tscales, outflags, outrecords, t, 0)
  call output_data_b (zp, zm, k, p, tscales, outflags, outrecords, t, 0)

  ! main loop
  print*, "*** Running main loop ***"
  call wallclock (p, 0, init=.TRUE.)

  do ti = 1, huge(1)

     if (t .gt. p%tmax) then 
        call check_time_scales (zp, zm, k, p, ti, t, tscales)
        exit
     end if

     ! adapt time step to next output at given time
     ! if requested and near enough (less than current delta_t)
     adaptdt = .FALSE.
     if (outflags%periodt - mod (t, outflags%periodt) < p%delta_t) then
        newdt1 = outflags%periodt - mod (t, outflags%periodt)
        adaptdt = .TRUE.
     else
        newdt1 = huge (p%delta_t)
     end if
     if (outflags%specperiodt - mod (t, outflags%specperiodt) < p%delta_t) then
        newdt2 = outflags%specperiodt - mod (t, outflags%specperiodt)
        adaptdt = .TRUE.
     else
        newdt2 = huge (p%delta_t)
     end if
     if (adaptdt) then
        olddt = p%delta_t
        ! Increment delta_t of an epsilon of t, not to stay 
        ! in the condition of coincidence with periodt or specperiodt
        p%delta_t = min (newdt1, newdt2) + t * epsilon (t)
     end if

     ! one time step forward...
     call integrate_step (zp, zm, k, p, t)
     t = t + p%delta_t
     ! revert to normal time step
     if (adaptdt) p%delta_t = olddt

     ! output data for integrated quantities
     call output_data_a (zp, zm, k, p, tscales, outflags, outrecords, t, ti)
     ! output data for non-integrated quantities
     call output_data_b (zp, zm, k, p, tscales, outflags, outrecords, t, ti)

     ! check for numerical explosion and quit if necessary
     en = energy_tot (zp, p) + energy_tot (zm, p)
     if (en > 1e100_dfloat) then
        print*, 'Energy too large= ', en
        call check_time_scales (zp, zm, k, p, ti, t, tscales)
        exit
     end if

     ! update timestep
     call check_time_scales (zp, zm, k, p, ti, t, tscales)
     if (p%delta_t < 1e-12_dfloat) then
        p%delta_t = 1e-12_dfloat
        ! high probability of imminent numerical explosion...
        print*, "Warning: Suspicious time scales. Time step set to", &
             p%delta_t
     end if

     ! output current simulation state every 10^7 / p%nz time steps
     ! to allow for restarting from this point
     if (mod (ti, floor (1.d7 / p%tnz)) == 0) then
       call write_simstate (zp, zm, p, outflags, outrecords, t, ti, &
            simstatefile=.TRUE.)
       call flush()
       ! and write timestamps on standard output
       call wallclock (p, ti)
    end if

  end do

  call wallclock (p, ti)

  call output_data_a (zp, zm, k, p, tscales, outflags, outrecords, t, ti, &
       lasttime=.TRUE.)
  call output_data_b (zp, zm, k, p, tscales, outflags, outrecords, t, ti, &
       lasttime=.TRUE.)
  ! close the output files
  call output_close_files (p, outflags)

  ! write simulation state
  ! (for use by IDL and to allow for continuation of this run by another run)
  call write_simstate (zp, zm, p, outflags, outrecords, t, ti, &
       simstatefile=.TRUE.)

  deallocate (zp, stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'
  deallocate (zm, stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'
  deallocate (k, stat=aerr)
  if (aerr .ne. 0) stop 'Allocation error'


contains



!+
!***********************************************************
! Routines for equation integration
!***********************************************************
!-


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Perform one step of the shell-model time integration
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine integrate_step (zp, zm, k, p, t)
    use boundary
    use propagation
!-
    implicit none
    type(params),         intent(inout)                        :: p
    complex(kind=dfloat), intent(inout), & 
         dimension(p%nmin:p%nmax, 0:p%nz-1)                    :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real(kind=dfloat) :: t

    ! integrate RHS of shell-model
    call integrate_step_nonlin (zp, zm, k, p)
    ! implicit scheme for dissipation in planes
    call dissipate_nuperp (zp, zm, k, p)
    ! Alfven wave propagation
    call propagate (zp, zm, k, p, t)
    !  Boundary conditions (reflection and/or forcing)
    call force (zp, zm, k, p, t)
  end subroutine integrate_step



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Implicit scheme for dissipation in planes
! Assumes a Prandtl number = 1: p%eta = p%nu
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine dissipate_nuperp (zp, zm, k, p)
!-
    implicit none
    type(params),         intent(in)                              :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k

    logical, save :: firsttime = .TRUE.
    integer :: iz

    if (firsttime .and. p%nu .ne. p%eta) then
       print*, 'Warning: implicit dissipation used but Prandtl number'
       print*, '  is not 1. Now assuming eta is the same as nu.'
    end if
    do iz=0, p%nz - 1
       zp(:,iz) = zp(:,iz) * exp (-p%nu * p%delta_t * k ** 2)
       zm(:,iz) = zm(:,iz) * exp (-p%nu * p%delta_t * k ** 2)
    end do

    firsttime = .FALSE.

  end subroutine dissipate_nuperp

End Program shell_atm

