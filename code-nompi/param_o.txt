# Period for output of integrated quantities (typically at small time scale)
.01
# Period for output of non-integrated quantities (typically at larger time scale)
.05
# Number of planes to output in out_simstate_nnn (all if 0)
100
