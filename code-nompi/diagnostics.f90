!+
!***********************************************************
! Module diagnostics
!***********************************************************
! Computation of diagnostics from fields, integrated quantities, conversions
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!  05 Nov 05: EB Take density into account
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module diagnostics
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_diagnostics = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy (massic density) contained in shells
! of a loop cross-section
! As we don't know the density in this section or its position,
! we can't take it into account right here
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_tot1d (z, p)
!-
    implicit none
    type   (params),      intent(in)                  :: p
    real   (kind=dfloat)                              :: energy_tot1d
    complex(kind=dfloat), intent(in), dimension(:)    :: z

    energy_tot1d = real (dot_product (z, z), kind=dfloat) * 0.25_dfloat

  end function energy_tot1d



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in shells of a field (whole
! box, all processors)
! Take into acount mass density, and separation between planes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_tot (z, p)
!-
    implicit none
    type   (params),      intent(in)                   :: p
    real   (kind=dfloat)                               :: energy_tot, tmpe
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)          :: z

    integer :: i

    ! compute total energy contained in shells of a field
    ! (all planes in one processor)
    tmpe = 0._dfloat
    do i = 0, p%nz - 1
       ! take into account effect of boundary conditions (reflection):
       ! the boundary planes should be counted half
       if ((i .eq. 0) .or. (i .eq. p%nz-1)) then
          tmpe = tmpe + energy_tot1d (z(:, i), p) * p%rho0 * .5_dfloat
       else
          tmpe = tmpe + energy_tot1d (z(:, i), p) * p%rho0
       end if
    end do
    
    ! convert this volumic energy to an energy
    energy_tot = tmpe * p%dz * pi ** 3 / p%k0 ** 2
  end function energy_tot





!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total dissipation for a field u or b (whole
!   box, all processors)
! Argument field is u or b
! Take into acount mass density, and separation between planes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function diss_tot (z, k, p, disscoeff)
!-
    implicit none
    type   (params),      intent(in)           :: p
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real   (kind=dfloat), intent(in)           :: disscoeff

    real   (kind=dfloat)                       :: diss_tot, tmpd
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)    :: z

    integer :: i

    ! compute total dissipation contained in shells of a field
    ! (all planes of one processor)
    tmpd = 0._dfloat
    do i = 0, p%nz - 1
       ! take into account effect of boundary conditions (reflexion)
       ! see also energy_tot
       if ((i .eq. 0) .or. (i .eq. p%nz-1)) then
          tmpd = tmpd + energy_tot1d (z(:, i) * k, p) * p%rho0 * 2._dfloat
       else
          tmpd = tmpd + energy_tot1d (z(:, i) * k, p) * p%rho0 * 4._dfloat
       end if
    end do

    ! convert this volumic energy to an energy
    diss_tot = disscoeff * tmpd * p%dz * pi ** 3 / p%k0 ** 2
  end function diss_tot




!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (u, b) to (zp, zm) for the planes of one processor
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine ub_to_zpm (u, b, zp, zm, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: u, b
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: zp, zm

    zp = u + b
    zm = u - b

  end subroutine ub_to_zpm




!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (zp, zm) to (u, b) for the planes of one processor
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine zpm_to_ub (zp, zm, u, b, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: u, b

    u = (zp + zm) / 2._dfloat
    b = (zp - zm) / 2._dfloat

  end subroutine zpm_to_ub




!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Check whether time scales are shorter than time step
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine check_time_scales (zp, zm, k, p, ti, t, tscales)
!-
    implicit none
    type(params),         intent(inout)                        :: p
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                    :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    integer             , intent(in)                           :: ti
    real(kind=dfloat)   , intent(in)                           :: t
    type(timescales)    , intent(inout)                        :: tscales

    real(kind=dfloat) :: tmp

    integer :: iz
    integer, save :: ic = 0

    ! compute smallest non-linear time scale
    tmp = 0
    do iz = 0, p%nz - 1
       tmp = max (tmp, &
            maxval (abs (k * zp(:,iz) )), &
            maxval (abs (k * zm(:,iz) )))
    end do
    tscales%taunl = 1._dfloat / tmp

    if (ic .eq. 0) then ! do this only at the first call
       ! compute smallest dissipation time scale
       tmp = abs (k(p%nmax) ** 2)
       tscales%taunu = min (1._dfloat / p%nu , 1._dfloat / p%eta) / tmp

       ! compute smallest Alfven time scale
       tscales%taual = p%dz / p%b0

       ! compute crossing time
       tscales%taucr = p%nz * p%dz / p%b0
    end if

    ! Update time step (adaptative time step)
    ! CFL condition with security factor
    ! No constraint from taunu as dissipation is implicit
    p%delta_t = min (tscales%taunl, tscales%taual) / 5._dfloat

    ! Time step and scales information
100 format ('ti:', i10, '  t:', ES12.5, '  dt:', ES10.3, &
        '  tNL:', ES10.3)
101 format ('         tcr:', ES10.3, '  tnu:', ES10.3, '  tAl:', ES10.3)
    if (mod (ic, 100) == 0) then
      print 100, ti, t, p%delta_t, tscales%taunl
      if (mod (ic, 1000) == 0) print 101,  &
                tscales%taucr, tscales%taunu, tscales%taual
      call flush (6)
    end if

    ic = ic + 1

  end subroutine check_time_scales


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialize or print wallclock time needed for simulation
! Prints information on efficiency
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine wallclock (p, ti, init)
!-
    type(params), intent(in)           :: p
    integer,      intent(in)           :: ti
    logical,      intent(in), optional :: init
    real(kind=dfloat), save            :: clock0, clock1
    real(kind=dfloat)                  :: cd

    if (present (init) .and. init) then
      call cpu_time (clock0)
    else
      call cpu_time (clock1)
      cd = clock1 - clock0
      print '(A,ES10.3,A,ES10.3,A)', 'Simulation duration: ', cd, &
            ' s   (',  cd / ti / p%tnz * p%mpi%np, ' s/nt/nz*np )'
      ! (don't write     / (ti * p%nz) to avoid integer overflows)
    end if
  end subroutine wallclock

end module diagnostics
