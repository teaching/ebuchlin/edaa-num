!+
!***********************************************************
! Module propagation-laxw
!***********************************************************
! Variables and routines for Alfven wave propagation
! With Lax-Wendroff numerical scheme (inside the domaine)
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  06 Jan 17 EB Created (forked from propagation-fromm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module propagation
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_propagation = "laxw"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Alfven wave propagation, Lax-Wendroff numerical scheme inside the box.
! zp on first plane and zm on last plane are _not_ computed (this is the
! job of the force routine)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine propagate (zp, zm, k, p, t)
!-
    implicit none
    type(params),         intent(inout)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! old values in whole box
    complex(kind=dfloat), dimension(:,:), save, allocatable :: ozp, ozm

    ! are the local fields already allocated?
    logical, save :: isallocated = .FALSE.
    integer :: iz, istart, iend
    real(kind=dfloat) :: lam

    if (.NOT. isallocated) then
       ! Old fields, with needed extension (ghost cells)
       allocate (ozp(p%nmin:p%nmax, -1:p%nz), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (ozm(p%nmin:p%nmax, -1:p%nz), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
    end if

!!! propagation by Lax-Wendroff numerical scheme inside the box, and 
!!! lower-order schemes at the boundaries (avoid boundary problem)

    !! Old values of fields
    ozp(:,0:p%nz-1) = zp
    ozm(:,0:p%nz-1) = zm

    !! TO DO (mpi_sendrecv):
    ! send ozp to the right and receive it from the left
    ! send ozp to the left  and receive it from the right
    ! send ozm to the right and receive it from the left
    ! send ozm to the left  and receive it from the right

    lam  = p%delta_t / p%dz

    !! update zp

    ! For first processor, start at iz=1: the new zp(0) is computed later
    ! (forcing).
    istart = 1
    iend = p%nz - 2

    ! For last processor at iz=p%nz-1, we need an entirely upwind scheme,
    ! we choose Beam-Warming
    iz = p%nz - 1
    zp(:,iz) = ozp(:,iz) * (1._dfloat - lam * p%b0 &
        - .5_dfloat * lam * p%b0   * (1._dfloat - lam * p%b0)) &
        + ozp(:,iz-1) * (.5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) &
        + .5_dfloat * lam * p%b0   * (1._dfloat - lam * p%b0) + lam * p%b0) &
        - ozp(:,iz-2) * .5_dfloat * lam * p%b0  * (1._dfloat - lam * p%b0)

    ! Lax-Wendroff numerical scheme, inside the box (excluding boundaries)
    do iz=istart, iend
        zp(:,iz) = ozp(:,iz) + &
              (- (ozp(:,iz) - ozp(:,iz-1)) * 0.5_dfloat * & 
              lam * p%b0 * (1._dfloat + lam * p%b0)) &
              - (ozp(:,iz+1) - ozp(:,iz)) * lam * p%b0 * 0.5_dfloat * &
              (1._dfloat - lam * p%b0)
    end do

    !! update zm
    ! For last processor, end at iz=nz-2: the new zm(nz-1) is
    ! computed later (forcing)
    iend = p%nz - 2
    istart = 1

    ! For first processor at iz=0, we need an entirely upwind scheme,
    ! we choose Beam-Warming
    iz = 0
    zm(:,iz) = ozm(:,iz) * (1._dfloat - lam * p%b0 &
        - .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0)) &
        + ozm(:,iz+1) * (.5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) &
        + .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) + lam * p%b0)  &
        - ozm(:,iz+2) * .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0)

    ! Lax-Wendroff numerical scheme, inside the box (excluding boundaries)
    do iz=istart, iend
        zm(:,iz) = ozm(:,iz) + &
              ((ozm(:,iz+1) - ozm(:,iz)) * 0.5_dfloat * & 
              lam * p%b0 * (1._dfloat + lam * p%b0) &
              + (ozm(:,iz) - ozm(:,iz-1)) * lam * p%b0 &
              * 0.5_dfloat * (1._dfloat - lam * p%b0))
    end do

  end subroutine propagate

end module propagation
