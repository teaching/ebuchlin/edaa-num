!+
!***********************************************************
! Module ioparams
!***********************************************************
! Input and output of parameters of simulation and of output
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module ioparams
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_ioparams = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read parameters from file params.txt
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_parameters (p)
!-
    implicit none
    type(params), intent(inout) :: p

    ! local variables
    character(len=80)         :: dummy
    integer                   :: tmpint
    real(kind=dfloat)         :: tmp
    integer                   :: excessplanes

    open (90, file='params.txt', status='old')
    read(90,*) dummy;   read(90,*) p%nmin
    read(90,*) dummy;   read(90,*) p%nmax
    read(90,*) dummy;   read(90,*) p%tnz
    read(90,*) dummy;   read(90,*) p%k0
    read(90,*) dummy;   read(90,*) p%lambda
    read(90,*) dummy;   read(90,*) p%nu
    read(90,*) dummy;   read(90,*) p%eta
    read(90,*) dummy;   read(90,*) p%eps
    read(90,*) dummy;   read(90,*) p%epsm
    read(90,*) dummy;   read(90,*) p%b0
    read(90,*) dummy;   read(90,*) p%dz
    read(90,*) dummy;   read(90,*) p%tmax
    read(90,*) dummy;   read(90,*) tmpint
    if (tmpint == 0) then
      p%continue = .FALSE.
    else
      p%continue = .TRUE.
    end if
    read(90,*) dummy;   read(90,*) p%initamp
    read(90,*) dummy;   read(90,*) p%initslope
    read(90,*) dummy;   read(90,*) p%forcamp
    read(90,*) dummy;   read(90,*) p%tstar
    p%rho0 = 1._dfloat
    close (90)
   
    ! other variables contained in p
    p%forcp = 0._dfloat

    ! deduce p%nz for current processor from p%tnz
    p%nz = p%tnz ! this is for one processor

    ! initialize random coefficients of forcing
    ! These will be overwritten by those read in simstate.dat
    ! if p%continue is true
    p%fc%a1 = 0;   p%fc%b1 = 0
    p%fc%a2 = 0;   p%fc%b2 = 0

  end subroutine read_parameters


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters on standard output
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine print_parameters (p)
    use boundary
    use diagnostics
    use initialize
    use iodata
    use iosimstate
    use nonlin
    use propagation
    use types
!-
    implicit none
    type(params), intent(in)     :: p

    character (len=*), parameter :: form1 = "(A, I12,    A, I12   )"
    character (len=*), parameter :: form2 = "(A, ES12.5, A, ES12.5)"
    character (len=*), parameter :: form3 = "(A, I12   , A, ES12.5)"
    character (len=*), parameter :: form4 = "(A, I12   )"
    character (len=*), parameter :: form5 = "(A, ES12.5)"

    print*, '*** Loaded modules ***'
    print*, 'boundary:    ', module_boundary
    print*, 'diagnostics: ', module_diagnostics
    print*, 'initialize:  ', module_initialize
    print*, 'iodata:      ', module_iodata
    print*, 'ioparams:    ', module_ioparams
    print*, 'iosimstate:  ', module_iosimstate
    print*, 'nonlin:      ', module_nonlin
    print*, 'propagation: ', module_propagation
    print*, 'types:       ', module_types
!    print*, 'usempi:      ', module_usempi
    print*, ''
    print*, '*** Parameters for the model ***'
    print form1, "      N: ", p%nmin,    "         .. ", p%nmax
    print form2, "     k0: ", p%k0,      "    lambda: ", p%lambda
    print form4, "     Nz: ", p%tnz
    print form2, "    eps: ", p%eps,     "      epsm: ", p%epsm
    print form2, "     nu: ", p%nu,      "       eta: ", p%eta
    print form2, "     b0: ", p%b0,      "        dz: ", p%dz
    print form2, "   Tmax: ", p%tmax,  "  dt_init: ", p%delta_t
    if (.not. p%continue) then
      print form2, "initamp: ", p%initamp, " initslope: ", p%initslope
    end if
    print form5, "forcamp: ", p%forcamp
    print form5, "tstar  : ", p%tstar
    print*, ''
    print*, '*** Parameters for parallel run ***'
    print form4, 'Number of processors: ', p%mpi%np

  end subroutine print_parameters


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files - Atmosphere profiles
! Should be done only once a the beginning
! MPI: OK
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_atm (p, outflags)
!-
    implicit none
    type(params),         intent(in)                       :: p
    type(saveflags),      intent(in)                       :: outflags
    integer :: i

153 format (ES23.15E3)

    ! Alfven velocity
    open (50,  file='out_va',   status='replace', form='formatted')
    do i = 0, p%tnz - 1
      write (50,153) p%b0
    end do
    close (50)
    ! Alfven velocity gradient
    open (51,  file='out_vad',  status='replace', form='formatted')
    do i=0, p%tnz - 1
      write (51,153) 0.
    end do
    close (51)
    ! Density
    open (52,  file='out_rho',  status='replace', form='formatted')
    do i=0, p%tnz - 1
      write (52,153) p%rho0
    end do
    close (52)
    ! distances from the base
    open (53,  file='out_rad',  status='replace', form='formatted')
    do i=0, p%tnz - 1
      write (53,153) i * p%dz
    end do
    close (53)
    ! Expansion factor
    open (54, file='out_aexp', status='replace', form='formatted')
    do i=0, p%tnz - 1
      write (54,153) 1.
    end do
    close (54)

  end subroutine output_data_atm


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get output options
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_output_options (p, outflags)
!-
    implicit none
    type(params),    intent(in)  :: p
    type(saveflags), intent(out) :: outflags
    character(len=80)            :: dummy
    integer                      :: tmpint
    real(kind=dfloat)            :: tmpdfl

    open (92, file='param_o.txt', status='old')
    read(92,*) dummy;   read(92,*) outflags%periodt
    read(92,*) dummy;   read(92,*) outflags%specperiodt
    ! by default, output all planes in out_simstate_nnn
    read(92,*) dummy;   read(92,*) outflags%outnz
    if (outflags%outnz .gt. p%tnz .or. outflags%outnz .le. 0) &
        outflags%outnz = p%tnz

  end subroutine read_output_options



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print output options on standard output
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine print_output_options (p, outflags)
!-
    implicit none
    type(params),    intent(in)  :: p
    type(saveflags), intent(in) :: outflags
    character (len=*), parameter :: form1 = "(a, i7, a)"
    character (len=*), parameter :: form2 = "(a, i5, a, i5)"
    character (len=*), parameter :: form3 = "(a, i3, a, i9)"
    character (len=*), parameter :: form4 = "(a, es12.5e2, a)"

    print*, '*** Output options ***'
    print form4, " Output every ", outflags%periodt, " "
    print form4, "    and every ", outflags%specperiodt, " (spectra, fields...)"
    print*, ''
  end subroutine print_output_options

end module ioparams
