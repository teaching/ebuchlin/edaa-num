!+
!***********************************************************
! Module initialize
!***********************************************************
! Initialization of fields, of atmosphere, of wavenumbers and
! of random numbers
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module initialize
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_initialize = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialization of shells values
! We can also write the initial fields in simstate.dat
! by an external program (or IDL or Python script) and start from there
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine init_shells (zp, zm, k, p)
    use diagnostics
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: zp, zm
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k

    zp = 0._dfloat
    zm = 0._dfloat

  end subroutine init_shells



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return in array k the values of the wavenumbers for each
! index value in the perpendicular direction
! Initialize random number generator
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine get_wavenumbers (k, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(out), dimension(p%nmin:p%nmax) :: k 

    integer                                                     :: i, n

    integer, dimension(:), allocatable :: randseed

    ! get size of seed and current seed and initialize to a fixed value
    call random_seed (size=n)
    allocate (randseed (n))
    do i = 1, n
      randseed(i) = i
    end do
    call random_seed (put=randseed)

    ! Initialize array of wavenumbers corresponding to each shell
    k(p%nmin) = (p%k0 * (p%lambda ** p%nmin)) * (1._dfloat, 0._dfloat)
    do i = p%nmin + 1, p%nmax
       k(i) = k(i - 1) * p%lambda
    end do

  end subroutine get_wavenumbers

end module initialize
