!+
!***********************************************************
! Module iosimstate-formatted
!***********************************************************
! Input and output of simulation state, formatted data
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!  18 Nov 05: EB Made a module alternative (other is for unformatted output)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module iosimstate
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_iosimstate = "formatted"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read state from which the simulation should continue
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_simstate (zp, zm, p, t)
!-
    implicit none
    type(params), intent(inout) :: p
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    real(kind=dfloat), intent(out) :: t
    type(params)      :: oldp
    integer           :: i, j
    real(kind=dfloat) :: fval1, fval2
    real(kind=dfloat) :: dummy
    character (len=*), parameter :: formi = "(a, i12, a, i12)"
    character (len=*), parameter :: formf = "(a, es12.5e2, a, es12.5e2)"
    
    print*, "Reading simulation state"

    open (91, file='simstate.dat', status='old')

    ! check parameters
    read(91,*) oldp%nmin;   read(91,*) oldp%nmax
    read(91,*) oldp%tnz
    read(91,*) oldp%k0;     read(91,*) oldp%lambda
    read(91,*) oldp%nu;     read(91,*) oldp%eta
    read(91,*) oldp%eps;    read(91,*) oldp%epsm
    read(91,*) oldp%b0;     read(91,*) oldp%dz
    read(91,*) oldp%nt;     read(91,*) oldp%delta_t
    if (p%nmin /= oldp%nmin) then
      print formi, "Warning: parameter nmin has changed.    Old: ", &
            oldp%nmin   , " New: ", p%nmin
    end if
    if (p%nmax /= oldp%nmax) then
      print formi, "Warning: parameter nmax has changed.    Old: ", &
            oldp%nmax   , " New: ", p%nmax
    end if
    if (p%nmax - p%nmin /= oldp%nmax - oldp%nmin) then
      print *, "Error: number of modes has changed."
      stop
    end if
    if (p%tnz /= oldp%tnz) then
      print *, "Error: number of loop sections has changed."
      stop
    end if
    if (p%k0 /= oldp%k0) then
      print formf, "Warning: parameter k0 has changed.      Old: ", &
            oldp%k0     , " New: ", p%k0
    end if
    if (p%lambda /= oldp%lambda) then
      print formf, "Warning: parameter lambda has changed.  Old: ", &
            oldp%lambda , " New: ", p%lambda
    end if
    if (p%eta /= oldp%eta) then
      print formf, "Warning: parameter eta has changed.     Old: ", &
            oldp%eta    , " New: ", p%eta
    end if
    if (p%nu /= oldp%nu) then
      print formf, "Warning: parameter nu has changed.      Old: ", &
            oldp%nu     , " New: ", p%nu
    end if
    if (p%eps /= oldp%eps) then
      print formf, "Warning: parameter eps has changed.     Old: ", &
            oldp%eps    , " New: ", p%eps
    end if
    if (p%epsm /= oldp%epsm) then
      print formf, "Warning: parameter epsm has changed.    Old: ", &
            oldp%epsm   , " New: ", p%epsm
    end if
    if (p%epsm /= oldp%b0) then
      print formf, "Warning: parameter b0 has changed.      Old: ", &
            oldp%b0     , " New: ", p%b0
    end if
    if (p%dz /= oldp%dz) then
      print formf, "Warning: parameter dz has changed.      Old: ", &
            oldp%dz     , " New: ", p%dz
    end if

    ! number of output records (not useful for restarting; only for data analysis)
    read(91,*) dummy
    read(91,*) dummy
    read(91,*) dummy

    ! get shell values
    do j = 0, p%tnz - 1
        do i = p%nmin, p%nmax
          read(91,*) fval1
          read(91,*) fval2
          zp(i, j) = cmplx (fval1, fval2, kind=dfloat)
        end do
    end do
    do j = 0, p%tnz - 1
       do i = p%nmin, p%nmax
          read(91,*) fval1
          read(91,*) fval2
          zm(i, j) = cmplx (fval1, fval2, kind=dfloat)
       end do
    end do

    ! read current time
    read(91,*) t
    ! Check correlation time of forcing
    read(91,*) oldp%tstar
    if (p%tstar /= oldp%tstar) then
      print formf, "Warning: parameter tstar has changed.      Old: ", &
            oldp%tstar     , " New: ", p%tstar
    end if
    read(91,*) dummy
    if (nkforce /= dummy) then
      print formf, "Error: number of forced modes has changed. Old: ", &
            dummy     , " New: ", nkforce
      stop
    end if

    ! Read random coefficients of forcing
    do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
      read(91,*) fval1;   read(91,*) fval2
      p%fc%a1(i) = cmplx (fval1, fval2, kind=dfloat)
      read(91,*) fval1;   read(91,*) fval2
      p%fc%b1(i) = cmplx (fval1, fval2, kind=dfloat)
    end do
    do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
      read(91,*) fval1;   read(91,*) fval2
      p%fc%a2(i) = cmplx (fval1, fval2, kind=dfloat)
      read(91,*) fval1;   read(91,*) fval2
      p%fc%b2(i) = cmplx (fval1, fval2, kind=dfloat)
    end do

    close (91)

  end subroutine read_simstate


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Write state from which the simulation could continue
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine write_simstate (zp, zm, p, outflags, outrecords, t, ti, &
       simstatefile)
!-
    implicit none
    type(params),      intent(inout) :: p
    type(saverecords), intent(in) :: outrecords
    type(saveflags),   intent(in) :: outflags
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    real(kind=dfloat), intent(in) :: t
    integer, intent(in)           :: ti
    logical, optional, intent(in) :: simstatefile
    ! if true, write in simstate.dat
    ! if false (default), write in out_simstate_00000000ti
    
    integer                       :: i, j, zstep, realoutnz
    character(len=23) :: filename

    if (present (simstatefile) .and. simstatefile) then
      filename = 'simstate.dat'
    else
      write (fmt='(i10.10)', unit=filename) ti
      write(102,'(i10.10)') ti
      filename = 'out_simstate_' // filename
    end if
    print*, "Writing simulation state to ", filename
    open (91, file=filename, status='replace')

    ! step for output of planes
    zstep = p%tnz / outflags%outnz
    ! number of planes that are output (may be more than outnz!)
    realoutnz = (p%tnz - 1) / zstep + 1 

    ! write parameters
    write(91,*) p%nmin;    write(91,*) p%nmax
    ! number of planes we write
    if (present (simstatefile) .and. simstatefile) then
      write(91,*) p%tnz
    else
      write(91,*) realoutnz
    end if
    write(91,*) p%k0;      write(91,*) p%lambda
    write(91,*) p%nu;      write(91,*) p%eta
    write(91,*) p%eps;     write(91,*) p%epsm
    write(91,*) p%b0;
    ! Distance between output planes
    if (present (simstatefile) .and. simstatefile) then
      write(91,*) p%dz
    else
      write(91,*) p%dz * zstep !(integer division)
    end if
    ! number of time steps really done (not p%nt!)
    write(91,*) ti
    write(91,*) p%delta_t   ! not taken into account by read_simstate

    ! write number of output records (only for data analysis)
    write(91,*) outrecords%energy
    write(91,*) 0
    write(91,*) outrecords%spec


    ! get shell values from all processors and write them
    do j = 0, p%tnz - 1
        if (mod (j, zstep) .eq. 0 &
            .or. (present (simstatefile) .and. simstatefile)) then
          do i = p%nmin, p%nmax
              write(91,*)  real  (zp(i, j), kind=dfloat)
              write(91,*)  aimag (zp(i, j))
          end do
        end if
    end do

    do j = 0, p%tnz - 1
        if (mod (j, zstep) .eq. 0 &
            .or. (present (simstatefile) .and. simstatefile)) then
          do i = p%nmin, p%nmax
              write(91,*)  real  (zm(i, j), kind=dfloat)
              write(91,*)  aimag (zm(i, j))
          end do
        end if
    end do

    ! Write current time
    write(91,*) t
    ! Write correlation time of forcing
    write(91,*) p%tstar
    ! Write number of modes on which forcing is made
    write(91,*) nkforce

    ! Write random coefficients of forcing
    do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
      write(91,*)  real (p%fc%a1(i), kind=dfloat)
      write(91,*) aimag (p%fc%a1(i))
      write(91,*)  real (p%fc%b1(i), kind=dfloat)
      write(91,*) aimag (p%fc%b1(i))
    end do
    ! Receive coefficients for second boundary from last processor
    do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
      write(91,*)  real (p%fc%a2(i), kind=dfloat)
      write(91,*) aimag (p%fc%a2(i))
      write(91,*)  real (p%fc%b2(i), kind=dfloat)
      write(91,*) aimag (p%fc%b2(i))
    end do

    ! Write parameters of atmosphere
    write(91,*) p%rho0
    write(91,*) 0.  ! h
    close (91)

  end subroutine write_simstate

end module iosimstate
