!+
!***********************************************************
! Module propagation-laxw
!***********************************************************
! Variables and routines for Alfven wave propagation
! With Lax-Wendroff numerical scheme (inside the domaine)
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  06 Jan 17 EB Created (forked from propagation-fromm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module propagation
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_propagation = "laxw"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Alfven wave propagation, Lax-Wendroff numerical scheme inside the box.
! zp on first plane and zm on last plane are _not_ computed (this is the
! job of the force routine)
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine propagate (zp, zm, k, p, t)
    use mpi
!-
    implicit none
    type(params),         intent(inout)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! old values in whole box
    complex(kind=dfloat), dimension(:,:), save, allocatable :: ozp, ozm

    ! are the local fields already allocated?
    logical, save :: isallocated = .FALSE.
    integer :: iz, istart, iend
    real(kind=dfloat) :: lam
    ! MPI variables
    integer, save     :: left, right
    integer           :: zlen
    integer           :: status(mpi_status_size)


    if (.NOT. isallocated) then
       ! Old fields, with needed extension (ghost cells)
       allocate (ozp(p%nmin:p%nmax, -1:p%nz), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (ozm(p%nmin:p%nmax, -1:p%nz), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       if (p%mpi%me .eq. 0) then
          left = mpi_proc_null
       else
          left = p%mpi%me - 1
       end if
       if (p%mpi%me .eq. p%mpi%np-1) then
          right = mpi_proc_null
       else
          right = p%mpi%me + 1
       end if
       isallocated = .TRUE.
    end if

!!! propagation by Lax-Wendroff numerical scheme inside the box, and 
!!! lower-order schemes at the boundaries (avoid boundary problem)

    !! Old values of fields
    ozp(:,0:p%nz-1) = zp
    ozm(:,0:p%nz-1) = zm

    zlen = p%nmax - p%nmin + 1
    ! send ozp to the right and receive it from the left
    call mpi_sendrecv (ozp(0,p%nz-1), zlen, mpi_double_complex, right, 18, &
                       ozp(0,    -1), zlen, mpi_double_complex, left,  18, &
                       mpi_comm_world, status, ierr)
    ! send ozp to the left  and receive it from the right
    call mpi_sendrecv (ozp(0,0),      zlen, mpi_double_complex, left,  20, &
                       ozp(0,p%nz),   zlen, mpi_double_complex, right, 20, &
                       mpi_comm_world, status, ierr)
    ! send ozm to the right and receive it from the left
    call mpi_sendrecv (ozm(0,p%nz-1), zlen, mpi_double_complex, right, 21, &
                       ozm(0,    -1), zlen, mpi_double_complex, left,  21, &
                       mpi_comm_world, status, ierr)
    ! send ozm to the left  and receive it from the right
    call mpi_sendrecv (ozm(0,0),      zlen, mpi_double_complex, left,  19, &
                       ozm(0,p%nz),   zlen, mpi_double_complex, right, 19, &
                       mpi_comm_world, status, ierr)

    lam  = p%delta_t / p%dz

    !! update zp
    if (p%mpi%me .eq. 0) then
      ! For first processor, start at iz=1: the new zp(0) is computed later
      ! (forcing).
      istart = 1
    else
      istart = 0
    end if
    if (p%mpi%me .eq. p%mpi%np - 1) then
      iend = p%nz - 2

      ! For last processor at iz=p%nz-1, we need an entirely upwind scheme,
      ! we choose Beam-Warming
      iz = p%nz - 1
      zp(:,iz) = ozp(:,iz) * (1._dfloat - lam * p%b0 &
          - .5_dfloat * lam * p%b0   * (1._dfloat - lam * p%b0)) &
          + ozp(:,iz-1) * (.5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) &
          + .5_dfloat * lam * p%b0   * (1._dfloat - lam * p%b0) + lam * p%b0) &
          - ozp(:,iz-2) * .5_dfloat * lam * p%b0  * (1._dfloat - lam * p%b0)
    else
      iend = p%nz - 1
    end if
          
    ! Lax-Wendroff numerical scheme, inside the box (excluding boundaries)
    do iz=istart, iend
        zp(:,iz) = ozp(:,iz) + &
              (- (ozp(:,iz) - ozp(:,iz-1)) * 0.5_dfloat * & 
              lam * p%b0 * (1._dfloat + lam * p%b0)) &
              - (ozp(:,iz+1) - ozp(:,iz)) * lam * p%b0 * 0.5_dfloat * &
              (1._dfloat - lam * p%b0)
    end do

    !! update zm
    if (p%mpi%me .eq. p%mpi%np-1) then
      ! For last processor, end at iz=nz-2: the new zm(nz-1) is
      ! computed later (forcing)
      iend = p%nz - 2
    else
      iend = p%nz - 1
    end if
    if (p%mpi%me .eq. 0) then
      istart = 1
      ! For first processor at iz=0, we need an entirely upwind scheme,
      ! we choose Beam-Warming
      iz = 0
      zm(:,iz) = ozm(:,iz) * (1._dfloat - lam * p%b0 &
          - .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0)) &
          + ozm(:,iz+1) * (.5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) &
          + .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0) + lam * p%b0)  &
          - ozm(:,iz+2) * .5_dfloat * lam * p%b0 * (1._dfloat - lam * p%b0)
    else
      istart = 0
    end if

    ! Lax-Wendroff numerical scheme, inside the box (excluding boundaries)
    do iz=istart, iend
        zm(:,iz) = ozm(:,iz) + &
              ((ozm(:,iz+1) - ozm(:,iz)) * 0.5_dfloat * & 
              lam * p%b0 * (1._dfloat + lam * p%b0) &
              + (ozm(:,iz) - ozm(:,iz-1)) * lam * p%b0 &
              * 0.5_dfloat * (1._dfloat - lam * p%b0))
    end do

  end subroutine propagate

end module propagation
