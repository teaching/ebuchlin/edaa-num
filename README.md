# Shell-Atm code, for the course on numerical simulations and high-performance computing (École Doctorale Astronomie et Astrophysique d'Île-de-France)

## What the code does

The code computes the evolution of the velocity and magnetic fields in a magnetic flux tube, in several cases (like coronal loops or the solar wind), including turbulence.
In such a flux tube, because of the longitudinal magnetic field, turbulence is essentially 2D, and can be described independently in different cross-sections of the flux tube. As a result of turbulence, energy is transferred to the small scales, where it is dissipated, this could explain heating of the corona and acceleration of the solar wind.

Each cross-section of the flux tube is modelled by a "shell-model", a simplified model (isotropic, with logarithmic discretization in wavenumber) of the nonlinear terms of 2D MHD.
These cross-sections are then coupled by the propagation of Alfvén waves along the flux tube.
Energy is injected into the system by imposing a velocity as boundary conditions, corresponding to the motions of the solar photosphere.

The different modules of the code (not all provided here) allow the simulation of different physical systems, using different numerical methods.
The ones I provide here correspond to:

* Order-3 Runge-Kutta scheme for solving the ODE for shell-models
* Lax-Wendroff scheme for the propagation of Alfvén waves
* Uniform atmosphere (density, magnetic field)
* Velocity forcing at the boundaries (both ends of the magnetic flux tube).

## How to use the code

### Edit the model parameters

In `params.txt`:

* `nz`: number of cross-sections in the longitudinal direction
* `dz`: grid step size (distance between cross-sections). I suggest that you keep the total length of the flux tube (`nz * dz`) equal to 1 and the Alfvén speed (`b0`) also equal to 1.
* `tmax`: the duration of the simulation

You do not need to change the other parameters.

### Edit the output parameters

In `param_o.txt`:
* time scales for output
* number of planes in output of fields


### Compile the code

First, check the compilers in `Makefile.platform`, and the modules to be used in `Makefile.model`. You can use a non-MPI compiler to prepare a non-parallel run, or a MPI compiler to prepare a parallel run.
Then use
    make

### Do an interactive, non-parallel run

Simply type

    ./shell-atm

As an alternative, you can use

    make interactiverun

this will also compile the code if needed, and delete output files from previous runs.


### Do a parallel run with the cluster queuing system

If the cluster queuing system is LoadLeveler, edit the script details in `mpi_ll.txt` then use `llsubmit mpi_ll.txt` to put the job in the queue. This can also be done using `make mpirunll`.

## Make the code parallel using MPI

1. Think about **how to split grid points between the processors**. In the `p` structure (defined in `types.f90` and used all over the code), `p%nz` is the number of grid points in the `z` (spatial) direction, and `p%tnz` is the total number of grid points.

1. Start by the **main program**, `shell-atm.f90`: initialize MPI (please use the variables included in `p%mpi` to store the processor rank and number of processors), get familiar with the code structure, the variables, the functions which are called. Then we will see which functions have to be parallelized.

1. You can **share the parallelization work** among you (each person or group working on a different file), then put everything together, and make the whole parallelized code work. Check that the code is working and giving meaningful results.

1. Check **parallelization efficiency** with weak and strong scaling curves.

Functions to be parallelized, by file:
* `shell-atm.f90`
  * `Program shell_atm`
* `nonlin-shell.f90`: nothing to do.
* `propagation-laxw.f90`
  * `propagate`: data can be transferred to and from previous and next processors with `mpi_send_recv`. Ghost cells are already present as needed.
* `boundary-photospheres.f90`:
    * `force`: with several processors, the two boundaries are in different processors.
* `diagnostics.f90`
  * `energy_tot`: sum of energy in all processors.
  * `diss_tot`: sum of dissipation power in all processors.
  * `check_time_scales`: take the smallest timescales of all processors.
  * `wallclock`: only needed on root processor.
* `initialize.f90`
  * `get_wavenumbers`: this is only to have a different initialization of the random numbers generator on different processors.
* `iodata-formatted.f90`
  * `output_open_files`, `output_close_files`: these files stay open during the simulation, but should be opened and accessed only by one processor.
  * `output_data_a` (small time scale output), `output_data_b` (larger time scale output): the code has to transfer all data from all processors to the processor which writes the files.
* `ioparams.f90`
  * `read_parameters`: to be done once, and broadcast values.
  * `print_parameters`
  * `output_data_atm`
  * `read_output_options`: to be done once, and broadcast values.
  * `print_output_options`
* `iostate-formatted.f90`
  * `read_simstate`: to be done once, and broadcast values.
  * `write_simstate`: the code has to transfer all data from all processors to the processor which writes the files.

When a function does *not* need to be parallelized, try also to understand why...


## Analyze the results of the code

The `PyShellAtm` module can be used to plot code results.

Ideas of physical results to obtain:
* Energy and dissipated power as a function of time: `plot_energy()`, `plot_de()`
* Wave energies, energy flux, kinetic and magnetic energies, as a function of time and position for a given wave number `k`: `plot_cutk()`.
* Same quantities as function of wavenumber and position, for a given time: `plot_cutt()`. Make a movie from the time evolution of these quantities, see waves propagation and nonlinear interactions.
* Time scales as a function of wavenumber: `plot_dt()`

## Proposed planning of work

1. Wednesday: parallelize code
2. Thursday: get results (scalings, physical output)
3. Friday morning: prepare presentation
4. Friday afternoon: presentation
